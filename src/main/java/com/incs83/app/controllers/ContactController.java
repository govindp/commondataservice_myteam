package com.incs83.app.controllers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.incs83.app.annotations.PreHandle;
import com.incs83.app.business.ContactService;
import com.incs83.app.common.CardContactRequest;
import com.incs83.app.common.CardRequest;
import com.incs83.app.common.ContactRequest;
import com.incs83.app.common.ShareContactsRequest;
import com.incs83.app.context.ExecutionContext;
import com.incs83.app.enums.ApiResponseCode;
import com.incs83.app.enums.ContactsType;
import com.incs83.app.enums.ResourceType;
import com.incs83.app.responsedto.ResponseDTO;
import com.incs83.app.responsedto.dto.CompanyDTO;
import com.incs83.app.responsedto.dto.CompanyDetailsDTO;
import com.incs83.app.service.data.Neo4jService;
import com.incs83.app.utils.application.CommonUtils;
import com.incs83.app.utils.system.ResponseUtil;
import com.incs83.logger.LoggerFactory;

/**
 * Created by rosrivas on 7/2/17.
 */
@RestController
@RequestMapping(value = "/{tenant}/api/v1/contacts")
public class ContactController {

    private static final Logger LOG = LoggerFactory.make();

    @Autowired
    private ResponseUtil responseUtil;

    @Autowired
    private Neo4jService neo4jService;

    @Autowired
    private ContactService contactService;


    @RequestMapping(value = "/upload/my", method = RequestMethod.POST)
    @PreHandle(requestMethod = RequestMethod.POST, resourceType = ResourceType.CONTACTS)
    public ResponseDTO<?> uploadMyContacts(@RequestBody MultipartFile file,
                                           HttpServletRequest httpServletRequest) throws Exception {
        contactService.uploadContacts(file, ContactsType.MY);
        return responseUtil.ok(null, ApiResponseCode.SUCCESS,
                CommonUtils.getLocaleStringFromClientHTTPRequest(httpServletRequest));
    }

    @RequestMapping(value = "/upload/company", method = RequestMethod.POST)
    @PreHandle(requestMethod = RequestMethod.POST, resourceType = ResourceType.COMPANY)
    public ResponseDTO<?> uploadCompanyContacts(@RequestBody MultipartFile file,
                                                HttpServletRequest httpServletRequest) throws Exception {
        contactService.uploadContacts(file, ContactsType.COMPANY);
        return responseUtil.ok(null, ApiResponseCode.SUCCESS,
                CommonUtils.getLocaleStringFromClientHTTPRequest(httpServletRequest));
    }

    @RequestMapping(value = "/companies", method = RequestMethod.GET)
    @PreHandle(requestMethod = RequestMethod.GET, resourceType = ResourceType.COMPANY)
    public ResponseDTO<?> getCompanies(HttpServletRequest httpServletRequest) throws Exception {
        List<CompanyDTO> companyDTOS = contactService.getCompanies();
        return responseUtil.ok(companyDTOS, ApiResponseCode.SUCCESS,
                CommonUtils.getLocaleStringFromClientHTTPRequest(httpServletRequest));
    }

    @RequestMapping(value = "/companies/{companyId}", method = RequestMethod.GET)
    @PreHandle(requestMethod = RequestMethod.GET, resourceType = ResourceType.COMPANY)
    public ResponseDTO<?> getCompanyDetails(@PathVariable String companyId, HttpServletRequest httpServletRequest) throws Exception {
        CompanyDetailsDTO companyDetails = contactService.getCompanyDetails(companyId);
        return responseUtil.ok(companyDetails, ApiResponseCode.SUCCESS,
                CommonUtils.getLocaleStringFromClientHTTPRequest(httpServletRequest));
    }

    @RequestMapping(value = "/my", method = RequestMethod.GET)
    @PreHandle(requestMethod = RequestMethod.GET, resourceType = ResourceType.CONTACTS)
    public ResponseDTO<?> viewMyContacts(ContactRequest contactRequest,
                                         HttpServletRequest httpServletRequest) throws Exception {
        LOG.info("Request to fetch My contacts....");
        return responseUtil.ok(neo4jService.viewMyContacts(contactRequest), ApiResponseCode.SUCCESS,
                CommonUtils.getLocaleStringFromClientHTTPRequest(httpServletRequest));
    }

    @RequestMapping(value = "/company", method = RequestMethod.GET)
    public ResponseDTO<?> viewCompanyContacts(ContactRequest contactRequest,
                                              HttpServletRequest httpServletRequest) throws Exception {
        LOG.info("Request to fetch My company contacts....");
        return responseUtil.ok(neo4jService.viewCompanyContacts(contactRequest), ApiResponseCode.SUCCESS,
                CommonUtils.getLocaleStringFromClientHTTPRequest(httpServletRequest));
    }

    @RequestMapping(value = "/share", method = RequestMethod.POST)
    @PreHandle(requestMethod = RequestMethod.POST, resourceType = ResourceType.CONTACTS)
    public ResponseDTO<?> share(@RequestBody ShareContactsRequest shareContactsRequest,
                                HttpServletRequest httpServletRequest) throws Exception {
        LOG.info("Request Share Contacts...");
        neo4jService.share(shareContactsRequest);
        return responseUtil.ok(null, ApiResponseCode.SUCCESS,
                CommonUtils.getLocaleStringFromClientHTTPRequest(httpServletRequest));
    }


    @RequestMapping(value = "/share", method = RequestMethod.DELETE)
    @PreHandle(requestMethod = RequestMethod.DELETE, resourceType = ResourceType.CONTACTS)
    public ResponseDTO<?> unshare(@RequestBody ShareContactsRequest shareContactsRequest,
                                  HttpServletRequest httpServletRequest) throws Exception {
        LOG.info("Request Share Contacts...");
        neo4jService.unshare(shareContactsRequest);
        return responseUtil.ok(null, ApiResponseCode.SUCCESS,
                CommonUtils.getLocaleStringFromClientHTTPRequest(httpServletRequest));
    }

    @RequestMapping(value = "/uploadCard", method = RequestMethod.POST)
    @PreHandle(requestMethod = RequestMethod.POST, resourceType = ResourceType.CARD)
    public ResponseDTO<?> uploadContactCards(@RequestPart MultipartFile frontImg, @RequestPart(required = false) MultipartFile backImg,
                                             HttpServletRequest httpServletRequest) throws Exception {
        LOG.info("Request to Upload Contacts Card...");
        CardRequest files = new CardRequest(frontImg, backImg);
        return responseUtil.ok(contactService.uploadContactCard(files), ApiResponseCode.SUCCESS,
                CommonUtils.getLocaleStringFromClientHTTPRequest(httpServletRequest));
    }

    @RequestMapping(value = "/uploadCardContact", method = RequestMethod.POST)
    @PreHandle(requestMethod = RequestMethod.POST, resourceType = ResourceType.CONTACTS)
    public ResponseDTO<?> addCards(@RequestBody CardContactRequest cardContactRequest,
                                   HttpServletRequest httpServletRequest) throws Exception {
        LOG.info("Request to add Massaged Card Data...");
        neo4jService.uploadContacts(contactService.toUser(cardContactRequest), ExecutionContext.CONTEXT.get().getUsercontext().getId(), ContactsType.MY);
        return responseUtil.ok(null, ApiResponseCode.SUCCESS,
                CommonUtils.getLocaleStringFromClientHTTPRequest(httpServletRequest));
    }

    @RequestMapping(value = "/cards", method = RequestMethod.GET)
    @PreHandle(requestMethod = RequestMethod.GET, resourceType = ResourceType.CARD)
    public ResponseDTO<?> getCards(HttpServletRequest httpServletRequest) throws Exception {
        LOG.info("Request to fetch Contacts Card...");
        return responseUtil.ok(contactService.fetchCards(), ApiResponseCode.SUCCESS,
                CommonUtils.getLocaleStringFromClientHTTPRequest(httpServletRequest));
    }

    @RequestMapping(value = "/cards/{cardId}", method = RequestMethod.DELETE)
    @PreHandle(requestMethod = RequestMethod.DELETE, resourceType = ResourceType.CARD)
    public ResponseDTO<?> deleteCard(@PathVariable String cardId, HttpServletRequest httpServletRequest) throws Exception {
        LOG.info("Request to delete Contact Card...");
        contactService.deleteCard(cardId);
        return responseUtil.ok(null, ApiResponseCode.SUCCESS,
                CommonUtils.getLocaleStringFromClientHTTPRequest(httpServletRequest));
    }
}
