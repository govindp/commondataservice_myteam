package com.incs83.app.controllers;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.incs83.app.annotations.PreHandle;
import com.incs83.app.business.AccountService;
import com.incs83.app.common.AccountRequest;
import com.incs83.app.enums.ApiResponseCode;
import com.incs83.app.enums.ResourceType;
import com.incs83.app.responsedto.ResponseDTO;
import com.incs83.app.utils.application.CommonUtils;
import com.incs83.app.utils.system.ResponseUtil;
import com.incs83.logger.LoggerFactory;

/**
 * Created by jayant on 30/4/17.
 */
@RestController
@RequestMapping(value = "/83incs/api")
public class PublicController {

	private static final Logger LOG = LoggerFactory.make();

	@Autowired
	private ResponseUtil responseUtil;

	@Autowired
	private AccountService accountService;

	@PreHandle(requestMethod = RequestMethod.POST, resourceType = ResourceType.ACCOUNT_REQUEST)
	@RequestMapping(value = "/requestAccount", method = RequestMethod.POST)
	public ResponseDTO<?> requestAccount(@RequestBody AccountRequest accountRequest,
			HttpServletRequest httpServletRequest) throws Exception {
		LOG.info("Request to persist Account..");
		// TaskRequest request = new TaskRequest();
		// request.setService("CDS");
		// request.setLabel("CREATE_ACCOUNT");
		// request.setState("NEW");
		// request.setPayload(new
		// ObjectMapper().writeValueAsString(accountRequest));
		// taskService.createTask(request);
		accountService.requestAccountCreation(accountRequest);
		// HashMap<String, String> mailParams = new HashMap<>();
		// mailParams.put("services",
		// accountRequest.getServices().stream().collect(Collectors.joining(",")));
		// mailService.sendWelcomeEmail(accountRequest.getEmailId(),
		// mailParams);
		return responseUtil.ok(null, ApiResponseCode.SUCCESS,
				CommonUtils.getLocaleStringFromClientHTTPRequest(httpServletRequest));
	}

	@PreHandle(requestMethod = RequestMethod.POST, resourceType = ResourceType.ACCOUNT_REQUEST)
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseDTO<?> requestAccount(@RequestBody String accountRequest, HttpServletRequest httpServletRequest)
			throws Exception {
		AccountRequest accountRequest1 = new ObjectMapper().readValue(accountRequest, AccountRequest.class);
		accountService.requestAccountCreation(accountRequest1);
		return responseUtil.ok(null, ApiResponseCode.SUCCESS,
				CommonUtils.getLocaleStringFromClientHTTPRequest(httpServletRequest));
	}

}
