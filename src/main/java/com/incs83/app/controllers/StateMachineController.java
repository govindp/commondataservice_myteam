package com.incs83.app.controllers;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.incs83.app.annotations.PreHandle;
import com.incs83.app.common.EventRequest;
import com.incs83.app.enums.ApiResponseCode;
import com.incs83.app.enums.ResourceType;
import com.incs83.app.responsedto.ResponseDTO;
import com.incs83.app.statemachine.Persist;
import com.incs83.app.utils.application.CommonUtils;
import com.incs83.app.utils.system.ResponseUtil;

/**
 * Created by rosrivas on 7/2/17.
 */
@RestController
@RequestMapping(value = "/{tenant}/api/v1/sm")
public class StateMachineController {

    @Autowired
    private ResponseUtil responseUtil;

    @Autowired
    private Persist persist;

    @PreHandle(requestMethod = RequestMethod.POST, resourceType = ResourceType.TASK)
    @RequestMapping(value = "/event", method = RequestMethod.POST)
    public ResponseDTO<?> sendEvent(@RequestBody EventRequest eventRequest, HttpServletRequest httpServletRequest)
            throws Exception {
        Map<String, Object> headers = new HashMap<>();
        headers.put("payload", eventRequest.getPayload());
        persist.change(eventRequest.getTaskId(), eventRequest.getSubTaskId(),
                eventRequest.getEvent(), eventRequest.getService(), headers);
        return responseUtil.ok(null, ApiResponseCode.SUCCESS,
                CommonUtils.getLocaleStringFromClientHTTPRequest(httpServletRequest));
    }
}
