package com.incs83.app.controllers;

import com.incs83.app.annotations.PreHandle;
import com.incs83.app.business.ProfileService;
import com.incs83.app.enums.ApiResponseCode;
import com.incs83.app.enums.ResourceType;
import com.incs83.app.responsedto.ResponseDTO;
import com.incs83.app.utils.application.CommonUtils;
import com.incs83.app.utils.system.ResponseUtil;
import com.incs83.logger.LoggerFactory;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(value = "/{tenant}/api/v1/profile")
public class ProfileController {

    private static final Logger LOG = LoggerFactory.make();

    @Autowired
    private ResponseUtil responseUtil;

    @Autowired
    private ProfileService profileService;

    @PreHandle(requestMethod = RequestMethod.GET, resourceType = ResourceType.USER)
    @RequestMapping(method = RequestMethod.GET)
    public ResponseDTO<?> getProfile(HttpServletRequest httpServletRequest, @RequestParam(value = "id",required = false)String id)
            throws Exception {
        LOG.info("Fetching Profile...");
        return responseUtil.ok(profileService.getProfile(id), ApiResponseCode.SUCCESS,
                CommonUtils.getLocaleStringFromClientHTTPRequest(httpServletRequest));
    }
}
