package com.incs83.app.controllers;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.incs83.app.annotations.PreHandle;
import com.incs83.app.business.AccountService;
import com.incs83.app.enums.AccountRequestActions;
import com.incs83.app.enums.ApiResponseCode;
import com.incs83.app.enums.ResourceType;
import com.incs83.app.responsedto.ResponseDTO;
import com.incs83.app.security.config.WebSecurityConfig;
import com.incs83.app.utils.application.CommonUtils;
import com.incs83.app.utils.system.ResponseUtil;
import com.incs83.logger.LoggerFactory;

/**
 * Created by jayant on 30/4/17.
 */
@RestController
@RequestMapping(value = "/{tenant}/api/v1/account")
public class AccountController {

    private static final Logger LOG = LoggerFactory.make();

    @Autowired
    private ResponseUtil responseUtil;

    @Autowired
    private AccountService accountService;

    @PreHandle(requestMethod = RequestMethod.GET, resourceType = ResourceType.ACCOUNT_REQUEST)
    @RequestMapping(value = "/requests", method = RequestMethod.GET)
    public ResponseDTO<?> getRequests(HttpServletRequest httpServletRequest,
                                      @RequestParam(required = false, value = "type") String type) throws Exception {
        LOG.info("Getting all Requests depending upon Filter...");
        return responseUtil.ok(accountService.getAllRequests(type), ApiResponseCode.SUCCESS, CommonUtils.getLocaleStringFromClientHTTPRequest(httpServletRequest));
    }

    @PreHandle(requestMethod = RequestMethod.GET, resourceType = ResourceType.ACCOUNT_REQUEST)
    @RequestMapping(value = "/request/{id}", method = RequestMethod.GET)
    public ResponseDTO<?> getRequestDetails(HttpServletRequest httpServletRequest, @PathVariable("id") String id) throws Exception {
        LOG.info("Show Details of the Request...");
        return responseUtil.ok(accountService.getRequestDetails(id), ApiResponseCode.SUCCESS, CommonUtils.getLocaleStringFromClientHTTPRequest(httpServletRequest));
    }

    @PreHandle(requestMethod = RequestMethod.GET, resourceType = ResourceType.ACCOUNT)
    @RequestMapping(method = RequestMethod.GET)
    public ResponseDTO<?> showAllAccounts(HttpServletRequest httpServletRequest) throws Exception {
        LOG.info("Show Details of the Request...");
        return responseUtil.ok(accountService.getAllAccounts(), ApiResponseCode.SUCCESS, CommonUtils.getLocaleStringFromClientHTTPRequest(httpServletRequest));
    }

    @PreHandle(requestMethod = RequestMethod.POST, resourceType = ResourceType.ACCOUNT_REQUEST)
    @RequestMapping(value = "/request/{id}/{action}", method = RequestMethod.POST)
    public ResponseDTO<?> takeRequestAction(HttpServletRequest httpServletRequest, @PathVariable("id") String id,
                                            @PathVariable("action") String action) throws Exception {
        if (action.equals(AccountRequestActions.APPROVE.name()) || action.equals(AccountRequestActions.REJECT.name())) {
            String tokenPayload = httpServletRequest.getHeader(WebSecurityConfig.JWT_TOKEN_HEADER_PARAM);
            Map<String, Object> headers = new HashMap<>();
            headers.put("token", tokenPayload);
            accountService.processRequest(tokenPayload, id, action);
            //persist.change(id, null, "APPROVE", "CDS", headers);
            return responseUtil.ok(null, ApiResponseCode.SUCCESS,
                    CommonUtils.getLocaleStringFromClientHTTPRequest(httpServletRequest));
        } else {
            return responseUtil.exception(ApiResponseCode.BAD_REQUEST);
        }
    }


    @PreHandle(requestMethod = RequestMethod.POST, resourceType = ResourceType.ACCOUNT_REQUEST)
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public ResponseDTO<?> createAccount(HttpServletRequest httpServletRequest,
                                        @RequestBody String accountRequest) throws Exception {
//        AccountRequest accountRequestObj = new ObjectMapper().readValue(accountRequest, AccountRequest.class);
        String tokenPayload = httpServletRequest.getHeader(WebSecurityConfig.JWT_TOKEN_HEADER_PARAM);
        Map<String, Object> headers = new HashMap<>();
        headers.put("token", tokenPayload);
        ///accountService.createAccount(accountRequestObj);
        return responseUtil.ok(null, ApiResponseCode.SUCCESS,
                    CommonUtils.getLocaleStringFromClientHTTPRequest(httpServletRequest));
    }
}
