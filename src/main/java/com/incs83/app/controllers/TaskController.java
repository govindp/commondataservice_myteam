package com.incs83.app.controllers;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.incs83.app.annotations.PreHandle;
import com.incs83.app.business.TaskService;
import com.incs83.app.common.AccountRequest;
import com.incs83.app.common.TaskRequest;
import com.incs83.app.enums.ApiResponseCode;
import com.incs83.app.enums.ResourceType;
import com.incs83.app.responsedto.ResponseDTO;
import com.incs83.app.service.application.MailService;
import com.incs83.app.utils.application.CommonUtils;
import com.incs83.app.utils.system.ResponseUtil;
import com.incs83.logger.LoggerFactory;

/**
 * Created by rosrivas on 7/2/17.
 */
@RestController
@RequestMapping(value = "/{tenant}/api/v1/task")
public class TaskController {

    private static final Logger LOG = LoggerFactory.make();

    @Autowired
    private MailService mailService;

    @Autowired
    private ResponseUtil responseUtil;

    @Autowired
    private TaskService taskService;

    @PreHandle(requestMethod = RequestMethod.POST, resourceType = ResourceType.TASK)
    @RequestMapping(method = RequestMethod.POST)
    public ResponseDTO<?> createTask(@RequestBody TaskRequest taskRequest, HttpServletRequest httpServletRequest)
            throws Exception {
        LOG.info("Creating a New Task...");
        taskService.createTask(taskRequest);
        return responseUtil.ok(null, ApiResponseCode.SUCCESS,
                CommonUtils.getLocaleStringFromClientHTTPRequest(httpServletRequest));
    }

    @PreHandle(requestMethod = RequestMethod.GET, resourceType = ResourceType.TASK)
    @RequestMapping(method = RequestMethod.GET)
    public ResponseDTO<?> getTasks(HttpServletRequest httpServletRequest, @RequestParam(value = "id",required = true) String id) throws Exception {
        LOG.info("Getting Task(s)..");
        return responseUtil.ok(taskService.getTasksForUser(id), ApiResponseCode.SUCCESS,
                CommonUtils.getLocaleStringFromClientHTTPRequest(httpServletRequest));
    }

    @PreHandle(requestMethod = RequestMethod.GET, resourceType = ResourceType.TASK)
    @RequestMapping(value = "/my", method = RequestMethod.GET)
    public ResponseDTO<?> getMyTasks(HttpServletRequest httpServletRequest) throws Exception {
        LOG.info("Getting Task(s)..");
        return responseUtil.ok(taskService.getTasksForUser(null), ApiResponseCode.SUCCESS,
                CommonUtils.getLocaleStringFromClientHTTPRequest(httpServletRequest));
    }

    @PreHandle(requestMethod = RequestMethod.DELETE, resourceType = ResourceType.TASK)
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseDTO<?> deleteTask(HttpServletRequest httpServletRequest, @PathVariable("id") String id) throws Exception {
        LOG.info("Deleting Task...");
        taskService.deleteTask(id);
        return responseUtil.ok(null, ApiResponseCode.SUCCESS,
                CommonUtils.getLocaleStringFromClientHTTPRequest(httpServletRequest));
    }

    @PreHandle(requestMethod = RequestMethod.POST, resourceType = ResourceType.TASK)
    @RequestMapping(value = "/notify", method = RequestMethod.POST)
    public ResponseDTO<?> notify(@RequestBody Map<String, String> mailParams, HttpServletRequest httpServletRequest)
            throws Exception {
        String email = mailParams.remove("email");
        mailService.sendWelcomeEmail(email, mailParams);
        return responseUtil.ok(null, ApiResponseCode.SUCCESS,
                CommonUtils.getLocaleStringFromClientHTTPRequest(httpServletRequest));
    }

    @PreHandle(requestMethod = RequestMethod.POST, resourceType = ResourceType.TASK)
    @RequestMapping(value = "/assign", method = RequestMethod.POST)
    public ResponseDTO<?> assign(@RequestBody AccountRequest accountRequest, @RequestParam("id") String id, HttpServletRequest httpServletRequest)
            throws Exception {
        taskService.assignTask(accountRequest, id);
        return responseUtil.ok(null, ApiResponseCode.SUCCESS,
                CommonUtils.getLocaleStringFromClientHTTPRequest(httpServletRequest));
    }

    @PreHandle(requestMethod = RequestMethod.POST, resourceType = ResourceType.TASK)
    @RequestMapping(value = "/approve", method = RequestMethod.POST)
    public ResponseDTO<?> approve(@RequestBody Map<String, String> approveParams, HttpServletRequest httpServletRequest)
            throws Exception {

        return responseUtil.ok(null, ApiResponseCode.SUCCESS,
                CommonUtils.getLocaleStringFromClientHTTPRequest(httpServletRequest));
    }
}
