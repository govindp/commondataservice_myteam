package com.incs83.app.controllers;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.incs83.app.annotations.PreHandle;
import com.incs83.app.business.ResourceService;
import com.incs83.app.common.AddSubStateRequest;
import com.incs83.app.common.RoleRequest;
import com.incs83.app.common.StateRequest;
import com.incs83.app.common.UserRequest;
import com.incs83.app.enums.ApiResponseCode;
import com.incs83.app.enums.ResourceType;
import com.incs83.app.responsedto.ResponseDTO;
import com.incs83.app.utils.application.CommonUtils;
import com.incs83.app.utils.system.ResponseUtil;
import com.incs83.logger.LoggerFactory;

/**
 * Created by rosrivas on 7/2/17.
 */
@RestController
@RequestMapping(value = "/{tenant}/api/v1/resource")
public class ResourceController {

    private static final Logger LOG = LoggerFactory.make();

    @Autowired
    private ResponseUtil responseUtil;

    @Autowired
    private ResourceService resourceService;

    @PreHandle(requestMethod = RequestMethod.POST, resourceType = ResourceType.ROLE)
    @RequestMapping(value = "/role", method = RequestMethod.POST)
    public ResponseDTO<?> createRole(@RequestBody RoleRequest roleRequest, HttpServletRequest httpServletRequest)
            throws Exception {
        LOG.info("Creating a New Role...");
        resourceService.createRole(roleRequest);
        return responseUtil.ok(null, ApiResponseCode.SUCCESS,
                CommonUtils.getLocaleStringFromClientHTTPRequest(httpServletRequest));
    }

    @PreHandle(requestMethod = RequestMethod.GET, resourceType = ResourceType.ROLE)
    @RequestMapping(value = "/role", method = RequestMethod.GET)
    public ResponseDTO<?> getRoles(HttpServletRequest httpServletRequest, @RequestParam(name = "id", required = false) String id) throws Exception {
        LOG.info("Getting Role(s)..");
        return responseUtil.ok(resourceService.getRoles(id), ApiResponseCode.SUCCESS,
                CommonUtils.getLocaleStringFromClientHTTPRequest(httpServletRequest));
    }

    @PreHandle(requestMethod = RequestMethod.DELETE, resourceType = ResourceType.ROLE)
    @RequestMapping(value = "/role", method = RequestMethod.DELETE)
    public ResponseDTO<?> deleteRole(HttpServletRequest httpServletRequest, @RequestParam(name = "id") String id) throws Exception {
        LOG.info("Deleting Role...");
        resourceService.deleteRole(id);
        return responseUtil.ok(null, ApiResponseCode.SUCCESS,
                CommonUtils.getLocaleStringFromClientHTTPRequest(httpServletRequest));
    }

    @PreHandle(requestMethod = RequestMethod.PUT, resourceType = ResourceType.ROLE)
    @RequestMapping(value = "/role", method = RequestMethod.PUT)
    public ResponseDTO<?> updateRole(HttpServletRequest httpServletRequest,
                                     @RequestParam(name = "id") String id,
                                     @RequestBody RoleRequest payload) throws Exception {
        LOG.info("Updating Role...");
        resourceService.updateRole(id, payload);
        return responseUtil.ok(null, ApiResponseCode.SUCCESS,
                CommonUtils.getLocaleStringFromClientHTTPRequest(httpServletRequest));
    }

    @RequestMapping(value = "/permission", method = RequestMethod.GET)
    @PreHandle(requestMethod = RequestMethod.GET, resourceType = ResourceType.PERMISSION)
    public ResponseDTO<?> getPermissions(HttpServletRequest httpServletRequest) throws Exception {
        LOG.info("Getting Permission(s)...");
        return responseUtil.ok(resourceService.getPermissions(), ApiResponseCode.SUCCESS,
                CommonUtils.getLocaleStringFromClientHTTPRequest(httpServletRequest));
    }

    @RequestMapping(value = "/entitlement", method = RequestMethod.GET)
    @PreHandle(requestMethod = RequestMethod.GET, resourceType = ResourceType.ENTITLEMENT)
    public ResponseDTO<?> getEntitlements(@RequestParam(name = "service") String service, HttpServletRequest httpServletRequest) throws Exception {
        LOG.info("Getting Entitlement(s)...");
        return responseUtil.ok(resourceService.getEntitlements(service), ApiResponseCode.SUCCESS,
                CommonUtils.getLocaleStringFromClientHTTPRequest(httpServletRequest));
    }

    @PreHandle(requestMethod = RequestMethod.POST, resourceType = ResourceType.USER)
    @RequestMapping(value = "/user", method = RequestMethod.POST)
    public ResponseDTO<?> createUser(@RequestBody @Validated UserRequest userRequest,
                                     HttpServletRequest httpServletRequest) throws Exception {
        LOG.info("Creating a New User...");
        resourceService.createUser(userRequest);
        return responseUtil.ok(null, ApiResponseCode.SUCCESS,
                CommonUtils.getLocaleStringFromClientHTTPRequest(httpServletRequest));
    }

    @PreHandle(requestMethod = RequestMethod.PUT, resourceType = ResourceType.USER)
    @RequestMapping(value = "/user", method = RequestMethod.PUT)
    public ResponseDTO<?> updateUser(HttpServletRequest httpServletRequest, @RequestParam(name = "id") String id,
                                     @RequestBody UserRequest userRequest) throws Exception {
        LOG.info("Updating User...");
        resourceService.updateUser(id, userRequest);
        return responseUtil.ok(null, ApiResponseCode.SUCCESS,
                CommonUtils.getLocaleStringFromClientHTTPRequest(httpServletRequest));
    }

    @PreHandle(requestMethod = RequestMethod.GET, resourceType = ResourceType.USER)
    @RequestMapping(value = "/user", method = RequestMethod.GET)
    public ResponseDTO<?> getUser(HttpServletRequest httpServletRequest, @RequestParam(name = "id", required = false) String id) throws Exception {
        LOG.info("Getting all Users...");
        return responseUtil.ok(resourceService.getUsers(id), ApiResponseCode.SUCCESS,
                CommonUtils.getLocaleStringFromClientHTTPRequest(httpServletRequest));
    }

    @PreHandle(requestMethod = RequestMethod.DELETE, resourceType = ResourceType.USER)
    @RequestMapping(value = "/user", method = RequestMethod.DELETE)
    public ResponseDTO<?> deleteUser(HttpServletRequest httpServletRequest, @RequestParam(name = "id") String id) throws Exception {
        LOG.info("Deleting User...");
        resourceService.deleteUser(id);
        return responseUtil.ok(null, ApiResponseCode.SUCCESS,
                CommonUtils.getLocaleStringFromClientHTTPRequest(httpServletRequest));
    }

    @PreHandle(requestMethod = RequestMethod.POST, resourceType = ResourceType.TASK_TEMPLATE)
    @RequestMapping(value = "/taskTemplate", method = RequestMethod.POST)
    public ResponseDTO<?> createState(@RequestBody StateRequest stateRequest, HttpServletRequest httpServletRequest)
            throws Exception {
        LOG.info("Creating a New Task Template...");
        resourceService.createState(stateRequest);
        return responseUtil.ok(null, ApiResponseCode.SUCCESS,
                CommonUtils.getLocaleStringFromClientHTTPRequest(httpServletRequest));
    }

    @PreHandle(requestMethod = RequestMethod.GET, resourceType = ResourceType.TASK_TEMPLATE)
    @RequestMapping(value = "/taskTemplate", method = RequestMethod.GET)
    public ResponseDTO<?> getStates(HttpServletRequest httpServletRequest)
            throws Exception {
        LOG.info("Getting all Task Templates...");
        return responseUtil.ok(resourceService.getStates(null), ApiResponseCode.SUCCESS,
                CommonUtils.getLocaleStringFromClientHTTPRequest(httpServletRequest));
    }

    @PreHandle(requestMethod = RequestMethod.DELETE, resourceType = ResourceType.TASK_TEMPLATE)
    @RequestMapping(value = "/taskTemplate/{id}", method = RequestMethod.DELETE)
    public ResponseDTO<?> deleteState(HttpServletRequest httpServletRequest, @PathVariable("id") String id) throws Exception {
        LOG.info("Deleting Permission...");
        resourceService.deleteState(id);
        return responseUtil.ok(null, ApiResponseCode.SUCCESS,
                CommonUtils.getLocaleStringFromClientHTTPRequest(httpServletRequest));
    }

    @PreHandle(requestMethod = RequestMethod.POST, resourceType = ResourceType.TASK_TEMPLATE)
    @RequestMapping(value = "/taskTemplate/taskTemplateDetail", method = RequestMethod.POST)
    public ResponseDTO<?> addSubstate(@RequestBody AddSubStateRequest addSubStateRequest, HttpServletRequest httpServletRequest)
            throws Exception {
        LOG.info("Getting all States...");
        resourceService.addSubState(addSubStateRequest);
        return responseUtil.ok(null, ApiResponseCode.SUCCESS,
                CommonUtils.getLocaleStringFromClientHTTPRequest(httpServletRequest));
    }

}

