package com.incs83.app.annotations;

import com.incs83.app.enums.ResourceType;
import org.springframework.web.bind.annotation.RequestMethod;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by hari on 27/7/17.
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface PreHandle {

    RequestMethod requestMethod() default RequestMethod.GET;

    ResourceType resourceType();

}
