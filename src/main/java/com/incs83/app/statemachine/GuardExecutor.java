package com.incs83.app.statemachine;

import com.incs83.app.entities.Task;
import com.incs83.app.entities.TaskDetail;
import com.incs83.app.enums.ResourceType;
import com.incs83.app.service.generic.DataAccessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.guard.Guard;

import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;

/**
 * Created by rosrivas on 7/23/17.
 */
@SuppressWarnings("rawtypes")
public class GuardExecutor implements Guard<String, String> {
    
	@Autowired
    private DataAccessService dataAccessService;

    @Override
    public boolean evaluate(StateContext<String, String> context) {
        /*try {
            if (Objects.isNull(context.getMessage())) {
                return false;
            }
           // CurrentTenantIdentifier._tenantIdentifier.set(tenant);
            String taskId = context.getMessage().getHeaders().get("task", String.class);
            Task task = (Task) dataAccessService.read(ResourceType.TASK, taskId);
            TaskDetail currentTask = context.getMessage().getHeaders().get("currentTask", TaskDetail.class);
            Map<Long, TaskDetail> sequence = new TreeMap<>();
            for (TaskDetail entry : task.getTaskDetails()) {
                sequence.put(entry.getSequence(), entry);
            }
            if (currentTask == null) {
                for (Map.Entry<Long, TaskDetail> entry : sequence.entrySet()) {
                    if (entry.getValue().getState().equals("PENDING")) {
                        currentTask = entry.getValue();
                        break;
                    }
                }
            }
            if (currentTask.getState().equals("COMPLETED")) {
                currentTask = sequence.get(currentTask.getSequence() + 1);
            }
            context.getExtendedState().getVariables().put("currentTask", currentTask);
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }*/
        return true;
    }
}
