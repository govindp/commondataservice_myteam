package com.incs83.app.statemachine;

import org.springframework.statemachine.config.model.*;

import java.util.ArrayList;
import java.util.Collection;

public class ApplicationStateMachineModelFactory implements StateMachineModelFactory<String, String> {

    @Override
    public StateMachineModel<String, String> build() {
        ConfigurationData<String, String> configurationData = new ConfigurationData<>();
        Collection<StateData<String, String>> stateData = new ArrayList<>();
        stateData.add(new StateData<>("NEW", true));
        stateData.add(new StateData<>("READY"));
        stateData.add(new StateData<>("ASSIGNED"));
        stateData.add(new StateData<>("PROCESSING"));
        stateData.add(new StateData<>("COMPLETED"));
        StatesData<String, String> statesData = new StatesData<>(stateData);
        Collection<TransitionData<String, String>> transitionData = new ArrayList<>();
        transitionData.add(new TransitionData<>("NEW", "READY", "INITIALIZE"));
        transitionData.add(new TransitionData<>("READY", "ASSIGNED", "ASSIGN"));
        transitionData.add(new TransitionData<>("ASSIGNED", "PROCESSING", "APPROVE"));
        transitionData.add(new TransitionData<>("PROCESSING", "COMPLETED", "FINISH"));
        TransitionsData<String, String> transitionsData = new TransitionsData<>(transitionData);
        StateMachineModel<String, String> stateMachineModel = new DefaultStateMachineModel<>(configurationData,
                statesData, transitionsData);
        return stateMachineModel;
    }

    @Override
    public StateMachineModel<String, String> build(String machineId) {
        return build();
    }
}
