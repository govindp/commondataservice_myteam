package com.incs83.app.statemachine;

import com.incs83.logger.LoggerFactory;
import org.apache.log4j.Logger;
import org.springframework.messaging.Message;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.listener.StateMachineListenerAdapter;
import org.springframework.statemachine.state.State;
import org.springframework.statemachine.transition.Transition;

/**
 * Created by rosrivas on 7/23/17.
 */
public class StateMachineEventListener
        extends StateMachineListenerAdapter<String, String> {

    private static final Logger LOG = LoggerFactory.make();

    @Override
    public void stateChanged(State<String, String> from, State<String, String> to) {
        LOG.info("State changed...");
    }

    @Override
    public void stateEntered(State<String, String> state) {
    }

    @Override
    public void stateExited(State<String, String> state) {
    }

    @Override
    public void transition(Transition<String, String> transition) {
    }

    @Override
    public void transitionStarted(Transition<String, String> transition) {
    }

    @Override
    public void transitionEnded(Transition<String, String> transition) {
    }

    @Override
    public void stateMachineStarted(StateMachine<String, String> stateMachine) {
    }

    @Override
    public void stateMachineStopped(StateMachine<String, String> stateMachine) {
    }

    @Override
    public void eventNotAccepted(Message<String> event) {
    }

    @Override
    public void extendedStateChanged(Object key, Object value) {
    }

    @Override
    public void stateMachineError(StateMachine<String, String> stateMachine, Exception exception) {
    }

    @Override
    public void stateContext(StateContext<String, String> stateContext) {

    }
}
