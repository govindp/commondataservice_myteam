package com.incs83.app.statemachine;

import com.incs83.app.business.AccountService;
import com.incs83.app.entities.Task;
import com.incs83.app.entities.TaskDetail;
import com.incs83.app.enums.ResourceType;
import com.incs83.app.service.generic.DataAccessService;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.action.Action;

import javax.persistence.PersistenceContext;
import java.util.Map;
import java.util.Objects;

/**
 * Created by rosrivas on 7/23/17.
 */
@SuppressWarnings({"unused","rawtypes"})
public class ActionExecutor implements Action<String, String> {

	@Autowired
    private DataAccessService dataAccessService;

    @Autowired
    private AccountService accountService;

    @Override
    public void execute(StateContext<String, String> context) {
        try {
            System.out.println("Hurray");
            if (Objects.isNull(context.getMessage())) {
                return;
            }
            return;
            /* TaskDetail currentTask = (TaskDetail) context.getExtendedState().getVariables().get("currentTask");
            System.out.println("Task: " + currentTask.getSequence() + " name: " + currentTask.getName());
            String taskId = context.getMessage().getHeaders().get("task", String.class);
            //currentTask.setState("COMPLETED");
           // dataAccessService.update(ResourceType.TASK_DETAIL, currentTask);
            String token = null;
            if (context.getMessage().getHeaders().containsKey("token")) {
                token = context.getMessage().getHeaders().get("token", String.class);
            } else {
                token = "Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbkA4M2luY3MuY29tIiwianRpIjoiZGNlZWQyYzYyMjMyNDU3NDhiNzAwNThkMDRlMjliYTkiLCJzY29wZSI6W3sibmFtZSI6IlJPTEVfU1lTX0FETUlOIiwic2VydmljZSI6IkNMT1VEU1RPUkUsQk1TLEhDTSIsInRlbmFudCI6ImNsb3Vkc3RvcmUiLCJlbnRpdGxlbWVudHMiOlt7InJlc291cmNlIjoiQUxMIiwicGVybWlzc2lvbnMiOlsiQUxMIl19XX1dLCJpc3MiOiJodHRwOi8vZW50ZXJwcmlzZTgzLmNvbSIsImlhdCI6MTUwMjA2NTkzNCwiZXhwIjoxNTIwMDY5NTM0fQ.ODh7Q0AlY5b-tTJdQJmU_vow8EIvS-VdU8436vq3EIJ2TXD0w70gcopTgNvCqY2iHj3izz4jQS_R8qeailrBAQ";
            }
            CloseableHttpClient httpClient = HttpClients.createDefault();
            if (currentTask.getEndpoint() == null) {
                currentTask.setState("COMPLETED");
                dataAccessService.update(ResourceType.TASK_DETAIL, currentTask);
                return;
            }
            /* HttpPost request = new HttpPost("http://localhost:9999" + currentTask.getEndpoint() + "?id=" + task.getId());
            request.addHeader("X-Authorization", token);
            request.addHeader("Content-Type", "application/json");
            JSONObject object = new JSONObject();
            object.put("id", task.getId());
            StringEntity entity = new StringEntity(currentTask.getPayload());
            request.setEntity(entity);
            HttpResponse httpResponse = httpClient.execute(request); */
           /*  HttpPost eventRequest = new HttpPost("http://localhost:9999" + "/83incs/api/v1/sm/event");
            eventRequest.addHeader("X-Authorization", token);
            eventRequest.addHeader("Content-Type", "application/json");
            JSONObject object = new JSONObject();
            object.put("taskId", taskId);
            object.put("subTaskId", currentTask.getId());
            object.put("service", "CDS");
            object.put("event", "SUCCESS");
           /*  if (httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                currentTask.setState("COMPLETED");
                dataAccessService.update(ResourceType.TASK_DETAIL, currentTask);
                object.put("event", "SUCCESS");
            } else {
                currentTask.setState("FAILED");
                dataAccessService.update(ResourceType.TASK_DETAIL, currentTask);
                object.put("event", "FAILURE");
            }
            StringEntity entity = new StringEntity(object.toString());
            eventRequest.setEntity(entity);
            HttpResponse httpResponse  = httpClient.execute(eventRequest);
            if (httpResponse.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
                throw new IllegalStateException("Unable to change state.");
            } */
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }

}
