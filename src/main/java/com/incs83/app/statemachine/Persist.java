/*
 * Copyright 2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.incs83.app.statemachine;

import com.incs83.app.constants.queries.TaskSQL;
import com.incs83.app.entities.Task;
import com.incs83.app.entities.TaskDetail;
import com.incs83.app.enums.ResourceType;
import com.incs83.app.service.generic.DataAccessService;
import com.incs83.app.statemachine.PersistStateMachineHandler.PersistStateChangeListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.state.State;
import org.springframework.statemachine.transition.Transition;

import java.util.*;

/**
 * Created by rosrivas on 7/2/17.
 */
@SuppressWarnings({"rawtypes","unchecked"})
public class Persist {

    private final PersistStateMachineHandler handler;

	@Autowired
    private DataAccessService dataAccessService;

    private final PersistStateChangeListener listener = new LocalPersistStateChangeListener();

    public Persist(PersistStateMachineHandler handler) {
        this.handler = handler;
        this.handler.addPersistStateChangeListener(listener);
    }

	public void change(String parent, String child, String event, String service, Map<String, Object> headers) {
        try {
            HashMap<String,String> params = new HashMap<>();
            params.put("id",parent);
            params.put("service",service);
            List<Task> tasks = (List<Task>) dataAccessService.read(ResourceType.TASK, TaskSQL.TASK_BY_ID_AND_SERVICE, params);
            Task task = tasks.get(0);
            MessageBuilder builder = MessageBuilder.withPayload(event);
            if (headers != null) {
                headers.forEach((key, value) -> {
                    builder.setHeader(key, value);
                });
            }
            String state = task.getState();
            if (child != null) {
                TaskDetail detail = (TaskDetail) dataAccessService.read(ResourceType.TASK_DETAIL, child);
                state = detail.getState();
                builder.setHeader("detail", detail.getId());
            }
            builder.setHeader("task", task.getId());
            handler.handleEventWithState(builder.build(), state);
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }

    private class LocalPersistStateChangeListener implements PersistStateChangeListener {

        @Override
        public void onPersist(State<String, String> state, Message<String> message,
                              Transition<String, String> transition, StateMachine<String, String> stateMachine) {
            if (message != null) {
                try {
                    if (message.getHeaders().containsKey("task")) {
                        if (message.getHeaders().containsKey("detail")) {
                            String detailId = message.getHeaders().get("detail", String.class);
                            TaskDetail taskDetail = (TaskDetail) dataAccessService.read(ResourceType.TASK_DETAIL, detailId);
                            taskDetail.setState(state.getId());
                            dataAccessService.update(ResourceType.TASK_DETAIL, taskDetail);
                        } else {
                            String taskId = message.getHeaders().get("task", String.class);
                            Task task = (Task) dataAccessService.read(ResourceType.TASK, taskId);
                            task.setState(state.getId());
                            dataAccessService.update(ResourceType.TASK, task);
                        }
                    }
                } catch (Exception e) {
                    throw new IllegalStateException(e);
                }
            }
        }
    }
}

