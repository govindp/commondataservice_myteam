package com.incs83.app.service.data;

import com.cloudinary.Cloudinary;
import com.incs83.app.config.CloudinaryConfig;
import com.incs83.app.enums.ApiResponseCode;
import com.incs83.app.security.exceptions.ApiException;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@SuppressWarnings("rawtypes")
public class CloudinaryService {

    public static final Logger log = LogManager.getLogger(CloudinaryService.class);

    @Autowired
    private CloudinaryConfig cloudinaryConfig;

    private static Cloudinary client = null;


    @PostConstruct
    public Cloudinary getClient() {

        Map<Object, Object> config = new HashMap<>();
        config.put("cloud_name", cloudinaryConfig.getName());
        config.put("api_key", cloudinaryConfig.getKey());
        config.put("api_secret", cloudinaryConfig.getSecret());
        client = new Cloudinary(config);
        return client;

    }

    public Map<?, ?> uploadImage(Object bytesOrUrl, String imageName, List<String> tags) {
        if ((bytesOrUrl instanceof byte[]) || (bytesOrUrl instanceof String) || (bytesOrUrl instanceof File)) {
            try {
                String commaSeparatedTags = String.join(",", tags);
                Map<String, String> imageConfig = getImageConfig(imageName, commaSeparatedTags);
                return client.uploader().upload(bytesOrUrl, imageConfig);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            throw new ApiException(ApiResponseCode.FILE_UPLOAD_FAILED);
        }
        return null;
    }

	public void deleteImage(String imageName) {
        try {
            client.uploader().destroy(imageName, new HashMap());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Map<String, String> getImageConfig(String imageName, String commaSeparatedTags) {
        Map<String, String> imageConfig = new HashMap<>();
        imageConfig.put("public_id", imageName);
        imageConfig.put("tags", commaSeparatedTags);
        imageConfig.put("folder", cloudinaryConfig.getFolder());
        return imageConfig;
    }
}
