package com.incs83.app.service.data;

import com.incs83.logger.LoggerFactory;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Service
public class HTTPService {

    private static final Logger LOG = LoggerFactory.make();

    public HttpResponse doPost(String url, HashMap<String,String> headers, JSONObject jsonObject, HttpEntity reqEntity) {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpResponse res = null;
        LOG.info("Going to make a Post Call to URL " + url);
        HttpPost request = new HttpPost(url);
        for (Map.Entry<String, String> entry : headers.entrySet()) {
            request.addHeader(entry.getKey(), entry.getValue());
        }
        try{
            if(jsonObject!=null){
                request.setEntity(new StringEntity(jsonObject.toString()));
            }else if(reqEntity!=null) {
                request.setEntity(reqEntity);
            }
            res = httpClient.execute(request);
        }catch (IOException e){
            LOG.error("Failed to make a HTTP Request: "+e);
        }
        return res;
    }
}
