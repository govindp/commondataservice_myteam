package com.incs83.app.service.data;

import com.incs83.app.entities.User;
import com.incs83.app.enums.ResourceType;
import com.incs83.app.mt.MongoTenantTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.CriteriaDefinition;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * Created by rosrivas on 6/23/17.
 */
@Service
@SuppressWarnings("unchecked")
public class MongoService<T> {

    @Autowired
    MongoTenantTemplate mongoTemplate;

    public List<T> read(ResourceType type, Map<String, String> keys) {
        return mongoTemplate.findAll(type.getClazz());
    }

    public T read(ResourceType type, Object resourceId) {
        Object object = mongoTemplate.findById(resourceId, type.getClazz());
        return (T) type.getClazz().cast(object);
    }

    public boolean create(ResourceType type, T resource) {
        mongoTemplate.save(resource, getCollectionName(type));
        return true;
    }

    public boolean create(ResourceType type, List<T> resources) {
        mongoTemplate.insert(resources, getCollectionName(type));
        return true;
    }

    public void delete(ResourceType type, Object resourceId) {
        CriteriaDefinition definition = Criteria.where("_id").is(resourceId);
        Query query = Query.query(definition);
        mongoTemplate.findAndRemove(query, type.getClazz(), getCollectionName(type));
    }

    public boolean update(ResourceType type, T resource) {
        String id = ((User) resource).getId();
        delete(type, id);
        mongoTemplate.save(resource, getCollectionName(type));
        return true;
    }

    private String getCollectionName(ResourceType type) {
        Document document = (Document) type.getClazz().getAnnotation(Document.class);
        String collection = document.collection();
        if (collection.trim().isEmpty()) {
            collection = type.getClazz().getSimpleName().toLowerCase();
        }
        return collection;
    }
}
