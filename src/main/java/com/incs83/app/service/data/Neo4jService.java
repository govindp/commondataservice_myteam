package com.incs83.app.service.data;

import com.incs83.app.common.ContactRequest;
import com.incs83.app.common.ShareContactsRequest;
import com.incs83.app.context.ExecutionContext;
import com.incs83.app.entities.Account;
import com.incs83.app.entities.AccountRequisitionRequest;
import com.incs83.app.entities.AccountTenant;
import com.incs83.app.entities.User;
import com.incs83.app.enums.*;
import com.incs83.app.mt.Neo4jTemplate;
import com.incs83.app.responsedto.dto.ContactListDTO;
import com.incs83.app.templates.Neo4jDao;
import com.incs83.app.templates.Neo4jQueries;
import com.incs83.logger.LoggerFactory;
import org.apache.log4j.Logger;
import org.neo4j.driver.v1.Record;
import org.neo4j.driver.v1.Session;
import org.neo4j.driver.v1.StatementResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.MessageFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by hari on 2/8/17.
 */
@Service
@SuppressWarnings({"rawtypes","unchecked"})
public class Neo4jService {

    private static final Logger LOG = LoggerFactory.make();

    @Autowired
    private Neo4jTemplate neo4JTemplate;

    @Autowired
    private Neo4jDao neo4JDao;
    
	@Autowired
    MongoService mongoService;

    public void createUser(User u, Object obj) throws IllegalAccessException {
        AccountRequisitionRequest arr = null;
        if (Objects.nonNull(obj)) {
            if (obj.getClass().getSimpleName().equals("AccountRequisitionRequest")) {
                arr = (AccountRequisitionRequest) obj;
            }
        }
        boolean saveFlag;
        Map<String, String> fromFields = new HashMap<>();
        fromFields.put("id", u.getId());
        Map<String, String> fields = new HashMap<>();
        fields.put("tenantName", arr == null ? ExecutionContext.get().getUsercontext().getAuthority().getTenant() : arr.getCompanyName());
        Session sess = neo4JTemplate.getNeo4jSession();
        saveFlag = neo4JDao.createNode(u, null, sess);
        if (!saveFlag) {
            saveFlag = neo4JDao.createRelationship(User.class.getSimpleName(), fromFields, "AccountTenant", fields, RelationshipTypeEnum.LINKED, null, sess);
            if (!saveFlag) {
                neo4JTemplate.closeSession(sess);
            }
        }
    }

    public boolean createAccountTenantRelation(Account account, AccountTenant accountTenant) throws IllegalAccessException {
        boolean saveFlag;
        Session sess = neo4JTemplate.getNeo4jSession();
        saveFlag = neo4JDao.createNode(accountTenant, null, sess);
        if (!saveFlag) { // Exception for Neo4J since it gives Inverted Result
            saveFlag = neo4JDao.createNode(account, null, sess);
            if (!saveFlag) {
                Map<String, String> toFields = new HashMap<>();
                toFields.put("id", account.getId());
                Map<String, String> fromFields = new HashMap<>();
                fromFields.put("id", accountTenant.getId());
                saveFlag = neo4JDao.createRelationship(AccountTenant.class, fromFields, Account.class, toFields, RelationshipTypeEnum.OWNS, null, sess);
                if (!saveFlag) {
                    Map<String, String> fields = new HashMap<>();
                    fields.put("name", "CDS");
                    saveFlag = neo4JDao.createNode("Service", fields, sess);
                    if (!saveFlag) {
                        saveFlag = neo4JDao.createRelationship(Account.class.getSimpleName(), toFields, "Service", fields, RelationshipTypeEnum.OWNS, null, sess);
                        if (!saveFlag) {
                            neo4JTemplate.closeSession(sess);
                            saveFlag = true;
                        }
                    }
                }
            }
        }
        return saveFlag;
    }

	public void uploadContacts(List<Object> contactList, String userId, ContactsType type) throws IllegalAccessException {

        Map<String, String> fields = new HashMap<>();
        Map<String, String> toFields = new HashMap<>();
        Map<String, String> fromFields = new HashMap<>();
        Map<String, String> userFields = new HashMap<>();

        Session session = neo4JTemplate.getNeo4jSession();

        toFields.put("id", userId);
        for (Object contactObj : contactList) {
            User contact = (User)contactObj;
            // Logic to Check already existing contact
            userFields.put("firstName", contact.getFirstName());
            userFields.put("lastName", contact.getLastName());
            Record record = neo4JDao.getNode(User.class, userFields, session);
            if (record != null) {
                contact.setId((String) record.values().get(0).asMap().get("id"));
            } else {
                neo4JDao.createNode(contact, null, session);
            }
            fromFields.put("id", contact.getId());
            neo4JDao.createRelationship(User.class, fromFields, User.class, toFields,
                    RelationshipTypeEnum.LINKED, fields, session);
            mongoService.create(ResourceType.USER,contact);
        }
        session.close();
    }

    public HashMap<String,ContactListDTO> viewMyContacts(ContactRequest contactRequest) {
        Session sess = neo4JTemplate.getNeo4jSession();
        contactRequest.setSortBy("A." + contactRequest.getSortBy());
        List<String> queries = new ArrayList<>();
        String query = MessageFormat.format(Neo4jQueries.FETCH_MY_LINKED_CONNECTIONS,
                ExecutionContext.CONTEXT.get().getUsercontext().getId());
        String query1 = MessageFormat.format(Neo4jQueries.FETCH_MY_LINKED_CONNECTIONS_REVERSE,
                ExecutionContext.CONTEXT.get().getUsercontext().getId());
        String filteredQuery = MessageFormat.format(Neo4jQueries.GENERIC_COUNT_QUERY, query);
        String filteredQuery1 = MessageFormat.format(Neo4jQueries.GENERIC_COUNT_QUERY, query1);
        StatementResult result = neo4JDao.executeQuery(filteredQuery, sess);
        StatementResult result1 = neo4JDao.executeQuery(filteredQuery1, sess);
        Long linkedCount = (Long) result.single().asMap().get("count");
        Long linkedCount1 = (Long) result1.single().asMap().get("count");
        if (linkedCount != 0) {
            queries.add(query);
        }
        if (linkedCount1 != 0) {
            queries.add(query1);
        }
        query = MessageFormat.format(Neo4jQueries.FETCH_MY_SHARED_CONNECTIONS,
                ExecutionContext.CONTEXT.get().getUsercontext().getId());
        filteredQuery = MessageFormat.format(Neo4jQueries.GENERIC_COUNT_QUERY, query);
        result = neo4JDao.executeQuery(filteredQuery, sess);
        Long sharedCount = (Long) result.single().asMap().get("count");
        if (sharedCount != 0) {
            queries.add(query);
        }

        query = MessageFormat.format(Neo4jQueries.FETCH_MY_COMPANY_LINKED_CONNECTIONS,
                ExecutionContext.CONTEXT.get().getUsercontext().getId());
        filteredQuery = MessageFormat.format(Neo4jQueries.GENERIC_COUNT_QUERY, query);
        result = neo4JDao.executeQuery(filteredQuery, sess);
        Long companyCount = (Long) result.single().asMap().get("count");
        if (companyCount != 0) {
            queries.add(query);
        }
        String collection = "collect(c { .*, relation:r {.*, type:type(r) }})";
        if (contactRequest.getFilter().equals("l") || contactRequest.getFilter().equals("g")){
            return generateQuadrantResponseForConnects(getContactsFromGraph(queries, query, collection, filteredQuery, sess, result, contactRequest));
        }else if (contactRequest.getFilter().equals("q")){
            return generateQuadrantResponseForConnects(getContactsFromGraph(queries, query, collection, filteredQuery, sess, result, contactRequest));
        }else {
            return generateQuadrantResponseForConnects(getContactsFromGraph(queries, query, collection, filteredQuery, sess, result, contactRequest));
        }
    }

    public List<Map<String, Object>> viewCompanyContacts(ContactRequest contactRequest) {
        Session sess = neo4JTemplate.getNeo4jSession();
        contactRequest.setSortBy("A." + contactRequest.getSortBy());

        List<String> queries = new ArrayList<>();
        String query = MessageFormat.format(Neo4jQueries.FETCH_MY_COMPANY,
                ExecutionContext.CONTEXT.get().getUsercontext().getId());
        StatementResult result = neo4JDao.executeQuery(query, sess);
        List<Record> records = result.list();
        if (records.isEmpty()) {
            throw new IllegalStateException("Company record not found for user.");
        }
        String companyId = (String) records.get(0).values().get(0).asMap().get("id");

        query = MessageFormat.format(Neo4jQueries.FETCH_MY_COMPANY_LINKED_CONNECTIONS, companyId);
        String filteredQuery = MessageFormat.format(Neo4jQueries.GENERIC_COUNT_QUERY, query);
        result = neo4JDao.executeQuery(filteredQuery, sess);
        Long linkedCount = (Long) result.single().asMap().get("count");
        if (linkedCount != 0) {
            queries.add(query);
        }

        query = MessageFormat.format(Neo4jQueries.FETCH_MY_COMPANY_SHARED_CONNECTIONS, companyId);
        filteredQuery = MessageFormat.format(Neo4jQueries.GENERIC_COUNT_QUERY, query);
        result = neo4JDao.executeQuery(filteredQuery, sess);
        Long sharedCount = (Long) result.single().asMap().get("count");
        if (sharedCount != 0) {
            queries.add(query);
        }

        List<Map<String, Object>> contacts = new ArrayList<>();
        String collection = "collect(c { .*, relation:r {.*, type:type(r) }})";
        contacts = getContactsFromGraph(queries, query, collection, filteredQuery, sess, result, contactRequest);
        sess.close();
        return contacts;

    }

    public void share(ShareContactsRequest shareContactsRequest) {
        Session session = neo4JTemplate.getNeo4jSession();
        String query = null;
        List<Record> records = null;
        StatementResult result = null;
        if (shareContactsRequest.getType().equals("COMPANY")) {
            for (String shareWith : shareContactsRequest.getShareWith()) {
                Map<String, String> toFields = new HashMap<>();
                query = MessageFormat.format(Neo4jQueries.FETCH_COMPANY_BY_ID, shareWith);
                result = neo4JDao.executeQuery(query, session);
                records = result.list();
                if (records.isEmpty()) {
                    LOG.warn("No company node exist with name : " + shareWith + " to share the contact with.");
                    continue;
                }
                toFields.put("id", String.valueOf(records.get(0).values().get(0).asMap().get("id")));
                Map<String, String> fields = new HashMap<>();
                /*fields.put("by", userId);
                fields.put("type", "COMPANY");*/
                for (String connect : shareContactsRequest.getConnects()) {
                    Map<String, String> fromFields = new HashMap<>();
                    fromFields.put("id", connect);
                    /*fields.put("to", String.valueOf(records.get(0).values().get(0).asMap().get("id")));
                    fields.put("from", connect);
                    fields.put("on", DateUtils.parseDatetoMMDDYYYY(new Date(), '/'));*/
                    neo4JDao.createRelationship(User.class, fromFields, Account.class, toFields,
                            RelationshipTypeEnum.SHARED, fields, session);
                }
            }
        } else if (shareContactsRequest.getType().equals("USER")) {
            for (String shareWith : shareContactsRequest.getShareWith()) {
                Map<String, String> toFields = new HashMap<>();
                query = MessageFormat.format(Neo4jQueries.FETCH_USER_BY_ID,
                        shareWith);
                result = neo4JDao.executeQuery(query, session);
                records = result.list();
                if (records.isEmpty()) {
                    LOG.warn("No user node exist with id : " + shareWith + " to share the contact with.");
                    continue;
                }
                toFields.put("id", String.valueOf(records.get(0).values().get(0).asMap().get("id")));
                Map<String, String> fields = new HashMap<>();
                /*fields.put("by", userId);
                fields.put("type", "USER");*/
                for (String connect : shareContactsRequest.getConnects()) {
                    query = MessageFormat.format(Neo4jQueries.FETCH_USER_BY_ID,
                            connect);
                    result = neo4JDao.executeQuery(query, session);
                    records = result.list();
                    if (records.isEmpty()) {
                        LOG.warn("No user node exist with id : " + connect);
                        continue;
                    }
                    Map<String, String> fromFields = new HashMap<>();
                    fromFields.put("id", connect);
                    /*fields.put("to", String.valueOf(records.get(0).values().get(0).asMap().get("id")));
                    fields.put("from", connect);
                    fields.put("on", DateUtils.parseDatetoMMDDYYYY(new Date(), '/'));*/
                    neo4JDao.createRelationship(User.class, fromFields, User.class, toFields,
                            RelationshipTypeEnum.SHARED, fields, session);
                }
            }
        }
        session.close();
    }

    public void unshare(ShareContactsRequest shareContactsRequest) {
        Session session = neo4JTemplate.getNeo4jSession();
        List<String> froms = shareContactsRequest.getConnects();
        List<String> toes = shareContactsRequest.getShareWith();
        String deleteQuery = null;
        if (shareContactsRequest.getType().equals("COMPANY")) {
            deleteQuery = "MATCH (f:User '{'id:''{0}'''}')-[r:SHARED]-(t:Account '{'id:''{1}'''}') DELETE r";
        } else if (shareContactsRequest.getType().equals("USER")) {
            deleteQuery = "MATCH (f:User '{'id:''{0}'''}')-[r:SHARED]-(t:User '{'id:''{1}'''}') DELETE r";
        }
        for (String from : froms) {
            for (String to : toes) {
                String query = MessageFormat.format(deleteQuery, from, to);
                neo4JDao.executeQuery(query, session);
            }
        }
        neo4JTemplate.closeSession(session);
    }

    private HashMap<String, ContactListDTO> generateQuadrantResponseForConnects(List<Map<String, Object>> contacts) {
        HashMap<String,ContactListDTO> respMap = new HashMap<>();
        ContactListDTO businessContacts = new ContactListDTO();
        ContactListDTO personalContacts = new ContactListDTO();

        // Professional

        businessContacts.setBusinessStrategy(contacts.stream().filter(p -> p.get("quadrant").equals(ProfessionalContactEnum.BUSINESS_STRATEGY.toString())).collect(Collectors.toList())); // Handling Case where the Person does not have any Quadrant Information
        businessContacts.setSalesMarketing(contacts.stream().filter(p -> p.get("quadrant").equals(ProfessionalContactEnum.SALES_MARKETING.toString())).collect(Collectors.toList()));
        businessContacts.setOperations(contacts.stream().filter(p -> p.get("quadrant").equals(ProfessionalContactEnum.OPERATIONS.toString())).collect(Collectors.toList()));
        businessContacts.setEngineering(contacts.stream().filter(p -> p.get("quadrant").equals(ProfessionalContactEnum.ENGINEERING.toString())).collect(Collectors.toList()));
        businessContacts.setTotalCount(contacts.size());

        /// Personal

        personalContacts.setBusinessStrategy(new ArrayList<>());
        personalContacts.setSalesMarketing(new ArrayList<>());
        personalContacts.setOperations(new ArrayList<>());
        personalContacts.setEngineering(new ArrayList<>());

        respMap.put("professional",businessContacts);
        respMap.put("personal",personalContacts);
        return respMap;
    }

    private List<Map<String, Object>> getContactsFromGraph(List<String> queries, String query, String collection, String filteredQuery, Session sess, StatementResult result, ContactRequest contactRequest) {
        List<Map<String, Object>> contacts = new ArrayList<>();
        if (!queries.isEmpty()) {
            StringBuffer combinedQueryBuffer = new StringBuffer();
            for (int i = 0; i < queries.size(); i++) {
                String q = queries.get(i).replace("collect(c)", collection);
                if (i == 0) {
                    combinedQueryBuffer.append(q.replace("ROW", "ROW" + (i + 1)));
                    combinedQueryBuffer.append(" ");
                }
                if (i > 0) {
                    combinedQueryBuffer.append(q.replace(collection + " AS ROW", "ROW" + i + " + " + collection + " AS ROW" + (i + 1)));
                    combinedQueryBuffer.append(" ");
                }
                if (i == queries.size() - 1) {
                    combinedQueryBuffer.append("UNWIND ROW" + (i + 1) + " AS A");
                }
            }
            query = MessageFormat.format(Neo4jQueries.GENERIC_RETURN_QUERY_USER, combinedQueryBuffer.toString());
            filteredQuery = MessageFormat.format(Neo4jQueries.GENERIC_FILTER_QUERY, query, contactRequest.getSortBy(), contactRequest.getOrderBy(),
                    contactRequest.getPage() * contactRequest.getLimit(), contactRequest.getLimit());
            result = neo4JDao.executeQuery(filteredQuery, sess);

            List<Record> resultContacts = result.list();
            for (Record record : resultContacts) {
                HashMap<String,Object> contactMap = new HashMap<>();
                contactMap.put("firstName",record.values().get(0).toString().replaceAll("\"",""));
                contactMap.put("lastName",record.values().get(1).toString().replaceAll("\"",""));
                contactMap.put("imageUrl",record.values().get(2).toString().replaceAll("\"",""));
                contactMap.put("title",record.values().get(3).toString().replaceAll("\"",""));
                contactMap.put("company",record.values().get(4).toString().replaceAll("\"",""));
                contactMap.put("mobilePhone",record.values().get(5).toString().replaceAll("\"",""));
                contactMap.put("deskPhone",record.values().get(6).toString().replaceAll("\"",""));
                contactMap.put("quadrant",record.values().get(7).toString().replaceAll("\"",""));
                contactMap.put("potential",record.values().get(9).toString().replaceAll("\"",""));
                contactMap.put("touch",record.values().get(8).toString().replaceAll("\"",""));
                contactMap.put("id",record.values().get(10).toString().replaceAll("\"",""));
                contactMap.put("email",record.values().get(11).toString().replaceAll("\"",""));
                contacts.add(contactMap);
            }
        }
        sess.close();
        return contacts;
    }


}
