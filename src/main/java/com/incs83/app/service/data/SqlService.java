package com.incs83.app.service.data;


import com.incs83.app.dao.*;
import com.incs83.app.entities.*;
import com.incs83.app.enums.ResourceType;
import com.incs83.logger.LoggerFactory;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;


/**
 * Created by jayant on 24/4/17.
 */
@Service
@SuppressWarnings({ "unchecked", "incomplete-switch" })
public class SqlService<T> {

    @Autowired
    private Repository<Role> roleRepository;

    @Autowired
    private Repository<User> userRepository;

    @Autowired
    private Repository<TaskTemplate> taskTemplateRepository;

    @Autowired
    private Repository<Task> taskRepository;

    @Autowired
    private Repository<AccountRequisitionRequest> accountRequisitionRequestRepository;

    @Autowired
    private Repository<Account> accountRepository;

    @Autowired
    private Repository<AccountTenant> accountTenantRepository;

    @Autowired
    private Repository<AccountTenantDetails> accountTenantDetailsRepository;

    @Autowired
    private Repository<TaskDetail> taskDetailRepository;

    private static final Logger LOG = LoggerFactory.make();

    public Iterable<T> read(ResourceType type, String query, HashMap<String, String> params) throws Exception {

        List<T> resourceList = new ArrayList<T>();
        switch (type) {
            case TASK_TEMPLATE:
                LOG.debug("Resource Accessed : TASK_TEMPLATE, Action : Read(by Params)");
                List<TaskTemplate> taskTemplates = taskTemplateRepository.read(query, params);
                for (TaskTemplate t : taskTemplates) {
                    resourceList.add((T) type.getClazz().cast(t));
                }
                break;
            case TASK:
                LOG.debug("Resource Accessed : TASK, Action : Read(by Params)");
                List<Task> tasks = taskRepository.read(query, params);
                for (Task r : tasks) {
                    resourceList.add((T) type.getClazz().cast(r));
                }
                break;
            case ROLE:
                LOG.debug("Resource Accessed : ROLE, Action : Read(by Params)");
                List<Role> roles = roleRepository.read(query, params);
                for (Role r : roles) {
                    resourceList.add((T) type.getClazz().cast(r));
                }
                break;
            case USER:
                LOG.debug("Resource Accessed : USER, Action : Read(by Params)");
                List<User> user = userRepository.read(query, params);
                for (User u : user) {
                    resourceList.add((T) type.getClazz().cast(u));
                }
                break;
            case ACCOUNT_REQUEST:
                LOG.debug("Resource Accessed : ROLE, Action : Read(by Params)");
                List<AccountRequisitionRequest> ar = accountRequisitionRequestRepository.readDefault(query, params);
                for (AccountRequisitionRequest a : ar) {
                    resourceList.add((T) type.getClazz().cast(a));
                }
                break;
            case ACCOUNT:
                break;
            case ACCOUNT_TENANT:
                LOG.debug("Resource Accessed : ROLE, Action : Read(by Params)");
                List<AccountTenant> at = accountTenantRepository.readDefault(query, params);
                for (AccountTenant a : at) {
                    resourceList.add((T) type.getClazz().cast(a));
                }
                break;
            case ACCOUNT_TENANT_DETAILS:
                break;
        }
        return resourceList;
    }

    public T read(ResourceType type, String resourceId) throws Exception {
        T resource = null;
        switch (type) {
            case TASK_TEMPLATE:
                LOG.debug("Resource Accessed : TASK_TEMPLATE, Action : Read(id)");
                Object state = taskTemplateRepository.read(type.getClazz(), resourceId);
                resource = (T) state;
                break;
            case TASK:
                LOG.debug("Resource Accessed : TASK, Action : Read(id)");
                Object task = taskRepository.read(type.getClazz(), resourceId);
                resource = (T) task;
                break;
            case TASK_DETAIL:
                LOG.debug("Resource Accessed : TASK_DETAIL, Action : Read(id)");
                Object taskDetail = taskDetailRepository.read(type.getClazz(),resourceId);
                resource = (T) taskDetail;
                break;
            case ROLE:
                LOG.debug("Resource Accessed : ROLE, Action : Read(id)");
                Object role = roleRepository.read(type.getClazz(), resourceId);
                resource = (T) role;
                break;
            case USER:
                LOG.debug("Resource Accessed : USER, Action : Read(id)");
                Object user = userRepository.read(type.getClazz(), resourceId);
                resource = (T) user;
                break;
            case ACCOUNT_REQUEST:
                LOG.debug("Resource Accessed : ACCOUNT_REQUEST, Action : Read(id)");
                Object accountRequest = accountRequisitionRequestRepository.readDefault(type.getClazz(), resourceId);
                resource = (T) accountRequest;
                break;
            case ACCOUNT:
                break;
            case ACCOUNT_TENANT:
                break;
            case ACCOUNT_TENANT_DETAILS:
                break;
        }
        return resource;
    }

    public Iterable<T> read(ResourceType type) throws Exception {

        List<T> resourceList = new ArrayList<T>();
        switch (type) {
            case TASK_TEMPLATE:
                LOG.debug("Resource Accessed : TASK_TEMPLATE, Action : ReadAll");
                List<TaskTemplate> taskTemplates = taskTemplateRepository.read(type.getClazz());
                for (TaskTemplate r : taskTemplates) {
                    resourceList.add((T) type.getClazz().cast(r));
                }
                break;
            case TASK:
                LOG.debug("Resource Accessed : TASK, Action : ReadAll");
                List<Task> tasks = taskRepository.read(type.getClazz());
                for (Task r : tasks) {
                    resourceList.add((T) type.getClazz().cast(r));
                }
                break;
            case ROLE:
                LOG.debug("Resource Accessed : ROLE, Action : ReadAll");
                List<Role> roles = roleRepository.read(type.getClazz());
                for (Role r : roles) {
                    resourceList.add((T) type.getClazz().cast(r));
                }
                break;
            case USER:
                LOG.debug("Resource Accessed : USER,  Action : ReadAll");
                List<User> users = userRepository.read(type.getClazz());
                for (User u : users) {
                    resourceList.add((T) type.getClazz().cast(u));
                }
                break;
            case ACCOUNT_REQUEST:
                LOG.debug("Resource Accessed : ACCOUNT, Action : Read(query)");
                List<AccountRequisitionRequest> accountReqRequest = accountRequisitionRequestRepository.readDefault(type.getClazz());
                for (AccountRequisitionRequest a : accountReqRequest) {
                    resourceList.add((T) type.getClazz().cast(a));
                }
                break;
            case ACCOUNT:
                LOG.debug("Resource Accessed : ACCOUNT, Action : Read(query)");
                List<Account> accounts = accountRepository.readDefault(type.getClazz());
                for (Account a : accounts) {
                    resourceList.add((T) type.getClazz().cast(a));
                }
                break;
            case ACCOUNT_TENANT:
                LOG.debug("Resource Accessed : ACCOUNT_TENANT, Action : Read(query)");
                List<AccountTenant> accountTenants = accountTenantRepository.readDefault(type.getClazz());
                for (AccountTenant a : accountTenants) {
                    resourceList.add((T) type.getClazz().cast(a));
                }
                break;
            case ACCOUNT_TENANT_DETAILS:
                break;
        }
        return resourceList;
    }

    public boolean create(ResourceType type, T resource) throws Exception {
        boolean saveFlag = false;
        switch (type) {
            case TASK_TEMPLATE:
                LOG.debug("Resource Accessed : TASK_TEMPLATE, Action : Create");
                TaskTemplate taskTemplate = (TaskTemplate) resource;
                if (Objects.nonNull(taskTemplateRepository.create(taskTemplate))) {
                    saveFlag = true;
                }
                break;
            case TASK_DETAIL:
                LOG.debug("Resource Accessed : TASK_DETAIL, Action : Create");
                TaskDetail taskDetail = (TaskDetail) resource;
                if (Objects.nonNull(taskDetailRepository.create(taskDetail))) {
                    saveFlag = true;
                }
                break;
            case TASK:
                LOG.debug("Resource Accessed : TASK, Action : Create");
                Task task = (Task) resource;
                if (Objects.nonNull(taskRepository.create(task))) {
                    saveFlag = true;
                }
                break;
            case ROLE:
                LOG.debug("Resource Accessed : ROLE, Action : Create");
                Role role = (Role) resource;
                if (Objects.nonNull(roleRepository.create(role))) {
                    saveFlag = true;
                }
                break;
            case USER:
                LOG.debug("Resource Accessed : USER, Action : Create");
                User user = (User) resource;
                if (Objects.nonNull(userRepository.create(user))) {
                    saveFlag = true;
                }
                break;
            case ACCOUNT_REQUEST:
                LOG.debug("Resource Accessed : ACCOUNT_REQUEST");
                AccountRequisitionRequest accountRequisitionRequest = (AccountRequisitionRequest) resource;
                if (Objects.nonNull(accountRequisitionRequestRepository.createDefault(accountRequisitionRequest))) {
                    saveFlag = true;
                }
                break;
            case ACCOUNT:
                LOG.debug("Resource Accessed : ACCOUNT");
                Account account = (Account) resource;
                if (Objects.nonNull(accountRepository.createDefault(account))) {
                    saveFlag = true;
                }
                break;
            case ACCOUNT_TENANT:
                LOG.debug("Resource Accessed : ACCOUNT_TENANT");
                AccountTenant accountTenant = (AccountTenant) resource;
                if (Objects.nonNull(accountTenantRepository.createDefault(accountTenant))) {
                    saveFlag = true;
                }
                break;
            case ACCOUNT_TENANT_DETAILS:
                LOG.debug("Resource Accessed : ACCOUNT_TENANT_DETAILS");
                AccountTenantDetails accountTenantDetails = (AccountTenantDetails) resource;
                if (Objects.nonNull(accountTenantDetailsRepository.createDefault(accountTenantDetails))) {
                    saveFlag = true;
                }
                break;
        }
        return saveFlag;
    }

    public void delete(ResourceType type, Object resourceId) throws Exception {
        switch (type) {
            case TASK_TEMPLATE:
                LOG.debug("Resource Accessed : TASK_TEMPLATE, Action : delete");
                taskTemplateRepository.delete(type.getClazz(), resourceId);
                break;
            case TASK:
                LOG.debug("Resource Accessed : TASK, Action : Delete");
                taskRepository.delete(type.getClazz(), resourceId);
                break;
            case ROLE:
                LOG.debug("Resource Accessed : ROLE, Action : delete");
                roleRepository.delete(type.getClazz(), resourceId);
                break;
            case USER:
                LOG.debug("Resource Accessed : USER, Action : Delete");
                userRepository.delete(type.getClazz(), resourceId);
                break;
            case ACCOUNT_REQUEST:
                break;
            case ACCOUNT:
                break;
            case ACCOUNT_TENANT:
                break;
            case ACCOUNT_TENANT_DETAILS:
                break;
        }
    }

    public boolean update(ResourceType type, T resource) throws Exception {
        boolean updateFlag = false;
        switch (type) {
            case TASK_DETAIL:
                LOG.debug("Resource Accessed : TASK_DETAIL, Action : Update");
                TaskDetail detail = (TaskDetail) resource;
                if (taskDetailRepository.update(detail) != null) {
                    updateFlag = true;
                }
                break;
            case TASK_TEMPLATE:
                LOG.debug("Resource Accessed : TASK_TEMPLATE, Action : Update");
                TaskTemplate taskTemplate = (TaskTemplate) resource;
                if (taskTemplateRepository.update(taskTemplate) != null) {
                    updateFlag = true;
                }
                break;
            case TASK:
                LOG.debug("Resource Accessed : TASK, Action : Update");
                Task task = (Task) resource;
                if (taskRepository.update(task) != null) {
                    updateFlag = true;
                }
                break;
            case ROLE:
                LOG.debug("Resource Accessed : ROLE, Action : Update");
                Role role = (Role) resource;
                if (roleRepository.update(role) != null) {
                    updateFlag = true;
                }
                break;
            case USER:
                LOG.debug("Resource Accessed : USER, Action : Update");
                User user = (User) resource;
                if (userRepository.update(user) != null) {
                    updateFlag = true;
                }
                break;
            case ACCOUNT_REQUEST:
                LOG.debug("Resource Accessed : ACCOUNT CREATION REQUEST, Action : Update");
                AccountRequisitionRequest accountRequisitionRequest = (AccountRequisitionRequest) resource;
                if (accountRequisitionRequestRepository.updateDefault(accountRequisitionRequest) != null) {
                    updateFlag = true;
                }
                break;
            case ACCOUNT:
                break;
            case ACCOUNT_TENANT:
                break;
            case ACCOUNT_TENANT_DETAILS:
                break;
        }
        return updateFlag;
    }

}
