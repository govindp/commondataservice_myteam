package com.incs83.app.service.data;

import com.hazelcast.core.HazelcastInstance;
import com.incs83.app.enums.ResourceType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Objects;

/**
 * Created by jayant on 07/09/16.
 */
@Service
@SuppressWarnings({"unchecked"})
public class CacheService<T> {

    @Autowired
    private HazelcastInstance hazelcastInstance;

    public Iterable<T> read(ResourceType type, Map<String, String> keys) {
        Iterable<T> resource = null;
        if (Objects.nonNull(hazelcastInstance)) {
            resource = (Iterable<T>) hazelcastInstance.getMap("USER_MAP").get("users");
        }
        return resource;
    }

    public Iterable<T> read(ResourceType type) {
        return null;
    }

    public T read(ResourceType type, Object resourceId) {
        return null;
    }

    public T read(ResourceType type, String query) {
        return null;
    }

    public T read(ResourceType type, Object flag, Object resourceId) {
        return null;
    }

    public boolean create(ResourceType type, T resource) {
        return false;
    }

    public void delete(ResourceType type, Object resourceId) {
    }

    public boolean update(ResourceType type, T resource) {
        return false;
    }

}
