package com.incs83.app.service.application;

import com.incs83.app.constants.ApplicationConstants;
import com.incs83.app.templates.EmailTemplate;
import com.sendgrid.SendGrid;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Jayant Puri on 11-07-2017.
 */
@Service
public class MailService {

    private static final Logger log = LogManager.getLogger(MailService.class);

    @Autowired
    private SendGrid sendGrid;

    @Autowired
    private Environment environment;


    @Async
    private void sendMail(EmailTemplate emailTemplate) {
        try {
            if (Boolean.valueOf(environment.getProperty(ApplicationConstants.APP_MAIL_CONFIGURED))) {
                EmailTemplate.Data data = emailTemplate.getData();
                SendGrid.Email email = EmailTemplate.getEmailObject(data);
                log.debug("#### Sending mail => " + data.toString());
                SendGrid.Response response = sendGrid.send(email);
                log.debug("code=> " + response.getCode() + " status=> " + response.getStatus() + " message=> " + response.getMessage());
                if (!response.getStatus()) {
                    for (int i = 1; i < emailTemplate.getMaxAttempt(); i++) {
                        response = sendGrid.send(email);
                        log.debug("code=> " + response.getCode() + " status=> " + response.getStatus() + " message=> " + response.getMessage());
                        if (response.getStatus()) {
                            break;
                        }
                    }
                }
            }
        } catch (Exception e) {
            log.error("### Error while sending email : " + e.getMessage());
        }
    }

    public void sendWelcomeEmail(String email, Map<String, String> valuesMap) {
        if (Boolean.valueOf(environment.getProperty(ApplicationConstants.APP_MAIL_CONFIGURED))) {
            EmailTemplate emailTemplate = EmailTemplate.WELCOME_EMAIL;
            Map<String, String> templateData = new HashMap<>();
            templateData.put("-SERVICES-", valuesMap.get("services"));
            EmailTemplate.Data data = new EmailTemplate.Data(templateData, 3, email);
            emailTemplate.setEmailData(data);
            sendMail(emailTemplate);
        }
    }

    public void sendOnboardingEmail(String email, HashMap<String, String> valuesMap) {
        if (Boolean.valueOf(environment.getProperty(ApplicationConstants.APP_MAIL_CONFIGURED))) {
            EmailTemplate emailTemplate = EmailTemplate.ACCOUNT_ONBOARDING;
            Map<String, String> templateData = new HashMap<>();
            templateData.put("-USER-", valuesMap.get("user"));
            templateData.put("-URL-", valuesMap.get("url"));
            templateData.put("-USERNAME-", valuesMap.get("username"));
            EmailTemplate.Data data = new EmailTemplate.Data(templateData, 3, email);
            emailTemplate.setEmailData(data);
            sendMail(emailTemplate);
        }
    }
}
