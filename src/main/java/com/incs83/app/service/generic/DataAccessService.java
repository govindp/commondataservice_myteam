package com.incs83.app.service.generic;

import java.util.HashMap;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.stereotype.Service;

import com.incs83.app.constants.ApplicationConstants;
import com.incs83.app.enums.ResourceType;
import com.incs83.app.service.data.CacheService;
import com.incs83.app.service.data.MongoService;
import com.incs83.app.service.data.SqlService;

/**
 * Created by jayant on 07/09/16.
 */
@Service
@SuppressWarnings({"rawtypes","unchecked"})
public class DataAccessService<T> {

    @Autowired
    private CacheService cacheService;

    @Autowired
    private SqlService databaseService;

    @Autowired
    private Environment environment;

    @Autowired
    private MongoService mongoService;

    public Iterable<T> read(ResourceType type, String query, HashMap<String, String> params) throws Exception {
        Iterable<T> resources = null;
        if (Boolean.valueOf(environment.getProperty(ApplicationConstants.CACHE_AVAILABLE))) {
            resources = (Iterable<T>) cacheService.read(type, query, params);
        }
        if (Objects.isNull(resources)) {
            resources = (Iterable<T>) databaseService.read(type, query, params);
        }
        return resources;
    }

    public T read(ResourceType type, String resourceId) throws Exception {
        Object resource = null;
        if (Boolean.valueOf(environment.getProperty(ApplicationConstants.CACHE_AVAILABLE))) {
            resource = cacheService.read(type, resourceId);
        }
        if (Objects.isNull(resource)) {
            resource = databaseService.read(type, resourceId);
        }
        return (T) resource;
    }

    public T readFromMongo(ResourceType type, String resourceId) throws Exception {
        Object resource = null;
        if (Boolean.valueOf(environment.getProperty(ApplicationConstants.CACHE_AVAILABLE))) {
            resource = cacheService.read(type, resourceId);
        }
        if (Objects.isNull(resource)) {
            resource = mongoService.read(type, resourceId);
        }
        return (T) resource;
    }

    public Iterable<T> read(ResourceType type) throws Exception {
        Iterable<T> resources = null;
        if (Boolean.valueOf(environment.getProperty(ApplicationConstants.CACHE_AVAILABLE))) {
            resources = (Iterable<T>) cacheService.read(type);
        }
        if (Objects.isNull(resources)) {
            resources = (Iterable<T>) databaseService.read(type);
        }
        return resources;
    }

    public boolean create(ResourceType type, T resource) throws Exception {
        boolean result = false;
        result =  databaseService.create(type, resource);
        if (type.getClazz().isAnnotationPresent(Document.class)) {
            result = mongoService.create(type, resource);
        }
        if (Boolean.valueOf(environment.getProperty(ApplicationConstants.CACHE_AVAILABLE))) {
            result = cacheService.create(type, resource);
        }
        return result;
    }

    public void delete(ResourceType type, Object resourceId) throws Exception {
        databaseService.delete(type, resourceId);
        if (Boolean.valueOf(environment.getProperty(ApplicationConstants.CACHE_AVAILABLE))) {
            cacheService.delete(type, resourceId);
        }
        if (type.getClazz().isAnnotationPresent(Document.class)) {
            mongoService.delete(type, resourceId);
        }
    }

    public boolean update(ResourceType type, T resource) throws Exception {
        boolean result = false;
        result = databaseService.update(type, resource);
        if (type.getClazz().isAnnotationPresent(Document.class)) {
            result = mongoService.update(type, resource);
        }
        if (Boolean.valueOf(environment.getProperty(ApplicationConstants.CACHE_AVAILABLE))) {
            result = cacheService.update(type, resource);
        }
        return result;
    }

    public boolean update(ResourceType type, String query) throws Exception {
        boolean result = false;
        result = databaseService.update(type, query);
        if (Boolean.valueOf(environment.getProperty(ApplicationConstants.CACHE_AVAILABLE))) {
            result = cacheService.update(type, query);
        }
        return result;
    }
}
