package com.incs83.app.service.generic;

import com.incs83.logger.LoggerFactory;
import org.apache.log4j.Logger;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by hari on 28/7/17.
 */
@SuppressWarnings("unchecked")
public class AuthenticationFilterService {

    private static final Logger LOG = LoggerFactory.make();

    private static ConcurrentHashMap<String, ConcurrentHashMap<String, String>> authenticatedResources;
    
	public static void uploadAuthenticationProperties() {

        Properties properties = new Properties();
        authenticatedResources = new ConcurrentHashMap<>();
        try (InputStream inputStream = new ClassPathResource("authentication.properties").getInputStream()) {
            properties.load(inputStream);
            Enumeration<String> enumeration = (Enumeration<String>) properties.propertyNames();
            List<String> cloudStore = Arrays.asList(properties.getProperty("CLOUDSTORE").split(","));
            List<String> cdsResources = Arrays.asList(properties.getProperty("CDS").split(","));
            while (enumeration.hasMoreElements()) {
                ConcurrentHashMap<String, String> map = new ConcurrentHashMap<>();
                List<String> resources = new ArrayList<>();
                String service = enumeration.nextElement();
                if (service.equals("CLOUDSTORE")) {
                    resources.addAll(cloudStore);
                    for (String resource : resources) {
                        map.put(resource, service);
                    }
                } else if (service.equals("CDS")) {
                    resources.addAll(cdsResources);
                    for (String resource : resources) {
                        map.put(resource, service);
                    }
                } else {
                    resources.addAll(cdsResources);
                    resources.addAll(Arrays.asList(properties.getProperty(service).split(",")));
                    for (String resource : resources) {
                        map.put(resource, service);
                    }
                }
                authenticatedResources.put(service, map);
            }
            LOG.debug("AUTHENTICATED RESOURCES MAP : " + authenticatedResources);
        } catch (IOException e) {
            LOG.error("config file not exist on main/resources/authentication.properties");
        }
    }

    public static ConcurrentHashMap<String, ConcurrentHashMap<String, String>> getAuthenticatedResources() {
        return authenticatedResources;
    }

    public static void setAuthenticatedResources(ConcurrentHashMap<String, ConcurrentHashMap<String, String>> authenticatedResources) {
        AuthenticationFilterService.authenticatedResources = authenticatedResources;
    }
}
