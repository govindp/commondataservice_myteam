package com.incs83.app.service.generic;

import com.incs83.app.constants.ApplicationConstants;
import com.incs83.app.service.generic.components.HibernateService;
import org.hibernate.engine.jdbc.connections.spi.ConnectionProvider;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Created by jayant on 09/09/16.
 */
@Component
public class ConnectionProviderService implements ConnectionProvider {

    /**
     *
     */
    private static final long serialVersionUID = -5698996471658672233L;
    private final DriverManagerDataSource basicDataSource = new DriverManagerDataSource();

    public ConnectionProviderService() {
    }

    public ConnectionProviderService(String database) {
        String jdbcURL = HibernateService.multiTenantProperties.getProperty(ApplicationConstants.JDBC_URL);
        jdbcURL = jdbcURL.replace(ApplicationConstants.DATABASE, database);
        this.basicDataSource.setUrl(jdbcURL);
        this.basicDataSource.setUsername(HibernateService.multiTenantProperties.get(ApplicationConstants.DB_USERNAME).toString());
        this.basicDataSource.setPassword(HibernateService.multiTenantProperties.get(ApplicationConstants.DB_PASSWORD).toString());
    }

    @Override
    public Connection getConnection() throws SQLException {
        return this.basicDataSource.getConnection();
    }

    @Override
    public void closeConnection(Connection connection) throws SQLException {
        connection.close();
    }

    @Override
    public boolean supportsAggressiveRelease() {
        return false;
    }

    @SuppressWarnings("rawtypes")
    @Override
    public boolean isUnwrappableAs(Class aClass) {
        return false;
    }

    @Override
    public <T> T unwrap(Class<T> aClass) {
        return null;
    }
}
