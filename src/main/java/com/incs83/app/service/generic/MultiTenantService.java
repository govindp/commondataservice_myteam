/**
 * @Author Jayant Puri
 * @Created 11-Apr-2017
 */
package com.incs83.app.service.generic;

import com.incs83.app.mt.CurrentTenantIdentifier;
import org.hibernate.engine.jdbc.connections.spi.AbstractMultiTenantConnectionProvider;
import org.hibernate.engine.jdbc.connections.spi.ConnectionProvider;
import org.springframework.stereotype.Service;

@Service
public class MultiTenantService extends AbstractMultiTenantConnectionProvider {
    private static final long serialVersionUID = 3305310005148709133L;

    @Override
    protected ConnectionProvider getAnyConnectionProvider() {
        return new ConnectionProviderService(CurrentTenantIdentifier.DEFAULT_TENANT);
    }

    @Override
    protected ConnectionProvider selectConnectionProvider(String s) {
        return new ConnectionProviderService(s);
    }
}