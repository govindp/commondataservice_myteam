package com.incs83.app.service.generic.components;

import com.hazelcast.client.HazelcastClient;
import com.hazelcast.client.config.ClientConfig;
import com.hazelcast.client.config.ClientNetworkConfig;
import com.hazelcast.core.HazelcastInstance;
import com.incs83.app.config.CacheConfig;
import com.incs83.app.constants.ApplicationConstants;
import com.incs83.logger.LoggerFactory;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

/**
 * Created by jayant on 4/5/17.
 */
@Component
public class HazelCastService {

    private static final Logger LOG = LoggerFactory.make();

    @Autowired
    private Environment environment;

    @Autowired
    private CacheConfig cacheConfig;

    private static HazelcastInstance hazelcastInstance = null;

    @Bean
    public HazelcastInstance getInstance() {
        if (hazelcastInstance == null) {
            if (Boolean.valueOf(environment.getProperty(ApplicationConstants.CACHE_AVAILABLE))) {
                int maxTries = 1;
                final ClientNetworkConfig networkConfig = new ClientNetworkConfig();
                networkConfig.addAddress(cacheConfig.getHostname());
                final ClientConfig clientConfig = new ClientConfig();
                clientConfig.setNetworkConfig(networkConfig);
                while (maxTries <= 3) {
                    LOG.error("Trying to Connect to Hazelcast :: Try No. " + maxTries);
                    try {
                        hazelcastInstance = HazelcastClient.newHazelcastClient(clientConfig);
                        break;
                    } catch (Exception e) {
                        LOG.error("Error Connecting to Cache Will retry Max 3 times");
                        try {
                            Thread.sleep(5000);
                            maxTries++;
                        } catch (InterruptedException e1) {
                            e1.printStackTrace();
                        }
                    }
                }
            }
            if (hazelcastInstance == null) {
                LOG.info("Cache Not available....");
            }
        }
        return hazelcastInstance;
    }
}
