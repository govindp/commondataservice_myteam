package com.incs83.app.mt;

import org.hibernate.cfg.DefaultNamingStrategy;

/**
 * Created by rosrivas on 7/16/17.
 */
public class ServiceNamingStrategy extends DefaultNamingStrategy {

	private static final long serialVersionUID = -8701942146594665933L;

	@Override
    public String tableName(String tableName) {

        return tableName + "_" + getServiceName();
    }

    private String getServiceName() {
        return "cds";
    }
}
