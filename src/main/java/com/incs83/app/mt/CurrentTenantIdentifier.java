/**
 * @Author Jayant Puri
 * @Created 11-Apr-2017
 */
package com.incs83.app.mt;

import org.hibernate.context.spi.CurrentTenantIdentifierResolver;

public class CurrentTenantIdentifier implements CurrentTenantIdentifierResolver {

    public static ThreadLocal<String> _tenantIdentifier = new ThreadLocal<>();
    public static ThreadLocal<String> _tenantName = new ThreadLocal<>();

    public static String DEFAULT_TENANT = "cloudstore";

    @Override
    public String resolveCurrentTenantIdentifier() {
        String tenantId = _tenantIdentifier.get();
        if (tenantId == null || tenantId.equals(DEFAULT_TENANT)) {
            tenantId = DEFAULT_TENANT;
        }
        return tenantId;
    }

    @Override
    public boolean validateExistingCurrentSessions() {
        return true;
    }
}