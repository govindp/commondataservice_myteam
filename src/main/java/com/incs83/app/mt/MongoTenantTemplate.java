package com.incs83.app.mt;

import com.incs83.app.config.MongoConfig;
import com.mongodb.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.geo.GeoResults;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.*;
import org.springframework.data.mongodb.core.BulkOperations.BulkMode;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.aggregation.TypedAggregation;
import org.springframework.data.mongodb.core.convert.MongoConverter;
import org.springframework.data.mongodb.core.mapreduce.GroupBy;
import org.springframework.data.mongodb.core.mapreduce.GroupByResults;
import org.springframework.data.mongodb.core.mapreduce.MapReduceOptions;
import org.springframework.data.mongodb.core.mapreduce.MapReduceResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.NearQuery;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.data.util.CloseableIterator;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class MongoTenantTemplate extends MongoTemplate {
    private static Map<String, MongoTemplate> tenantTemplates = new HashMap<>();

    @Autowired
    public MongoConfig mongoConfig;

    public MongoTenantTemplate(MongoDbFactory mongoDbFactory) {
        super(mongoDbFactory);
        tenantTemplates.put(mongoDbFactory.getDb().getName(), new MongoTemplate(mongoDbFactory));
    }

    protected MongoTemplate getTenantMongoTemplate(String tenant) {
        if (tenant == null) {
            tenant = CurrentTenantIdentifier.DEFAULT_TENANT;
        }
        MongoTemplate mongoTemplate = tenantTemplates.get(tenant);
        if (mongoTemplate == null) {
            ServerAddress serverAddress = new ServerAddress(mongoConfig.getMongoDBHost(), mongoConfig.getMongoPortNumber());
            /*List mongoCredentials = new ArrayList<MongoCredential>();
            mongoCredentials.add(MongoCredential.createCredential(mongoConfig.getMongoUserName(), mongoConfig.getMongoDatabase(),
                    mongoConfig.getMongoPassword().toCharArray()));*/
            SimpleMongoDbFactory mongoDbFactory = new SimpleMongoDbFactory(
                    new MongoClient(serverAddress/*,mongoCredentials*/), tenant);
            mongoTemplate = new MongoTemplate(mongoDbFactory);
            tenantTemplates.put(tenant, mongoTemplate);
        }
        return mongoTemplate;
    }

    @Override
    public String getCollectionName(Class<?> entityClass) {
        return getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).getCollectionName(entityClass);
    }

    @Override
    public CommandResult executeCommand(String jsonCommand) {
        return getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).executeCommand(jsonCommand);
    }

    @Override
    public CommandResult executeCommand(DBObject command) {
        return getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).executeCommand(command);
    }

    @Override
    @SuppressWarnings("deprecation")
    public CommandResult executeCommand(DBObject command, int options) {
        return getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).executeCommand(command, options);
    }

    @Override
    public CommandResult executeCommand(DBObject command, ReadPreference readPreference) {
        return getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).executeCommand(command, readPreference);
    }

    @Override
    public void executeQuery(Query query, String collectionName, DocumentCallbackHandler dch) {
        getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).executeQuery(query, collectionName, dch);

    }

    @Override
    public <T> T execute(DbCallback<T> action) {
        return getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).execute(action);
    }

    @Override
    public <T> T execute(Class<?> entityClass, CollectionCallback<T> action) {
        return getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).execute(entityClass, action);
    }

    @Override
    public <T> T execute(String collectionName, CollectionCallback<T> action) {
        return getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).execute(collectionName, action);
    }

    @SuppressWarnings("deprecation")
    @Override
    public <T> T executeInSession(DbCallback<T> action) {
        return getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).executeInSession(action);
    }

    @Override
    public <T> CloseableIterator<T> stream(Query query, Class<T> entityType) {
        return getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).stream(query, entityType);
    }

    @Override
    public <T> DBCollection createCollection(Class<T> entityClass) {
        return getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).createCollection(entityClass);
    }

    @Override
    public <T> DBCollection createCollection(Class<T> entityClass, CollectionOptions collectionOptions) {
        return getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).createCollection(entityClass, collectionOptions);
    }

    @Override
    public DBCollection createCollection(String collectionName) {
        return getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).createCollection(collectionName);
    }

    @Override
    public DBCollection createCollection(String collectionName, CollectionOptions collectionOptions) {
        return getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).createCollection(collectionName, collectionOptions);
    }

    @Override
    public Set<String> getCollectionNames() {
        return getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).getCollectionNames();
    }

    @Override
    public DBCollection getCollection(String collectionName) {
        return getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).getCollection(collectionName);
    }

    @Override
    public <T> boolean collectionExists(Class<T> entityClass) {
        return getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).collectionExists(entityClass);
    }

    @Override
    public boolean collectionExists(String collectionName) {
        return getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).collectionExists(collectionName);
    }

    @Override
    public <T> void dropCollection(Class<T> entityClass) {
        getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).dropCollection(entityClass);
    }

    @Override
    public void dropCollection(String collectionName) {
        getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).dropCollection(collectionName);

    }

    @Override
    public IndexOperations indexOps(String collectionName) {
        return getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).indexOps(collectionName);
    }

    @Override
    public IndexOperations indexOps(Class<?> entityClass) {
        return getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).indexOps(entityClass);
    }

    @Override
    public ScriptOperations scriptOps() {
        return getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).scriptOps();
    }

    @Override
    public BulkOperations bulkOps(BulkMode mode, String collectionName) {
        return getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).bulkOps(mode, collectionName);
    }

    @Override
    public BulkOperations bulkOps(BulkMode mode, Class<?> entityType) {
        return getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).bulkOps(mode, entityType);
    }

    @Override
    public BulkOperations bulkOps(BulkMode mode, Class<?> entityType, String collectionName) {
        return getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).bulkOps(mode, entityType, collectionName);
    }

    @Override
    public <T> List<T> findAll(Class<T> entityClass) {
        return getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).findAll(entityClass);
    }

    @Override
    public <T> List<T> findAll(Class<T> entityClass, String collectionName) {
        return getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).findAll(entityClass, collectionName);
    }

    @Override
    public <T> GroupByResults<T> group(String inputCollectionName, GroupBy groupBy, Class<T> entityClass) {
        return getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).group(inputCollectionName, groupBy, entityClass);
    }

    @Override
    public <T> GroupByResults<T> group(Criteria criteria, String inputCollectionName, GroupBy groupBy,
                                       Class<T> entityClass) {
        return getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).group(criteria, inputCollectionName, groupBy,
                entityClass);
    }

    @Override
    public <O> AggregationResults<O> aggregate(TypedAggregation<?> aggregation, String collectionName,
                                               Class<O> outputType) {
        return getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).aggregate(aggregation, outputType);
    }

    @Override
    public <O> AggregationResults<O> aggregate(TypedAggregation<?> aggregation, Class<O> outputType) {
        return getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).aggregate(aggregation, outputType);
    }

    @Override
    public <O> AggregationResults<O> aggregate(Aggregation aggregation, Class<?> inputType, Class<O> outputType) {
        return getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).aggregate(aggregation, inputType, outputType);
    }

    @Override
    public <O> AggregationResults<O> aggregate(Aggregation aggregation, String collectionName, Class<O> outputType) {
        return getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).aggregate(aggregation, collectionName, outputType);
    }

    @Override
    public <T> MapReduceResults<T> mapReduce(String inputCollectionName, String mapFunction, String reduceFunction,
                                             Class<T> entityClass) {
        return getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).mapReduce(inputCollectionName, mapFunction,
                reduceFunction, entityClass);
    }

    @Override
    public <T> MapReduceResults<T> mapReduce(String inputCollectionName, String mapFunction, String reduceFunction,
                                             MapReduceOptions mapReduceOptions, Class<T> entityClass) {
        return getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).mapReduce(inputCollectionName, mapFunction,
                reduceFunction, mapReduceOptions, entityClass);
    }

    @Override
    public <T> MapReduceResults<T> mapReduce(Query query, String inputCollectionName, String mapFunction,
                                             String reduceFunction, Class<T> entityClass) {
        return getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).mapReduce(query, inputCollectionName, mapFunction,
                reduceFunction, entityClass);
    }

    @Override
    public <T> MapReduceResults<T> mapReduce(Query query, String inputCollectionName, String mapFunction,
                                             String reduceFunction, MapReduceOptions mapReduceOptions, Class<T> entityClass) {
        return mapReduce(query, inputCollectionName, mapFunction, reduceFunction, mapReduceOptions, entityClass);
    }

    @Override
    public <T> GeoResults<T> geoNear(NearQuery near, Class<T> entityClass) {
        return getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).geoNear(near, entityClass);
    }

    @Override
    public <T> GeoResults<T> geoNear(NearQuery near, Class<T> entityClass, String collectionName) {
        return getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).geoNear(near, entityClass, collectionName);
    }

    @Override
    public <T> T findOne(Query query, Class<T> entityClass) {
        return getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).findOne(query, entityClass);
    }

    @Override
    public <T> T findOne(Query query, Class<T> entityClass, String collectionName) {
        return getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).findOne(query, entityClass, collectionName);
    }

    @Override
    public boolean exists(Query query, String collectionName) {
        return getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).exists(query, collectionName);
    }

    @Override
    public boolean exists(Query query, Class<?> entityClass) {
        return getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).exists(query, entityClass);
    }

    @Override
    public boolean exists(Query query, Class<?> entityClass, String collectionName) {
        return getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).exists(query, entityClass, collectionName);
    }

    @Override
    public <T> List<T> find(Query query, Class<T> entityClass) {
        return getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).find(query, entityClass);
    }

    @Override
    public <T> List<T> find(Query query, Class<T> entityClass, String collectionName) {
        return getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).find(query, entityClass, collectionName);
    }

    @Override
    public <T> T findById(Object id, Class<T> entityClass) {
        return getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).findById(id, entityClass);
    }

    @Override
    public <T> T findById(Object id, Class<T> entityClass, String collectionName) {
        return getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).findById(id, entityClass, collectionName);
    }

    @Override
    public <T> T findAndModify(Query query, Update update, Class<T> entityClass) {
        return getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).findAndModify(query, update, entityClass);
    }

    @Override
    public <T> T findAndModify(Query query, Update update, Class<T> entityClass, String collectionName) {
        return getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).findAndModify(query, update, entityClass,
                collectionName);
    }

    @Override
    public <T> T findAndModify(Query query, Update update, FindAndModifyOptions options, Class<T> entityClass) {
        return getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).findAndModify(query, update, options, entityClass);
    }

    @Override
    public <T> T findAndModify(Query query, Update update, FindAndModifyOptions options, Class<T> entityClass,
                               String collectionName) {
        return getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).findAndModify(query, update, options, entityClass,
                collectionName);
    }

    @Override
    public <T> T findAndRemove(Query query, Class<T> entityClass) {
        return getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).findAndRemove(query, entityClass);
    }

    @Override
    public <T> T findAndRemove(Query query, Class<T> entityClass, String collectionName) {
        return getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).findAndRemove(query, entityClass, collectionName);
    }

    @Override
    public long count(Query query, Class<?> entityClass) {
        return getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).count(query, entityClass);
    }

    @Override
    public long count(Query query, String collectionName) {
        return getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).count(query, collectionName);
    }

    @Override
    public long count(Query query, Class<?> entityClass, String collectionName) {
        return getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).count(query, entityClass, collectionName);
    }

    @Override
    public void insert(Object objectToSave) {
        getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).insert(objectToSave);
    }

    @Override
    public void insert(Object objectToSave, String collectionName) {
        getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).insert(objectToSave, collectionName);
    }

    @Override
    public void insert(Collection<? extends Object> batchToSave, Class<?> entityClass) {
        getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).insert(batchToSave, entityClass);
    }

    @Override
    public void insert(Collection<? extends Object> batchToSave, String collectionName) {
        getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).insert(batchToSave, collectionName);
    }

    @Override
    public void insertAll(Collection<? extends Object> objectsToSave) {
        getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).insertAll(objectsToSave);
    }

    @Override
    public void save(Object objectToSave) {
        getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).save(objectToSave);
    }

    @Override
    public void save(Object objectToSave, String collectionName) {
        getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).save(objectToSave, collectionName);
    }

    @Override
    public WriteResult upsert(Query query, Update update, Class<?> entityClass) {
        return getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).upsert(query, update, entityClass);
    }

    @Override
    public WriteResult upsert(Query query, Update update, String collectionName) {
        return getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).upsert(query, update, collectionName);
    }

    @Override
    public WriteResult upsert(Query query, Update update, Class<?> entityClass, String collectionName) {
        return getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).upsert(query, update, entityClass, collectionName);
    }

    @Override
    public WriteResult updateFirst(Query query, Update update, Class<?> entityClass) {
        return getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).updateFirst(query, update, entityClass);
    }

    @Override
    public WriteResult updateFirst(Query query, Update update, String collectionName) {
        return getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).updateFirst(query, update, collectionName);
    }

    @Override
    public WriteResult updateFirst(Query query, Update update, Class<?> entityClass, String collectionName) {
        return getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).updateFirst(query, update, entityClass,
                collectionName);
    }

    @Override
    public WriteResult updateMulti(Query query, Update update, Class<?> entityClass) {
        return getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).updateMulti(query, update, entityClass);
    }

    @Override
    public WriteResult updateMulti(Query query, Update update, String collectionName) {
        return getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).updateMulti(query, update, collectionName);
    }

    @Override
    public WriteResult updateMulti(Query query, Update update, Class<?> entityClass, String collectionName) {
        return getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).updateMulti(query, update, entityClass,
                collectionName);
    }

    @Override
    public WriteResult remove(Object object) {
        return getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).remove(object);
    }

    @Override
    public WriteResult remove(Object object, String collection) {
        return getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).remove(object, collection);
    }

    @Override
    public WriteResult remove(Query query, Class<?> entityClass) {
        return getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).remove(query, entityClass);
    }

    @Override
    public WriteResult remove(Query query, Class<?> entityClass, String collectionName) {
        return getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).remove(query, entityClass, collectionName);
    }

    @Override
    public WriteResult remove(Query query, String collectionName) {
        return getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).remove(query, collectionName);
    }

    @Override
    public <T> List<T> findAllAndRemove(Query query, String collectionName) {
        return getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).findAllAndRemove(query, collectionName);
    }

    @Override
    public <T> List<T> findAllAndRemove(Query query, Class<T> entityClass) {
        return getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).findAllAndRemove(query, entityClass);
    }

    @Override
    public <T> List<T> findAllAndRemove(Query query, Class<T> entityClass, String collectionName) {
        return getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).findAllAndRemove(query, entityClass, collectionName);
    }

    @Override
    public MongoConverter getConverter() {
        return getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).getConverter();
    }

    @Override
    public DB getDb() {
        return getTenantMongoTemplate(CurrentTenantIdentifier._tenantIdentifier.get()).getDb();
    }

}
