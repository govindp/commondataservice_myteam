package com.incs83.app.mt;

import com.incs83.app.annotations.PreHandle;
import com.incs83.app.business.AccountService;
import com.incs83.app.constants.ApplicationConstants;
import com.incs83.app.context.ExecutionContext;
import com.incs83.app.entities.AccountTenant;
import com.incs83.app.enums.ApiResponseCode;
import com.incs83.app.security.exceptions.AuthEntityNotAllowedException;
import com.incs83.app.security.exceptions.AuthMethodNotSupportedException;
import com.incs83.security.tokenFactory.Authority;
import com.incs83.security.tokenFactory.Entitlement;
import com.incs83.security.tokenFactory.Permission;
import com.incs83.security.tokenFactory.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Objects;

/**
 * Created by rosrivas on 7/2/17.
 */
@Component
public class TenantInterceptor extends HandlerInterceptorAdapter {

    @Autowired
    private AccountService accountService;

    @Autowired
    private Environment environment;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {
        String context = request.getRequestURI().split("/")[1];
        if (context.equals("swagger-resources") || context.equals("v2")) {
            return true;
        }
        if (Boolean.valueOf(environment.getProperty(ApplicationConstants.SECURITY_AVAILABLE))) {
            enableSecuriy(handler, context);
        }
        setDBName(context);
        return super.preHandle(request, response, handler);
    }

    @Override
    public void postHandle(
            HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView)
            throws Exception {
        ExecutionContext.CONTEXT.remove();
        CurrentTenantIdentifier._tenantIdentifier.remove();
    }

    private void setDBName(String context) {
        String dbname = null;
        if (context == null || context.equals("83incs") || context.equals(CurrentTenantIdentifier.DEFAULT_TENANT) || context.equals("error")) {
            dbname = CurrentTenantIdentifier.DEFAULT_TENANT;
        } else {
            AccountTenant accountTenant = accountService.getAccountTenant(context);
            dbname = accountTenant == null ? null : accountTenant.getTenantGuid();
            if (dbname == null) {
                throw new RuntimeException("No Such Tenant As: " + context);
            }
        }
        CurrentTenantIdentifier._tenantIdentifier.set(dbname);
    }

    private synchronized void checkResourceEntitlementForRole(String endpoint, String requestMethod) {
        if (ExecutionContext.CONTEXT.get() == null) {
            return;
        }
        UserContext uc = ExecutionContext.CONTEXT.get().getUsercontext();
        Authority authority = uc.getAuthority();
        Entitlement entitlement = authority.getEntitlements().stream().filter(p -> p.getResource().equals("ALL") || p.getResource().equalsIgnoreCase(endpoint)).findFirst().orElse(null);
        if (Objects.isNull(entitlement)) {
            throw new AuthEntityNotAllowedException(ApiResponseCode.RESOURCE_NOT_ALLOWED);
        }
        List<String> permissions = entitlement.getPermissions();
        Permission requiredPermission = getRequiredPermission(requestMethod);
        String permission = permissions.stream().filter(p -> p.equals("ALL") || p.equalsIgnoreCase(requiredPermission.getValue())).findFirst().orElse(null);
        if (Objects.isNull(permission)) {
            throw new AuthMethodNotSupportedException(ApiResponseCode.RESOURCE_PERMISSION_DENIED);
        }
    }

    private Permission getRequiredPermission(String requestMethod) {
        Permission permission = null;
        switch (requestMethod) {
            case "GET":
                permission = Permission.READ;
                break;
            case "HEAD":
                break;
            case "POST":
                permission = Permission.CREATE;
                break;
            case "PUT":
                permission = Permission.UPDATE;
                break;
            case "PATCH":
                permission = Permission.UPDATE;
                break;
            case "DELETE":
                permission = Permission.DELETE;
                break;
            case "OPTIONS":
                break;
            case "TRACE":
                break;
        }
        return permission;
    }

    private boolean checkTokenValidity(String context,ExecutionContext ec) {
        if ((ec.getUsercontext().getAuthority().getTenant().equals(context)) && !ec.getUsercontext().getAuthority().getTenant().equals("cloudstore")){
            return true;
        }else{
            if (ec.getUsercontext().getAuthority().getTenant().equals("cloudstore")){
                return true;
            }
        }
        return false;
    }

    private void enableSecuriy(Object handler, String context) {
        if(context.equals("error")){

            return;
        }
        ExecutionContext ec = ExecutionContext.CONTEXT.get();
        if(ec==null) {
            return;
        }
        HandlerMethod method = (HandlerMethod) handler;
        PreHandle preHandleAnnotation = method.getMethodAnnotation(PreHandle.class);
        boolean isTokenValid = checkTokenValidity(context,ec);
        if (!isTokenValid) {
            throw new AuthEntityNotAllowedException(ApiResponseCode.RESOURCE_NOT_ALLOWED);
        }
        if (Objects.nonNull(preHandleAnnotation)) {
            checkResourceEntitlementForRole(preHandleAnnotation.resourceType().getClazz().getSimpleName(), preHandleAnnotation.requestMethod().name());
        } else {
            throw new AuthEntityNotAllowedException(ApiResponseCode.RESOURCE_NOT_ALLOWED);
        }
    }
}
