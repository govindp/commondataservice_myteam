package com.incs83.app.mt;

import com.incs83.app.config.Neo4jConfig;
import org.neo4j.driver.v1.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class Neo4jTemplate {

    private static Driver driver;

    @Autowired
    private Neo4jConfig neo4jConfig;

    @SuppressWarnings("deprecation")
    @PostConstruct
    private void init() {
        driver = GraphDatabase.driver("bolt://" + neo4jConfig.getDbHostName() + ":" + neo4jConfig.getPortNumber(),
                AuthTokens.basic(neo4jConfig.getUserName(), neo4jConfig.getPassword()), Config.build().withMaxSessions(Integer.parseInt(neo4jConfig.getPoolSize()))
                        .withEncryptionLevel(Config.EncryptionLevel.NONE).toConfig());
    }

    public Session getNeo4jSession() {
        return driver.session();
    }

    public void closeSession(Session session) {
        if (session != null) {
            session.close();
            session = null;
        }
    }
}
