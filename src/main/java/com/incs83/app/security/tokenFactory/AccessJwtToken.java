package com.incs83.app.security.tokenFactory;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.jsonwebtoken.Claims;


/**
 * @Author Jayant Puri
 * @Created 13-Apr-2017
 */
public final class AccessJwtToken implements JwtToken {
    private final String rawToken;
    @JsonIgnore
    private Claims claims;

    protected AccessJwtToken(final String access_token, Claims claims) {
        this.rawToken = access_token;
        this.claims = claims;
    }

    public String getToken() {
        return this.rawToken;
    }

    public Claims getClaims() {
        return claims;
    }
}
