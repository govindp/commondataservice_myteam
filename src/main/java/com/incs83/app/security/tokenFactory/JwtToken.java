package com.incs83.app.security.tokenFactory;

/**
 * @Author Jayant Puri
 * @Created 13-Apr-2017
 */
public interface JwtToken {
    String getToken();
}
