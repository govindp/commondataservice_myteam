package com.incs83.app.security.endpoint;

import com.incs83.app.config.JwtConfig;
import com.incs83.app.constants.queries.UserSQL;
import com.incs83.app.entities.BaseEntity;
import com.incs83.app.entities.User;
import com.incs83.app.enums.ResourceType;
import com.incs83.app.security.auth.jwt.extractor.TokenExtractor;
import com.incs83.app.security.auth.jwt.verifier.TokenVerifier;
import com.incs83.app.security.config.WebSecurityConfig;
import com.incs83.app.security.exceptions.InvalidJwtToken;
import com.incs83.app.security.tokenFactory.JwtTokenFactory;
import com.incs83.app.security.tokenFactory.RawAccessJwtToken;
import com.incs83.app.security.tokenFactory.RefreshToken;
import com.incs83.app.service.generic.DataAccessService;
import com.incs83.app.utils.application.CommonUtils;
import com.incs83.security.tokenFactory.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;

/**
 * @Author Jayant Puri
 * @Created 13-Apr-2017
 */
@RestController
public class RefreshTokenEndpoint {

    @Autowired
    private JwtTokenFactory tokenFactory;
    @Autowired
    private JwtConfig jwtConfig;
    @Autowired
    private DataAccessService<BaseEntity> dataAccessService;
    @Autowired
    private TokenVerifier tokenVerifier;
    @Autowired
    @Qualifier("jwtHeaderTokenExtractor")
    private TokenExtractor tokenExtractor;

    @RequestMapping(value = "/api/auth/token", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    public
    @ResponseBody
    HashMap<String, String> refreshToken(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String tokenPayload = tokenExtractor.extract(request.getHeader(WebSecurityConfig.JWT_TOKEN_HEADER_PARAM));

        RawAccessJwtToken rawToken = new RawAccessJwtToken(tokenPayload);
        RefreshToken refreshToken = RefreshToken.create(rawToken, jwtConfig.getTokenSigningKey()).orElseThrow(() -> new InvalidJwtToken("Invalid Token"));

        String jti = refreshToken.getJti();
        if (!tokenVerifier.verify(jti)) {
            throw new InvalidJwtToken("Invalid Token");
        }

        String query = UserSQL.GET_USER_BY_EMAIL;
        HashMap<String, String> params = new HashMap<>();
        params.put("email", refreshToken.getSubject());
        User user = (User) dataAccessService.read(ResourceType.USER, query, params).iterator().next();

        if (user == null) {
            throw new UsernameNotFoundException("User not found: " + refreshToken.getSubject());
        }
        if (user.getRole() == null) throw new InsufficientAuthenticationException("User has no roles assigned");

        UserContext userContext = UserContext.create(user.getId(), user.getUsername(), CommonUtils.generateScopePayloadForToken(user));

        HashMap<String, String> refTokenMap = new HashMap<>();
        refTokenMap.put("access_token", tokenFactory.createAccessJwtToken(userContext).getToken());
        refTokenMap.put("access_token", tokenFactory.createAccessJwtToken(userContext).getToken());

        return refTokenMap;
    }
}
