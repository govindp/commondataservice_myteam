package com.incs83.app.security.auth.ajax;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.incs83.app.enums.ApiResponseCode;
import com.incs83.app.security.exceptions.AuthMethodNotSupportedException;
import com.incs83.app.security.exceptions.JwtExpiredTokenException;
import com.incs83.app.utils.system.ResponseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @Author Jayant Puri
 * @Created 13-Apr-2017
 */
@Component
public class AjaxAwareAuthenticationFailureHandler implements AuthenticationFailureHandler {
    private final ObjectMapper mapper;

    @Autowired
    public AjaxAwareAuthenticationFailureHandler(ObjectMapper mapper) {
        this.mapper = mapper;
    }

    @Autowired
    private ResponseUtil responseUtil;

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
                                        AuthenticationException e) throws IOException, ServletException {

        response.setStatus(HttpStatus.UNAUTHORIZED.value());
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        if (e instanceof BadCredentialsException) {
            mapper.writeValue(response.getWriter(), responseUtil.exception(ApiResponseCode.INVALID_USERNAME_PASSWORD));
        } else if (e instanceof JwtExpiredTokenException) {
            mapper.writeValue(response.getWriter(), responseUtil.exception(ApiResponseCode.TOKEN_EXPIRED));
        } else if (e instanceof AuthMethodNotSupportedException) {
            mapper.writeValue(response.getWriter(), responseUtil.exception(ApiResponseCode.GENERIC_UNAUTHORIZED));
        } else {
            mapper.writeValue(response.getWriter(), responseUtil.exception(ApiResponseCode.AUTH_FAILED));
        }
    }
}
