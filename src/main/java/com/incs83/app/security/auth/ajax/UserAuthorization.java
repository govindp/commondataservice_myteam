/**
 * @Author Jayant Puri
 * @Created 26-Apr-2017
 */
package com.incs83.app.security.auth.ajax;

import com.incs83.app.entities.User;
import org.springframework.security.core.GrantedAuthority;

/**
 * @author jayant
 *
 */
public class UserAuthorization implements GrantedAuthority {

    private static final long serialVersionUID = 3733447884331675194L;

    private User user;

    private String authority;

    public User getUser() {
        return user;
    }

    public UserAuthorization setUser(User user) {
        this.user = user;
        return this;
    }

    @Override
    public String getAuthority() {
        return authority;
    }

    public UserAuthorization setAuthority(String authority) {
        this.authority = authority;
        return this;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof UserAuthorization))
            return false;

        UserAuthorization ua = (UserAuthorization) obj;
        return ua.getAuthority() == this.getAuthority() || ua.getAuthority().equals(this.getAuthority());
    }
}