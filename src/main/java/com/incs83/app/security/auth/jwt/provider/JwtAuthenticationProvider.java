package com.incs83.app.security.auth.jwt.provider;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.incs83.app.config.JwtConfig;
import com.incs83.app.context.ExecutionContext;
import com.incs83.app.security.exceptions.InvalidJwtToken;
import com.incs83.app.security.tokenFactory.JwtAuthenticationToken;
import com.incs83.app.security.tokenFactory.RawAccessJwtToken;
import com.incs83.app.service.generic.DataAccessService;
import com.incs83.security.tokenFactory.Authority;
import com.incs83.security.tokenFactory.UserContext;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @Author Jayant Puri
 * @Created 13-Apr-2017
 */
@Component
@SuppressWarnings({"unchecked","rawtypes"})
public class JwtAuthenticationProvider implements AuthenticationProvider {
    private final JwtConfig jwtConfig;

    @Autowired
    public JwtAuthenticationProvider(JwtConfig jwtConfig) {
        this.jwtConfig = jwtConfig;
    }

    @Autowired
    DataAccessService dataAccessService;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        RawAccessJwtToken rawAccessToken = (RawAccessJwtToken) authentication.getCredentials();

        Jws<Claims> jwsClaims = rawAccessToken.parseClaims(jwtConfig.getTokenSigningKey());
        String subject = jwsClaims.getBody().getSubject();
        String id = jwsClaims.getBody().getId();
        List<Authority> authorityList = jwsClaims.getBody().get("scope", List.class);

        if (null == authorityList) {
            throw new InvalidJwtToken("Missing Claims");
        }

        Authority authorityFromToken = null;
        try {
            authorityFromToken = new ObjectMapper().convertValue(authorityList.get(0), Authority.class);
        } catch (Exception e) {
            throw new InvalidJwtToken("Invalid Claims");
        }

        UserContext context = UserContext.create(id, subject, authorityFromToken);
        ExecutionContext ec = new ExecutionContext(context);
        ExecutionContext.CONTEXT.set(ec);
        return new JwtAuthenticationToken(context);
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return (JwtAuthenticationToken.class.isAssignableFrom(authentication));
    }
}
