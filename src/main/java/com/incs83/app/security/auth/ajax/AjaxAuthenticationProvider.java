package com.incs83.app.security.auth.ajax;

import com.incs83.app.constants.queries.UserSQL;
import com.incs83.app.context.ExecutionContext;
import com.incs83.app.entities.BaseEntity;
import com.incs83.app.entities.User;
import com.incs83.app.enums.ResourceType;
import com.incs83.app.service.generic.DataAccessService;
import com.incs83.app.utils.application.CommonUtils;
import com.incs83.security.tokenFactory.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.util.HashMap;

/**
 * @Author Jayant Puri
 * @Created 13-Apr-2017
 */
@Component
public class AjaxAuthenticationProvider implements AuthenticationProvider {
    private final BCryptPasswordEncoder encoder;
    private final DataAccessService<BaseEntity> dataAccessService;

    @Autowired
    public AjaxAuthenticationProvider(final DataAccessService<BaseEntity> dataAccessService, final BCryptPasswordEncoder encoder) {
        this.dataAccessService = dataAccessService;
        this.encoder = encoder;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        Assert.notNull(authentication, "No authentication data provided");

        String username = (String) authentication.getPrincipal();
        String password = (String) authentication.getCredentials();

        String query = UserSQL.GET_USER_BY_EMAIL;
        HashMap<String, String> params = new HashMap<>();
        params.put("email", username);

        User user = null;
        try {
            user = (User) dataAccessService.read(ResourceType.USER, query, params).iterator().next();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (user == null) {
            throw new UsernameNotFoundException("User not found: " + username);
        }

        if (!encoder.matches(password, user.getPassword())) {
            throw new BadCredentialsException("Authentication Failed. Username or Password not valid.");
        }

        if (user.getRole() == null) throw new InsufficientAuthenticationException("User has no roles assigned");

        UserContext userContext = UserContext.create(user.getId(), user.getUsername(), CommonUtils.generateScopePayloadForToken(user));
        ExecutionContext ec = new ExecutionContext(userContext);
        ExecutionContext.CONTEXT.set(ec);

        return new UsernamePasswordAuthenticationToken(userContext, null);
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return (UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication));
    }

}
