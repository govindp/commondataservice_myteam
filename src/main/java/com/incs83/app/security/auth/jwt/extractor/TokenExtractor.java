package com.incs83.app.security.auth.jwt.extractor;

/**
 * @Author Jayant Puri
 * @Created 13-Apr-2017
 */
public interface TokenExtractor {
    public String extract(String payload);
}
