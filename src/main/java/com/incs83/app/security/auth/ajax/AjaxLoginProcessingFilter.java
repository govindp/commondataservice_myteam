package com.incs83.app.security.auth.ajax;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.incs83.app.business.AccountService;
import com.incs83.app.common.LoginRequest;
import com.incs83.app.entities.AccountTenant;
import com.incs83.app.enums.ApiResponseCode;
import com.incs83.app.mt.CurrentTenantIdentifier;
import com.incs83.app.security.exceptions.AuthMethodNotSupportedException;
import com.incs83.app.utils.system.WebUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @Author Jayant Puri
 * @Created 13-Apr-2017
 */
public class AjaxLoginProcessingFilter extends AbstractAuthenticationProcessingFilter {
    private final AuthenticationSuccessHandler successHandler;
    private final AuthenticationFailureHandler failureHandler;

    private final ObjectMapper objectMapper;

    private AccountService accountService;

    public AjaxLoginProcessingFilter(String defaultProcessUrl, AuthenticationSuccessHandler successHandler,
                                     AuthenticationFailureHandler failureHandler, ObjectMapper mapper, AccountService accountService) {
        super(defaultProcessUrl);
        this.successHandler = successHandler;
        this.failureHandler = failureHandler;
        this.objectMapper = mapper;
        this.accountService = accountService;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
            throws AuthenticationException, IOException, ServletException {


        if (!HttpMethod.POST.name().equals(request.getMethod()) || !WebUtils.isAjax(request)) {
            if (logger.isDebugEnabled()) {
                logger.debug("Authentication method not supported. Request method: " + request.getMethod());
            }
            throw new AuthMethodNotSupportedException(ApiResponseCode.RESOURCE_PERMISSION_DENIED);
        }
        String context = request.getRequestURI().toString().split("/")[1];
        boolean setDBResp = setDBName(context.equals("83incs") ? "cloudstore" : context);
        if (!setDBResp) {
            throw new AuthenticationServiceException("No Such Tenant Exists");
        }
        LoginRequest loginRequest = objectMapper.readValue(request.getReader(), LoginRequest.class);

        if (StringUtils.isBlank(loginRequest.getUsername()) || StringUtils.isBlank(loginRequest.getPassword())) {
            throw new AuthenticationServiceException("Username or Password not provided");
        }

        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(loginRequest.getUsername().toLowerCase().trim(), loginRequest.getPassword());

        return this.getAuthenticationManager().authenticate(token);
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
                                            Authentication authResult) throws IOException, ServletException {
        successHandler.onAuthenticationSuccess(request, response, authResult);
    }

    @Override
    protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response,
                                              AuthenticationException failed) throws IOException, ServletException {
        SecurityContextHolder.clearContext();
        failureHandler.onAuthenticationFailure(request, response, failed);
    }

    private boolean setDBName(String context) {
        boolean dbSet = false;
        String dbname = null;
        if (context == null || context.equals("83incs") || context.equals(CurrentTenantIdentifier.DEFAULT_TENANT)) {
            dbname = CurrentTenantIdentifier.DEFAULT_TENANT;
        } else {
            AccountTenant ac = accountService.getAccountTenant(context);
            dbname = ac == null ? null : ac.getTenantGuid();
        }
        if (dbname != null) {
            CurrentTenantIdentifier._tenantIdentifier.set(dbname);
            CurrentTenantIdentifier._tenantName.set(context);
            dbSet = true;
        }
        return dbSet;
    }
}
