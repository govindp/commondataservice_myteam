package com.incs83.app.security.auth.jwt.verifier;

/**
 * @Author Jayant Puri
 * @Created 13-Apr-2017
 */
public interface TokenVerifier {
    public boolean verify(String jti);
}
