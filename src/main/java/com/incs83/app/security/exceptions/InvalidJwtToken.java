package com.incs83.app.security.exceptions;

import org.springframework.security.core.AuthenticationException;

/**
 * @Author Jayant Puri
 * @Created 13-Apr-2017
 */
public class InvalidJwtToken extends AuthenticationException {
    private static final long serialVersionUID = -294671188037098603L;

    public InvalidJwtToken(String msg, Throwable t) {
        super(msg, t);
    }

    public InvalidJwtToken(String msg) {
        super(msg);
    }
}
