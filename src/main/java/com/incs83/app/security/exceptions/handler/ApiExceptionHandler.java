package com.incs83.app.security.exceptions.handler;

import com.incs83.app.enums.ApiResponseCode;
import com.incs83.app.responsedto.ResponseDTO;
import com.incs83.app.security.exceptions.ApiException;
import com.incs83.app.security.exceptions.AuthEntityNotAllowedException;
import com.incs83.app.security.exceptions.AuthMethodNotSupportedException;
import com.incs83.app.security.exceptions.ValidationException;
import com.incs83.app.utils.system.ResponseUtil;
import com.incs83.logger.LoggerFactory;
import org.apache.log4j.Logger;
import org.springframework.beans.TypeMismatchException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageConversionException;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletException;

@ControllerAdvice(basePackages = "com.incs83.app")
@RestController
@SuppressWarnings("rawtypes")
public class ApiExceptionHandler {

    private static final Logger LOGGER = LoggerFactory.make();

    @Autowired
    private ResponseUtil responseUtil;

    @ResponseStatus(value = HttpStatus.OK)
    @ExceptionHandler(value = {ApiException.class})
    public ResponseDTO handleGenericException(ApiException e) {
        LOGGER.error(String.format("IotBackendApi: Got [[%s]] exception with message: %s", e.getClass().getName(), e.getMessage()), e);
        return responseUtil.exception(e.getCode());
    }

    @ResponseStatus(value = HttpStatus.OK)
    @ExceptionHandler(value = {ValidationException.class})
    public ResponseDTO handleGenericException(ValidationException e) {
        LOGGER.error(String.format("IotBackendApi: Got [[%s]] exception with message: %s", e.getClass().getName(), e.getMessage()));
        return responseUtil.validationFailed(e.getCode(), e.getMessage());
    }

    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    @ExceptionHandler(value = {TypeMismatchException.class})
    public ResponseDTO exception(TypeMismatchException e) {
        LOGGER.error(String.format("HS: Got [[%s]] exception with message: %s", e.getClass().getName(), e.getMessage()), e);
        return responseUtil.exception(ApiResponseCode.ERROR_PROCESSING_REQUEST);
    }

    @ResponseStatus(value = HttpStatus.FORBIDDEN)
    @ExceptionHandler(value = {AuthMethodNotSupportedException.class})
    public ResponseDTO exception(AuthMethodNotSupportedException e) {
        LOGGER.error(String.format("HS: Got [[%s]] exception with message: %s", e.getClass().getName(), e.getMessage()), e);
        return responseUtil.exception(e.getCode());
    }

    @ResponseStatus(value = HttpStatus.FORBIDDEN)
    @ExceptionHandler(value = {AuthEntityNotAllowedException.class})
    public ResponseDTO exception(AuthEntityNotAllowedException e) {
        LOGGER.error(String.format("HS: Got [[%s]] exception with message: %s", e.getClass().getName(), e.getMessage()), e);
        return responseUtil.exception(e.getCode());
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = {HttpMessageConversionException.class})
    public ResponseDTO exception(HttpMessageConversionException e) {
        LOGGER.error(String.format("IotBackendApi: Got [[%s]] exception with message: %s", e.getClass().getName(), e.getMessage()), e);
        return responseUtil.exception(ApiResponseCode.ERROR_PROCESSING_REQUEST);
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = {BindException.class})
    public ResponseDTO exception(BindException e) {
        LOGGER.error(String.format("IotBackendApi: Got [[%s]] exception with message: %s", e.getClass().getName(), e.getMessage()), e);
        return responseUtil.exception(ApiResponseCode.ERROR_PROCESSING_REQUEST);
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = {NumberFormatException.class})
    public ResponseDTO exception(NumberFormatException e) {
        LOGGER.error(String.format("IotBackendApi: Got [[%s]] exception with message: %s", e.getClass().getName(), e.getMessage()), e);
        return responseUtil.exception(ApiResponseCode.ERROR_PROCESSING_REQUEST);
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = {ServletException.class})
    public ResponseDTO exception(ServletException e) {
        LOGGER.error(String.format("IotBackendApi: Got [[%s]] exception with message: %s", e.getClass().getName(), e.getMessage()), e);
        return responseUtil.exception(ApiResponseCode.ERROR_PROCESSING_REQUEST);
    }

    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(value = Exception.class)
    public ResponseDTO exception(Exception e) {
        LOGGER.error(String.format("IotBackendApi: Got [[%s]] exception with message: %s", e.getClass().getName(), e.getMessage()), e);
        return responseUtil.exception(ApiResponseCode.ERROR);
    }
}
