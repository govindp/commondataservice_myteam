package com.incs83.app.security.exceptions;

import com.incs83.app.enums.abstraction.ResponseCode;
import org.springframework.security.authentication.AuthenticationServiceException;

/**
 * @Author Jayant Puri
 * @Created 13-Apr-2017
 */
public class AuthEntityNotAllowedException extends AuthenticationServiceException {
    private static final long serialVersionUID = 3705043083010304496L;

    private int code;
    private String message;

    public AuthEntityNotAllowedException(ResponseCode responseCode) {
        super(responseCode.getMessage());
        this.code = responseCode.getCode();
        this.message = responseCode.getMessage();
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
