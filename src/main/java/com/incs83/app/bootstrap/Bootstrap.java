package com.incs83.app.bootstrap;

import com.incs83.app.service.generic.AuthenticationFilterService;
import com.incs83.logger.LoggerFactory;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

/**
 * Created by hari on 28/7/17.
 */
@Component
public class Bootstrap implements InitializingBean {

    private static final Logger log = LoggerFactory.make();

    @Override
    public void afterPropertiesSet() throws Exception {
        log.info("### STARTING BOOTSTRAP ###");
        AuthenticationFilterService.uploadAuthenticationProperties();
        log.info("### BOOTSTRAP DONE ###");
    }
}
