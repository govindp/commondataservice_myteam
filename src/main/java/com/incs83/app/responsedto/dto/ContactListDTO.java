package com.incs83.app.responsedto.dto;

import java.util.List;
import java.util.Map;

@SuppressWarnings("unused")
public class ContactListDTO {

    private List<Map<String, Object>> businessStrategy;
	private int businessStrategyCount;
    private List<Map<String, Object>> salesMarketing;
    private int salesMarketingCount;
    private List<Map<String, Object>> operations;
    private int operationsCount;
    private List<Map<String, Object>> engineering;
    private int engineeringCount;

    private int totalCount;

    public List<Map<String, Object>> getBusinessStrategy() {
        return businessStrategy;
    }

    public void setBusinessStrategy(List<Map<String, Object>> businessStrategy) {
        this.businessStrategy = businessStrategy;
    }

    public int getBusinessStrategyCount() {
        return getBusinessStrategy().size();
    }

    public void setBusinessStrategyCount(int businessStrategyCount) {
        this.businessStrategyCount = businessStrategyCount;
    }

    public List<Map<String, Object>> getSalesMarketing() {
        return salesMarketing;
    }

    public void setSalesMarketing(List<Map<String, Object>> salesMarketing) {
        this.salesMarketing = salesMarketing;
    }

    public int getSalesMarketingCount() {
        return getSalesMarketing().size();
    }

    public void setSalesMarketingCount(int salesMarketingCount) {
        this.salesMarketingCount = salesMarketingCount;
    }

    public List<Map<String, Object>> getOperations() {
        return operations;
    }

    public void setOperations(List<Map<String, Object>> operations) {
        this.operations = operations;
    }

    public int getOperationsCount() {
        return getOperations().size();
    }

    public void setOperationsCount(int operationsCount) {
        this.operationsCount = operationsCount;
    }

    public List<Map<String, Object>> getEngineering() {
        return engineering;
    }

    public void setEngineering(List<Map<String, Object>> engineering) {
        this.engineering = engineering;
    }

    public int getEngineeringCount() {
        return getEngineering().size();
    }

    public void setEngineeringCount(int engineeringCount) {
        this.engineeringCount = engineeringCount;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }
}
