package com.incs83.app.responsedto.dto;

import java.util.StringJoiner;

/**
 * Created by hari on 20/8/17.
 */
public class CompanyDTO {

    private String id;

    private String companyName;

    public CompanyDTO() {
    }

    public CompanyDTO(String id, String companyName) {
        this.id = id;
        this.companyName = companyName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", "CompanyDTO{", "}")
                .add("id='" + id + "'")
                .add("companyName='" + companyName + "'")
                .toString();
    }
}
