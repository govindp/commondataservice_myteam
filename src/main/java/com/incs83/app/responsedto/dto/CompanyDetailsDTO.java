package com.incs83.app.responsedto.dto;

import com.incs83.app.entities.BaseEntity;
import com.incs83.app.entities.Company;
import com.incs83.app.entities.User;
import com.incs83.app.enums.OrbitEnum;
import com.incs83.app.enums.TouchPotentialEnum;

import javax.persistence.Id;
import java.io.Serializable;
import java.util.List;
import java.util.StringJoiner;

/**
 * Created by Jayant on 7/8/17.
 */
public class CompanyDetailsDTO extends BaseEntity implements Serializable {

    private static final long serialVersionUID = -5723258342757016392L;

    @Id
    private String id;

    private String companyName;

    private String website;

    private String contact;

    private String location;

    private String socialMedia;

    private String currentStatus;

    private String association;

    private String relationship;

    private TouchPotentialEnum touch;

    private OrbitEnum orbits;

    private String nearTermPotential;//(<6months)

    private String longTermPotential;//( >6months)

    private String marketLevel;

    private String entityType;

    private String industryType;

    private String revenue;

    private String companySize;

    private String technology;

    private String category;

    private String competitors;

    private List<User> staff;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getSocialMedia() {
        return socialMedia;
    }

    public void setSocialMedia(String socialMedia) {
        this.socialMedia = socialMedia;
    }

    public String getCurrentStatus() {
        return currentStatus;
    }

    public void setCurrentStatus(String currentStatus) {
        this.currentStatus = currentStatus;
    }

    public String getAssociation() {
        return association;
    }

    public void setAssociation(String association) {
        this.association = association;
    }

    public String getRelationship() {
        return relationship;
    }

    public void setRelationship(String relationship) {
        this.relationship = relationship;
    }

    public TouchPotentialEnum getTouch() {
        return touch;
    }

    public void setTouch(TouchPotentialEnum touch) {
        this.touch = touch;
    }

    public OrbitEnum getOrbits() {
        return orbits;
    }

    public void setOrbits(OrbitEnum orbits) {
        this.orbits = orbits;
    }

    public String getNearTermPotential() {
        return nearTermPotential;
    }

    public void setNearTermPotential(String nearTermPotential) {
        this.nearTermPotential = nearTermPotential;
    }

    public String getLongTermPotential() {
        return longTermPotential;
    }

    public void setLongTermPotential(String longTermPotential) {
        this.longTermPotential = longTermPotential;
    }

    public String getMarketLevel() {
        return marketLevel;
    }

    public void setMarketLevel(String marketLevel) {
        this.marketLevel = marketLevel;
    }

    public String getEntityType() {
        return entityType;
    }

    public void setEntityType(String entityType) {
        this.entityType = entityType;
    }

    public String getIndustryType() {
        return industryType;
    }

    public void setIndustryType(String industryType) {
        this.industryType = industryType;
    }

    public String getRevenue() {
        return revenue;
    }

    public void setRevenue(String revenue) {
        this.revenue = revenue;
    }

    public String getCompanySize() {
        return companySize;
    }

    public void setCompanySize(String companySize) {
        this.companySize = companySize;
    }

    public String getTechnology() {
        return technology;
    }

    public void setTechnology(String technology) {
        this.technology = technology;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCompetitors() {
        return competitors;
    }

    public void setCompetitors(String competitors) {
        this.competitors = competitors;
    }

    public List<User> getStaff() {
        return staff;
    }

    public void setStaff(List<User> staff) {
        this.staff = staff;
    }

    public static CompanyDetailsDTO mapFromCompany(Company company, List<User> users) {
        CompanyDetailsDTO companyDetailsDTO = new CompanyDetailsDTO();
        companyDetailsDTO.setId(company.getId());
        companyDetailsDTO.setCompanyName(company.getCompanyName());
        companyDetailsDTO.setWebsite(company.getWebsite());
        companyDetailsDTO.setContact(company.getContact());
        companyDetailsDTO.setLocation(company.getLocation());
        companyDetailsDTO.setSocialMedia(company.getSocialMedia());
        companyDetailsDTO.setCurrentStatus(company.getCurrentStatus());
        companyDetailsDTO.setAssociation(company.getAssociation());
        companyDetailsDTO.setRelationship(company.getRelationship());
        companyDetailsDTO.setTouch(company.getTouch());
        companyDetailsDTO.setOrbits(company.getOrbits());
        companyDetailsDTO.setNearTermPotential(company.getNearTermPotential());
        companyDetailsDTO.setLongTermPotential(company.getLongTermPotential());
        companyDetailsDTO.setMarketLevel(company.getMarketLevel());
        companyDetailsDTO.setEntityType(company.getEntityType());
        companyDetailsDTO.setIndustryType(company.getIndustryType());
        companyDetailsDTO.setRevenue(company.getRevenue());
        companyDetailsDTO.setCompanySize(company.getCompanySize());
        companyDetailsDTO.setTechnology(company.getTechnology());
        companyDetailsDTO.setCategory(company.getCategory());
        companyDetailsDTO.setCompetitors(company.getCompetitors());
        companyDetailsDTO.setStaff(users);
        companyDetailsDTO.setCreatedBy(company.getCreatedBy());
        companyDetailsDTO.setUpdatedBy(company.getUpdatedBy());
        companyDetailsDTO.setCreatedAt(company.getCreatedAt());
        companyDetailsDTO.setUpdatedAt(company.getUpdatedAt());
        return companyDetailsDTO;

    }

    @Override
    public String toString() {
        return new StringJoiner(", ", "Company{", "}")
                .add("id='" + id + "'")
                .add("companyName='" + companyName + "'")
                .add("website='" + website + "'")
                .add("contact='" + contact + "'")
                .add("location='" + location + "'")
                .add("socialMedia='" + socialMedia + "'")
                .add("currentStatus='" + currentStatus + "'")
                .add("association='" + association + "'")
                .add("relationship='" + relationship + "'")
                .add("touch=" + touch)
                .add("orbits=" + orbits)
                .add("nearTermPotential='" + nearTermPotential + "'")
                .add("longTermPotential='" + longTermPotential + "'")
                .add("marketLevel='" + marketLevel + "'")
                .add("entityType='" + entityType + "'")
                .add("industryType='" + industryType + "'")
                .add("revenue='" + revenue + "'")
                .add("companySize='" + companySize + "'")
                .add("technology='" + technology + "'")
                .add("category='" + category + "'")
                .add("competitors='" + competitors + "'")
                .add("staff=" + staff)
                .toString();
    }
}
