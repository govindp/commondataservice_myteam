package com.incs83.app.responsedto.dto;

import java.util.HashMap;
import java.util.Set;

import com.incs83.app.entities.Role;

public class UserProfileDTO {

    Set<Role> role;

    Set<String> service;

    HashMap<String,Object> demographicInfo;

    HashMap<String,Object> personalInfo;

    HashMap<String,Object> professionalInfo;

    HashMap<String,Object> businessInfoInfo;

    public Set<Role> getRole() {
        return role;
    }

    public void setRole(Set<Role> role) {
        this.role = role;
    }

    public Set<String> getService() {
        return service;
    }

    public void setService(Set<String> service) {
        this.service = service;
    }

    public HashMap<String, Object> getDemographicInfo() {
        return demographicInfo;
    }

    public void setDemographicInfo(HashMap<String, Object> demographicInfo) {
        this.demographicInfo = demographicInfo;
    }

    public HashMap<String, Object> getPersonalInfo() {
        return personalInfo;
    }

    public void setPersonalInfo(HashMap<String, Object> personalInfo) {
        this.personalInfo = personalInfo;
    }

    public HashMap<String, Object> getProfessionalInfo() {
        return professionalInfo;
    }

    public void setProfessionalInfo(HashMap<String, Object> professionalInfo) {
        this.professionalInfo = professionalInfo;
    }

    public HashMap<String, Object> getBusinessInfoInfo() {
        return businessInfoInfo;
    }

    public void setBusinessInfoInfo(HashMap<String, Object> businessInfoInfo) {
        this.businessInfoInfo = businessInfoInfo;
    }
}
