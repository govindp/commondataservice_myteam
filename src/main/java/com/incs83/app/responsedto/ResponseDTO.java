package com.incs83.app.responsedto;

@SuppressWarnings("rawtypes")
public interface ResponseDTO<T> {

    int getCode();

    void setCode(int code);

    String getMessage();

	ResponseDTO setMessage(String message);

    T getData();

    void setData(T data);
}
