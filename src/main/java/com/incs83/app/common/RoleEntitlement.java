package com.incs83.app.common;

import com.incs83.security.tokenFactory.Entitlement;
import com.incs83.security.tokenFactory.Permission;

import java.util.List;
import java.util.StringJoiner;
import java.util.stream.Collectors;

public class RoleEntitlement {
    private String resource;
    private List<Permission> permissions;

    public String getResource() {
        return resource;
    }

    public void setResource(String resource) {
        this.resource = resource;
    }

    public List<Permission> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<String> permissions) {
        this.permissions = permissions.stream().map(p -> Permission.valueOf(p)).collect(Collectors.toList());
    }

    public Entitlement mapToEntitlement() {
        Entitlement entitlement = new Entitlement();
        entitlement.setPermissions(permissions.stream().map(Permission::getValue).collect(Collectors.toList()));
        entitlement.setResource(resource);
        return entitlement;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", "RoleEntitlement{", "}")
                .add("resourceType=" + resource)
                .add("permission=" + permissions)
                .toString();
    }
}
