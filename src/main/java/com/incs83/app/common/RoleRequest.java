package com.incs83.app.common;

import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created by jayant on 30/4/17.
 */
public class RoleRequest {

    @NotBlank
    @NotNull
    String name;

    @NotBlank
    @NotNull
    String service;

    @NotBlank
    @NotNull
    List<RoleEntitlement> roleEntitlement;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public List<RoleEntitlement> getRoleEntitlement() {
        return roleEntitlement;
    }

    public void setRoleEntitlement(List<RoleEntitlement> roleEntitlement) {
        this.roleEntitlement = roleEntitlement;
    }
}

