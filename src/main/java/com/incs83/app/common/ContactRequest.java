package com.incs83.app.common;

import java.util.StringJoiner;

/**
 * Created by hari on 1/8/17.
 */
public class ContactRequest {
    private String filter = "l";
    private int page = 0;
    private int limit = 50;
    private String sortBy = "firstName";
    private String orderBy = "ASC";

    public ContactRequest() {
    }

    public String getFilter() {
        return filter;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public String getSortBy() {
        return sortBy;
    }

    public void setSortBy(String sortBy) {
        this.sortBy = sortBy;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", "ContactRequest{", "}")
                .add("filter='" + filter + "'")
                .add("page=" + page)
                .add("limit=" + limit)
                .add("sortBy='" + sortBy + "'")
                .add("orderBy='" + orderBy + "'")
                .toString();
    }
}
