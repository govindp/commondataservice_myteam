package com.incs83.app.common;

import java.util.List;

import javax.validation.constraints.NotNull;

/**
 * @author jayant
 */
public class ShareContactsRequest {

    @NotNull
    private List<String> connects;

    @NotNull
    private String type;

    @NotNull
    private List<String> shareWith;

    public List<String> getConnects() {
        return connects;
    }

    public void setConnects(List<String> connects) {
        this.connects = connects;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<String> getShareWith() {
        return shareWith;
    }

    public void setShareWith(List<String> shareWith) {
        this.shareWith = shareWith;
    }
}
