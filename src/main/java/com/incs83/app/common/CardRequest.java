package com.incs83.app.common;

import org.springframework.web.multipart.MultipartFile;

/**
 * Created by hari on 16/8/17.
 */
public class CardRequest {
    private MultipartFile frontImg;
    private MultipartFile backImg;

    public CardRequest(MultipartFile frontImg, MultipartFile backImg) {
        this.frontImg = frontImg;
        this.backImg = backImg;
    }

    public MultipartFile getFrontImg() {
        return frontImg;
    }

    public void setFrontImg(MultipartFile frontImg) {
        this.frontImg = frontImg;
    }

    public MultipartFile getBackImg() {
        return backImg;
    }

    public void setBackImg(MultipartFile backImg) {
        this.backImg = backImg;
    }
}
