package com.incs83.app.common;

import com.incs83.app.entities.TaskTemplateDetails;

import java.util.Set;

/**
 * Created by rosrivas on 6/23/17.
 */
public class StateRequest {

    private String name;

    private Set<TaskTemplateDetails> taskTemplateDetails;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<TaskTemplateDetails> getTaskTemplateDetails() {
        return taskTemplateDetails;
    }

    public void setTaskTemplateDetails(Set<TaskTemplateDetails> taskTemplateDetails) {
        this.taskTemplateDetails = taskTemplateDetails;
    }
}
