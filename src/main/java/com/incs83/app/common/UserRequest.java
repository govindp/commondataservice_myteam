package com.incs83.app.common;

import java.util.Date;
import java.util.List;

/**
 * Created by jayant on 1/5/17.
 */
public class UserRequest {

    private List<String> roles;
    private List<String> service;
    private String firstName;
    private String middleName;
    private String lastName;
    private String gender;
    private String email;
    private String company;
    private String title;
    private String mobilePhone;
    private String deskPhone;
    private String city;
    private String state;
    private String address1;
    private String address2;
    private String pincode;
    private String country;
    private String imageUrl;
    private Date dob;
    private String occupation;
    private String emailType;
    private String quadrant;
    private String location;
    private String socialMedia;
    private String age;
    private String experience;
    private String orbits;
    private String potential;
    private String touch;
    private String resumeLastUpdated;
    private String sourceOfResume;
    private String association;
    private String skills;
    private String jobType;
    private String currentIndustry;
    private String industryLookingFor;
    private String currentAreas;
    private String areasLookingFor;
    private String desiredCompanyType;
    private String salary;
    private String qualification;
    private String educationBackground;
    private String willingToTravel;
    private String relocation;
    private String visaStatus;
    private String lifeCycle;
    private String maritalStatus;
    private String lineOfBusiness;

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    public List<String> getService() {
        return service;
    }

    public void setService(List<String> service) {
        this.service = service;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public String getDeskPhone() {
        return deskPhone;
    }

    public void setDeskPhone(String deskPhone) {
        this.deskPhone = deskPhone;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getEmailType() {
        return emailType;
    }

    public void setEmailType(String emailType) {
        this.emailType = emailType;
    }

    public String getQuadrant() {
        return quadrant;
    }

    public void setQuadrant(String quadrant) {
        this.quadrant = quadrant;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getSocialMedia() {
        return socialMedia;
    }

    public void setSocialMedia(String socialMedia) {
        this.socialMedia = socialMedia;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public String getOrbits() {
        return orbits;
    }

    public void setOrbits(String orbits) {
        this.orbits = orbits;
    }

    public String getPotential() {
        return potential;
    }

    public void setPotential(String potential) {
        this.potential = potential;
    }

    public String getTouch() {
        return touch;
    }

    public void setTouch(String touch) {
        this.touch = touch;
    }

    public String getResumeLastUpdated() {
        return resumeLastUpdated;
    }

    public void setResumeLastUpdated(String resumeLastUpdated) {
        this.resumeLastUpdated = resumeLastUpdated;
    }

    public String getSourceOfResume() {
        return sourceOfResume;
    }

    public void setSourceOfResume(String sourceOfResume) {
        this.sourceOfResume = sourceOfResume;
    }

    public String getAssociation() {
        return association;
    }

    public void setAssociation(String association) {
        this.association = association;
    }

    public String getSkills() {
        return skills;
    }

    public void setSkills(String skills) {
        this.skills = skills;
    }

    public String getJobType() {
        return jobType;
    }

    public void setJobType(String jobType) {
        this.jobType = jobType;
    }

    public String getCurrentIndustry() {
        return currentIndustry;
    }

    public void setCurrentIndustry(String currentIndustry) {
        this.currentIndustry = currentIndustry;
    }

    public String getIndustryLookingFor() {
        return industryLookingFor;
    }

    public void setIndustryLookingFor(String industryLookingFor) {
        this.industryLookingFor = industryLookingFor;
    }

    public String getCurrentAreas() {
        return currentAreas;
    }

    public void setCurrentAreas(String currentAreas) {
        this.currentAreas = currentAreas;
    }

    public String getAreasLookingFor() {
        return areasLookingFor;
    }

    public void setAreasLookingFor(String areasLookingFor) {
        this.areasLookingFor = areasLookingFor;
    }

    public String getDesiredCompanyType() {
        return desiredCompanyType;
    }

    public void setDesiredCompanyType(String desiredCompanyType) {
        this.desiredCompanyType = desiredCompanyType;
    }

    public String getSalary() {
        return salary;
    }

    public void setSalary(String salary) {
        this.salary = salary;
    }

    public String getQualification() {
        return qualification;
    }

    public void setQualification(String qualification) {
        this.qualification = qualification;
    }

    public String getEducationBackground() {
        return educationBackground;
    }

    public void setEducationBackground(String educationBackground) {
        this.educationBackground = educationBackground;
    }

    public String getWillingToTravel() {
        return willingToTravel;
    }

    public void setWillingToTravel(String willingToTravel) {
        this.willingToTravel = willingToTravel;
    }

    public String getRelocation() {
        return relocation;
    }

    public void setRelocation(String relocation) {
        this.relocation = relocation;
    }

    public String getVisaStatus() {
        return visaStatus;
    }

    public void setVisaStatus(String visaStatus) {
        this.visaStatus = visaStatus;
    }

    public String getLifeCycle() {
        return lifeCycle;
    }

    public void setLifeCycle(String lifeCycle) {
        this.lifeCycle = lifeCycle;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getLineOfBusiness() {
        return lineOfBusiness;
    }

    public void setLineOfBusiness(String lineOfBusiness) {
        this.lineOfBusiness = lineOfBusiness;
    }
}
