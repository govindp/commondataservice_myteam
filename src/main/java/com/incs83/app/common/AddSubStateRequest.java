package com.incs83.app.common;

import com.incs83.app.entities.TaskTemplateDetails;

import java.util.Set;

/**
 * Created by rosrivas on 6/23/17.
 */
public class AddSubStateRequest {

    private String id;

    private Set<TaskTemplateDetails> taskTemplateDetails;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Set<TaskTemplateDetails> getTaskTemplateDetails() {
        return taskTemplateDetails;
    }

    public void setTaskTemplateDetails(Set<TaskTemplateDetails> taskTemplateDetails) {
        this.taskTemplateDetails = taskTemplateDetails;
    }
}
