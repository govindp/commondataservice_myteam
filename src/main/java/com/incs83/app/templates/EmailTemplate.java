package com.incs83.app.templates;

import com.sendgrid.SendGrid;
import com.sendgrid.smtpapi.SMTPAPI;
import org.json.JSONException;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.StringJoiner;

/**
 * Created by Jayant Puri on 11-07-2017.
 */
public enum EmailTemplate {

    WELCOME_EMAIL {
        private String templateId = "e99f54ed-0afe-47fe-9340-4dcb195040a1";
        private String sendFrom = "noreply@bms.iot83.com";
        private String html = " ";
        private String subject = "Welcome to 83incs - Registration";
        private Data data;

        @Override
        public void setEmailData(Data data) {
            this.data = data;
            if (Objects.nonNull(this.data)) {
                validate(this.data, templateId, sendFrom, html, subject);
            }
        }

        @Override
        public int getMaxAttempt() {
            return data.getMaxAttempts();
        }

        @Override
        public Data getData() {
            return data;
        }
    },
    ACCOUNT_ONBOARDING {
        private String templateId = "de059ac1-ccee-45bf-b59e-765b513323a0";
        private String sendFrom = "noreply@bms.iot83.com";
        private String html = " ";
        private String subject = "Account Onboarding - iot83 BMS";
        private Data data;

        @Override
        public void setEmailData(Data data) {
            this.data = data;
            if (Objects.nonNull(this.data)) {
                validate(this.data, templateId, sendFrom, html, subject);
            }
        }

        @Override
        public int getMaxAttempt() {
            return data.getMaxAttempts();
        }

        @Override
        public Data getData() {
            return data;
        }
    };

    public abstract void setEmailData(Data data);

    public abstract int getMaxAttempt();

    public abstract Data getData();

    public static SendGrid.Email getEmailObject(Data data) {
        SendGrid.Email email = new SendGrid.Email();
        email.addTo(data.getTo()).setFrom(data.getFrom()).setSubject(data.getSubject())
                .setTemplateId(data.getTemplateId()).setHtml(data.getHtml());
        substituteAsSMTP(data.getTemplateData(), email);
        return email;
    }

    public static void substituteAsSMTP(Map<String, String> map, SendGrid.Email email) throws JSONException {
        SMTPAPI smtpApi = email.getSMTPAPI();
        for (Map.Entry<String, String> entry : map.entrySet()) {
            smtpApi.addSubstitution(entry.getKey(), entry.getValue());
        }
    }

    public static void validate(Data data, String templateId, String sendFrom, String html, String subject) {
        if (Objects.isNull(data.getTemplateId()) || data.getTemplateId().isEmpty())
            data.setTemplateId(templateId);
        if (Objects.isNull(data.getFrom()) || data.getFrom().isEmpty())
            data.setFrom(sendFrom);
        if (Objects.isNull(data.getFrom()) || data.getFrom().isEmpty())
            data.setHtml(html);
        if (data.getMaxAttempts() < 0)
            data.setMaxAttempts(1);
        if (Objects.isNull(data.getSubject()) || data.getSubject().isEmpty())
            data.setSubject(subject);
    }

    public static class Data {
        private String templateId = "";
        private String from = "";
        private String[] to;
        private String subject = "";
        private Map<String, String> templateData = new HashMap<>();
        private String html = " ";
        private int maxAttempts = 1;

        public Data() {
        }

        public Data(Map<String, String> templateData, String... to) {
            this.to = to;
            this.templateData = templateData;
        }

        public Data(Map<String, String> templateData, int maxAttempts, String... to) {
            this.to = to;
            this.maxAttempts = maxAttempts;
            this.templateData = templateData;
        }

        public Data(String subject, Map<String, String> templateData, String... to) {
            this.to = to;
            this.subject = subject;
            this.templateData = templateData;
        }

        public Data(String subject, Map<String, String> templateData, int maxAttempts, String... to) {
            this.to = to;
            this.subject = subject;
            this.templateData = templateData;
            this.maxAttempts = maxAttempts;
        }

        public String[] getTo() {
            return to;
        }

        public Data setTo(String... to) {
            this.to = to;
            return this;
        }

        public String getSubject() {
            return subject;
        }

        public Data setSubject(String subject) {
            this.subject = subject;
            return this;
        }

        public Map<String, String> getTemplateData() {
            return templateData;
        }

        public Data setTemplateData(Map<String, String> templateData) {
            this.templateData = templateData;
            return this;
        }

        public String getTemplateId() {
            return templateId;
        }

        public Data setTemplateId(String templateId) {
            this.templateId = templateId;
            return this;
        }

        public String getFrom() {
            return from;
        }

        public Data setFrom(String from) {
            this.from = from;
            return this;
        }

        public String getHtml() {
            return html;
        }

        public Data setHtml(String html) {
            this.html = html;
            return this;
        }

        public int getMaxAttempts() {
            return maxAttempts;
        }

        public void setMaxAttempts(int maxAttempts) {
            this.maxAttempts = maxAttempts;
        }

        @Override
        public String toString() {
            return new StringJoiner(", ", this.getClass().getSimpleName() + "[", "]")
                    .add("templateId = " + templateId)
                    .add("from = " + from)
                    .add("to = " + to)
                    .add("subject = " + subject)
                    .add("templateData = " + templateData)
                    .add("html = " + html)
                    .add("maxAttempts = " + maxAttempts).toString();
        }
    }
}

