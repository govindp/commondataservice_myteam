package com.incs83.app.templates;

public interface Neo4jQueries {

    public static final String CREATE_QUERY = "MERGE ({0}:{1} {2})";

    public static final String UPDATE_QUREY = "MATCH ({0}:{1} {2}) SET {3}";

    public static final String CREATE_RELATIONSHIP_QUERY = "MATCH ({0}:{1} {2}), ({3}:{4} {5}) MERGE ({0})-[:{6} {7}]->({3})";

    public static final String UPDATE_SET_QUERY = "MATCH ({0}:{1} {2}) SET {3} RETURN {0}";

    public static final String UPDATE_REMOVE_QUERY = "MATCH ({0}:{1} {2}) REMOVE {3} RETURN {0}";

    public static final String DELETE_NODE = "MATCH ({0}:{1} {2}) OPTIONAL MATCH ({0})-[r]-(n) DELETE r,{0}";

    public static final String READ_NODE = "MATCH ({0}:{1} {2}) RETURN {0}";

    public static final String READ_RELATIONSHIP = "MATCH ({0}:{1} {2})-[r:{6}]->({3}:{4} {5}) RETURN r,{3}";

    public static final String READ_RELATIONSHIP_AND_NODE = "MATCH ({0}:{1} {2})-[r:{6}]->({3}:{4} {5}) RETURN r,{0},{3}";

    public static final String NODE_INCOMING_RELATIONSHIPS = "MATCH ({0}:{1} {2}) MATCH (n)-[r]->({0}) return n,r,{0}";

    public static final String NODE_OUTGOING_RELATIONSHIPS = "MATCH ({0}:{1} {2}) MATCH ({0})-[r]->(n) return {0},r,n";

    public static final String NODE_ALL_RELATIONSHIP = "MATCH ({0}:{1} {2}) MATCH (n)-[r]->({0}) return n,r,{0} UNION MATCH ({0}:{1} {2}) MATCH ({0})-[r]->(n) return {0},r,n";

    public static final String DELETE_NODE_ALL_RELATIONSHIP = "MATCH ({0}:{1} {2}) MATCH (n)-[r]->({0}) DELETE r";

    public static final String GENERIC_COUNT_QUERY = "{0} UNWIND ROW AS A return DISTINCT count (A) AS count";

    public static final String GENERIC_RETURN_QUERY = "{0} return DISTINCT A";

    public static final String GENERIC_RETURN_QUERY_USER = "{0} return DISTINCT A.firstName as firstName, A.lastName as lastName, A.imageUrl as imageUrl, A.title as title, A.company as company, A.mobilePhone as mobilePhone, A.deskPhone as deskPhone, A.quadrant as quadrant, A.touch as touch, A.potential as potential , A.id as id, A.email as email ";

    public static final String FETCH_MY_LINKED_CONNECTIONS =
            "MATCH (c:User)-[r:LINKED]->(u:User '{'id:''{0}'''}') with collect(c) AS ROW";

    public static final String FETCH_MY_LINKED_CONNECTIONS_REVERSE =
            "MATCH (c:User)<-[r:LINKED]-(u:User '{'id:''{0}'''}') with collect(c) AS ROW";

    public static final String FETCH_MY_SHARED_CONNECTIONS =
            "MATCH (c:User)-[r:SHARED]->(u:User '{'id:''{0}'''}') with collect(c) AS ROW";

    public static final String FETCH_MY_COMPANY =
            "MATCH (u:User '{'id:''{0}'''}')-[:LINKED]->(:Service)<-[:OWNS]-(a:Account) return a";

    public static final String FETCH_MY_COMPANY_SHARED_CONNECTIONS =
            "MATCH (a:Account '{'id:''{0}'''}')<-[r:SHARED]-(c:User) with collect(c) AS ROW";

    public static final String FETCH_MY_COMPANY_LINKED_CONNECTIONS =
            "MATCH (a:Account '{'id:''{0}'''}')-[:OWNS]->(:Service)<-[r:LINKED]-(c:User) with collect(c) AS ROW";

    public static final String FETCH_COMPANY_BY_ID =
            "MATCH (c:Account '{'companyName:''{0}'''}') " +
                    "return DISTINCT c";

    public static final String FETCH_USER_BY_ID =
            "MATCH (u:User '{'id:''{0}'''}') " +
                    "return DISTINCT u";

    public static final String GENERIC_FILTER_QUERY = "{0} ORDER BY {1} {2} SKIP {3} LIMIT {4}";

}
