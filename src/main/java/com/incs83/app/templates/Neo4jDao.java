/**
 * @Author Jayant Puri
 * @Created 29-May-2017
 */
package com.incs83.app.templates;

import com.incs83.app.enums.RelationshipTypeEnum;
import org.neo4j.driver.v1.Record;
import org.neo4j.driver.v1.Session;
import org.neo4j.driver.v1.StatementResult;

import java.util.List;
import java.util.Map;

public interface Neo4jDao {

    public <T> boolean createNode(T object, Map<String, String> fields, Session session) throws IllegalAccessException;

    public <T> boolean updateNode(T object, Map<String, String> fields, Session session) throws IllegalAccessException;

    public <T> boolean deleteNode(T object, Map<String, String> fields, Session session) throws IllegalAccessException;

    public <T> boolean deleteNode(String node, Map<String, String> fields, Session session) throws IllegalAccessException;

    public boolean createNode(String node, Map<String, String> fields, Session session) throws IllegalAccessException;

    public boolean updateNode(String node, Map<String, String> fields, Session session) throws IllegalAccessException;

    public boolean createRelationship(Class<?> from, Map<String, String> fromFields, Class<?> to, Map<String, String> toFields, RelationshipTypeEnum relation, Map<String, String> fields, Session session);

    boolean createRelationship(String from, Map<String, String> fromFields, String to, Map<String, String> toFields, RelationshipTypeEnum relation, Map<String, String> fields, Session session);

    public boolean isNodeExist(Class<?> clazz, Map<String, String> fields, Session session) throws IllegalAccessException;

    public Record getNode(Class<?> clazz, Map<String, String> fields, Session session) throws IllegalAccessException;

    public Record getNode(String node, Map<String, String> fields, Session session) throws IllegalAccessException;

    public boolean deleteAllRelationshipForNode(String node, Map<String, String> fields, Session session) throws IllegalAccessException;

    public boolean isRelationshipExist(Class<?> from, Map<String, String> fromFields, Class<?> to, Map<String, String> toFields, RelationshipTypeEnum relation);

    public List<Record> findNodeAndRelationship(String from, Map<String, String> fromFields, String to, Map<String, String> toFields, RelationshipTypeEnum relation, Session session) throws IllegalAccessException;

    public boolean deleteNode();

    public boolean deleteRelationship();

    public boolean findByAttribute();

    public StatementResult executeQuery(String query, Session session);

    public <T> Map<String, String> getFields(T object) throws IllegalAccessException;
}
