/**
 * @Author Jayant Puri
 * @Created 29-May-2017
 */
package com.incs83.app.templates.impl;

import com.incs83.app.annotations.Neo4jRecord;
import com.incs83.app.enums.RelationshipTypeEnum;
import com.incs83.app.templates.Neo4jQueries;
import com.incs83.app.templates.Neo4jDao;
import com.incs83.app.utils.application.ResourceUtils;
import org.neo4j.driver.v1.Record;
import org.neo4j.driver.v1.Session;
import org.neo4j.driver.v1.StatementResult;
import org.springframework.stereotype.Service;

import java.lang.reflect.Field;
import java.text.MessageFormat;
import java.util.*;


@Service
public class Neo4JDaoImpl implements Neo4jDao, Neo4jQueries {

    /**
     * Neo4j Basics:
     * 1) Create Node
     * 2) Create Relationships
     * 3) Before Deleting a Node do ensure that its relationship is deleted.
     */

    @Override
    public <T> boolean createNode(T object, Map<String, String> fields, Session session) throws IllegalAccessException {
        String parameters = "{ " + getNeo4jFields(object, fields) + " }";
        String query = MessageFormat.format(CREATE_QUERY, object.getClass().getSimpleName().toLowerCase(), object.getClass().getSimpleName(), parameters);
        StatementResult result = session.run(query);
        return !result.list().isEmpty();
    }

    @Override
    public <T> boolean deleteNode(T object, Map<String, String> fields, Session session) throws IllegalAccessException {
        String parameters = new StringJoiner("", "{", "}").add(getNeo4jIdField(object, null)).toString();
        String query = MessageFormat.format(DELETE_NODE, object.getClass().getSimpleName().toLowerCase(), object.getClass().getSimpleName(), parameters);
        StatementResult result = session.run(query);
        return !result.list().isEmpty();
    }

    @Override
    public <T> boolean deleteNode(String node, Map<String, String> fields, Session session) throws IllegalAccessException {
        String matchCriteria = new StringJoiner("", "{", "}").add(getNeo4jIdField(null, fields)).toString();
        String query = MessageFormat.format(DELETE_NODE, node.toLowerCase(), node, matchCriteria);
        StatementResult result = session.run(query);
        return !result.list().isEmpty();
    }

    @Override
    public <T> boolean updateNode(T object, Map<String, String> fields, Session session) throws IllegalAccessException {
        String matchCriteria = new StringJoiner("", "{", "}").add(getNeo4jIdField(object, null)).toString();
        String updatedData = createUpdateFields(object, null, null);
        String query = MessageFormat.format(UPDATE_QUREY, object.getClass().getSimpleName().toLowerCase(), object.getClass().getSimpleName(), matchCriteria, updatedData);
        StatementResult result = session.run(query);
        return !result.list().isEmpty();
    }

    @Override
    public boolean updateNode(String node, Map<String, String> fields, Session session) throws IllegalAccessException {
        String matchCriteria = new StringJoiner("", "{", "}").add(getNeo4jIdField(null, fields)).toString();
        String updatedData = createUpdateFields(null, fields, node.toLowerCase());
        String query = MessageFormat.format(UPDATE_QUREY, node.toLowerCase(), node, matchCriteria, updatedData);
        StatementResult result = session.run(query);
        return !result.list().isEmpty();
    }

    @Override
    public boolean createNode(String node, Map<String, String> fields, Session session) throws IllegalAccessException {
        String parameters = "{ " + getNeo4jFields(null, fields) + " }";
        String query = MessageFormat.format(CREATE_QUERY, node.toLowerCase(), node, parameters);
        StatementResult result = session.run(query);
        return !result.list().isEmpty();
    }

    @Override
    public boolean createRelationship(Class<?> from, Map<String, String> fromFields, Class<?> to, Map<String, String> toFields, RelationshipTypeEnum relation, Map<String, String> fields, Session session) {
        String fromFilter = "{" + createFilter(fromFields) + "}";
        String toFilter = "{" + createFilter(toFields) + "}";
        String parameters = fields != null && !fields.isEmpty() ? "{ " + createFilter(fields) + " }" : "";
        String query;
        if (from.getSimpleName().toLowerCase().equals(to.getSimpleName().toLowerCase())) {
            query = MessageFormat.format(CREATE_RELATIONSHIP_QUERY, "from_" + from.getSimpleName().toLowerCase(),
                    from.getSimpleName(), fromFilter, "to_" + to.getSimpleName().toLowerCase(), to.getSimpleName(), toFilter, relation.name(), parameters);
        } else {
            query = MessageFormat.format(CREATE_RELATIONSHIP_QUERY, from.getSimpleName().toLowerCase(),
                    from.getSimpleName(), fromFilter, to.getSimpleName().toLowerCase(), to.getSimpleName(), toFilter, relation.name(), parameters);
        }
        StatementResult result = session.run(query);
        return !result.list().isEmpty();
    }

    @Override
    public boolean createRelationship(String from, Map<String, String> fromFields, String to, Map<String, String> toFields, RelationshipTypeEnum relation, Map<String, String> fields, Session session) {
        String fromFilter = "{" + createFilter(fromFields) + "}";
        String toFilter = "{" + createFilter(toFields) + "}";
        String parameters = fields != null && fields.isEmpty() ? "{ " + createFilter(fields) + " }" : "";
        String query = MessageFormat.format(CREATE_RELATIONSHIP_QUERY, from.toLowerCase(),
                from, fromFilter, to.toLowerCase(), to, toFilter, relation.name(), parameters);
        StatementResult result = session.run(query);
        return !result.list().isEmpty();
    }

    @Override
    public boolean isNodeExist(Class<?> clazz, Map<String, String> fields, Session session) throws IllegalAccessException {
        Record record = getNode(clazz, fields, session);
        return record == null ? false : true;
    }


    @Override
    public Record getNode(Class<?> clazz, Map<String, String> fields, Session session) throws IllegalAccessException {
        String parameters = "{ " + getNeo4jFields(null, fields) + " }";
        String query = MessageFormat.format(READ_NODE, clazz.getSimpleName().toLowerCase(), clazz.getSimpleName(), parameters);
        StatementResult result = session.run(query);
        List<Record> recList = result.list();
        return recList.size() == 0 ? null : recList.get(0);
    }

    @Override
    public Record getNode(String node, Map<String, String> fields, Session session) throws IllegalAccessException {
        String parameters = "{ " + getNeo4jFields(null, fields) + " }";
        String query = MessageFormat.format(READ_NODE, node.toLowerCase(), node, parameters);
        StatementResult result = session.run(query);
        List<Record> recList = result.list();
        return recList.size() == 0 ? null : recList.get(0);
    }

    @Override
    public boolean deleteAllRelationshipForNode(String node, Map<String, String> fields, Session session) throws IllegalAccessException {
        String parameters = "{ " + getNeo4jFields(null, fields) + " }";
        String query = MessageFormat.format(DELETE_NODE_ALL_RELATIONSHIP, node.toLowerCase(), node, parameters);
        StatementResult result = session.run(query);
        return !result.list().isEmpty();
    }

    @Override
    public boolean isRelationshipExist(Class<?> from, Map<String, String> fromFields, Class<?> to, Map<String, String> toFields, RelationshipTypeEnum relation) {
        return false;
    }

    @Override
    public List<Record> findNodeAndRelationship(String from, Map<String, String> fromFields, String to, Map<String, String> toFields, RelationshipTypeEnum relation, Session session) throws IllegalAccessException {
        String fromParams = "{ " + getNeo4jFields(null, fromFields) + " }";
        String toParams = "{ " + getNeo4jFields(null, toFields) + " }";
        String query = MessageFormat.format(READ_RELATIONSHIP_AND_NODE, from.toLowerCase(), from, fromParams,
                to.toLowerCase(), to, toParams, relation.name());
        StatementResult result = session.run(query);
        return result.list();
    }

    @Override
    public boolean deleteNode() {
        return false;
    }

    @Override
    public boolean deleteRelationship() {
        return false;
    }

    @Override
    public boolean findByAttribute() {
        return false;
    }


    @Override
    public <T> Map<String, String> getFields(T resource) throws IllegalAccessException {
        Map<String, String> fields = null;
        if (resource != null) {
            fields = new HashMap<>();
            List<Field> resourceFields = ResourceUtils.getFields(resource);
            for (Field field : resourceFields) {
                Neo4jRecord annotation = field.getAnnotation(Neo4jRecord.class);
                if (annotation != null) {
                    field.setAccessible(true);
                    fields.put(field.getName(), (String) field.get(resource));
                }
            }
        }
        return fields;
    }

    private <T> String getNeo4jFields(T resource, Map<String, String> extraFields) throws IllegalAccessException {
        StringBuffer buffer = new StringBuffer();
        if (resource != null) {
            List<Field> fields = ResourceUtils.getFields(resource);
            for (Field field : fields) {
                Neo4jRecord annotation = field.getAnnotation(Neo4jRecord.class);
                if (annotation != null) {
                    field.setAccessible(true);
                    buffer.append(field.getName());
                    buffer.append(":'");
                    buffer.append(field.get(resource));
                    buffer.append("',");
                }
            }
        }
        if (extraFields != null) {
            for (Map.Entry<String, String> entry : extraFields.entrySet()) {
                buffer.append(entry.getKey());
                buffer.append(":'");
                buffer.append(entry.getValue());
                buffer.append("',");
            }
        }
        if (buffer.length() != 0) {
            buffer.deleteCharAt(buffer.lastIndexOf(","));
        }
        return buffer.toString();
    }

    private <T> String getNeo4jIdField(T resource, Map<String, String> extraFields) throws IllegalAccessException {
        StringJoiner joiner = new StringJoiner(",");
        if (resource != null) {
            List<Field> fields = ResourceUtils.getFields(resource);
            for (Field field : fields) {
                Neo4jRecord annotation = field.getAnnotation(Neo4jRecord.class);
                if (annotation != null) {
                    field.setAccessible(true);
                    if (field.getName().equals("id")) {
                        joiner.add(field.getName() + ":" + "'" + field.get(resource) + "'");
                    }
                }
            }
        }
        if (extraFields != null) {
            for (Map.Entry<String, String> entry : extraFields.entrySet()) {
                if (entry.getKey().equals("id")) {
                    joiner.add(entry.getKey() + ":" + "'" + entry.getValue() + "'");
                }
            }
        }
        return joiner.toString();
    }

    private <T> String createUpdateFields(T resource, Map<String, String> extraFields, String nodeName) throws IllegalAccessException {
        StringJoiner joiner = new StringJoiner(",");
        if (resource != null) {
            nodeName = resource.getClass().getSimpleName().toLowerCase();
            List<Field> fields = ResourceUtils.getFields(resource);
            for (Field field : fields) {
                Neo4jRecord annotation = field.getAnnotation(Neo4jRecord.class);
                if (annotation != null) {
                    field.setAccessible(true);
                    if (Objects.nonNull(field.get(resource))) {
                        joiner.add(nodeName + "." + field.getName() + "='" + field.get(resource) + "'");
                    }
                }
            }
        }
        if (extraFields != null && nodeName != null && !nodeName.isEmpty()) {
            for (Map.Entry<String, String> entry : extraFields.entrySet()) {
                if (Objects.nonNull(entry.getValue())) {
                    joiner.add(nodeName + "." + entry.getKey() + "='" + entry.getValue() + "'");
                }
            }
        }
        return joiner.toString();
    }

    private String createFilter(Map<String, String> fields) {
        StringBuffer buffer = new StringBuffer();
        for (Map.Entry<String, String> entry : fields.entrySet()) {
            buffer.append(entry.getKey());
            buffer.append(":'");
            buffer.append(entry.getValue());
            buffer.append("',");
        }
        buffer.deleteCharAt(buffer.lastIndexOf(","));
        return buffer.toString();
    }

    @Override
    public StatementResult executeQuery(String query, Session session) {
        return session.run(query);
    }

}

