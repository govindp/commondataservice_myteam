/**
 * @Author Jayant Puri
 * @Created 30-May-2017
 */
package com.incs83.app.context;

import com.incs83.security.tokenFactory.UserContext;

import java.util.Objects;

/**
 * @author jayant
 */
public class ExecutionContext {

    private final UserContext userContext;

    public static final ThreadLocal<ExecutionContext> CONTEXT = new ThreadLocal<>();

    public static void clear() {
        CONTEXT.remove();
    }

    public static ExecutionContext get() {
        return CONTEXT.get();
    }

    public static void set(ExecutionContext ec) {
        if (Objects.isNull(ec)) {
            throw new RuntimeException("Invalid Execution Context");
        }
        CONTEXT.set(ec);
    }

    public ExecutionContext(UserContext userContext) {
        this.userContext = userContext;
    }


    public UserContext getUsercontext() {
        return userContext;
    }
}
