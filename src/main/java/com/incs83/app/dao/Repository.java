package com.incs83.app.dao;

import java.util.HashMap;
import java.util.List;

import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by hari on 11/8/17.
 */
@Service
@SuppressWarnings({"unchecked","rawtypes"})
public class Repository<A> {

    @Autowired
    private PersistenceRepository persistenceRepository;

    public A create(A a) {
        return (A) persistenceRepository.createWithTenant(session -> {
            session.save(a);
            return a;
        });
    }

    public A createDefault(A a) {
        return (A) persistenceRepository.createWithDefaultTenant(session -> {
            session.save(a);
            return a;
        });
    }

    
    public List<A> read(Class clazz) {
        return (List<A>) persistenceRepository.readWithTenant(session -> {
            Query query = session.createQuery("from " + clazz.getSimpleName());
            return (List<A>) query.list();
        });
    }

    public List<A> readDefault(Class clazz) {
        return (List<A>) persistenceRepository.readWithDefaultTenant(session -> {
            Query query = session.createQuery("from " + clazz.getSimpleName());
            return (List<A>) query.list();
        });
    }

    public A read(Class clazz, String id) {
        return (A) persistenceRepository.readWithTenant(session -> session.get(clazz, (String) id));
    }

    public A readDefault(Class clazz, String id) {
        return (A) persistenceRepository.readWithDefaultTenant(session -> session.get(clazz, (String) id));
    }


    public List<A> read(String strQuery, HashMap<String, String> params) {
        return (List<A>) persistenceRepository.readWithTenant(session -> {
            Query<A> query = session.createQuery(strQuery);
            params.forEach(query::setParameter);
            return query.list();
        });
    }

    public List<A> readDefault(String strQuery, HashMap<String, String> params) {
        return (List<A>) persistenceRepository.readWithDefaultTenant(session -> {
            Query<A> query = session.createQuery(strQuery);
            params.forEach(query::setParameter);
            return query.list();
        });
    }

    public A update(A a) throws Exception {
        persistenceRepository.updateWithTenant(session -> session.saveOrUpdate(a));
        return a;
    }

    public A updateDefault(A a) throws Exception {
        persistenceRepository.updateWithDefaultTenant(session -> session.saveOrUpdate(a));
        return a;
    }

    public void delete(Class clazz, Object id) throws Exception {
        persistenceRepository.deleteWithTenant(session -> {
            A a = (A) session.get(clazz, (String) id);
            if (a != null) {
                session.delete(a);
            }
        });
    }

    public void deleteDefault(Class clazz, Object id) throws Exception {
        persistenceRepository.deleteWithDefaultTenant(session -> {
            A a = (A) session.get(clazz, (String) id);
            if (a != null) {
                session.delete(a);
            }
        });
    }

}
