package com.incs83.app.dao;

import com.incs83.app.enums.ApiResponseCode;
import com.incs83.app.mt.CurrentTenantIdentifier;
import com.incs83.app.security.exceptions.ApiException;
import com.incs83.app.service.generic.components.HibernateService;
import com.incs83.logger.LoggerFactory;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;

/**
 * Created by hari on 3/8/17.
 */
@Service
public class PersistenceRepository {

    @Autowired
    private SessionFactory sessionFactory;

    private static final Logger LOG = LoggerFactory.make();

    @FunctionalInterface
    public interface FunctionalCreate {
        Object persist(Session session);
    }

    @FunctionalInterface
    public interface FunctionalRead {
        Object read(Session session);
    }

    @FunctionalInterface
    public interface FunctionalUpdate {
        void update(Session session);
    }

    @FunctionalInterface
    public interface FunctionalDelete {
        void delete(Session session);
    }

    public Object createWithDefaultTenant(FunctionalCreate cl) {
        Transaction transaction = null;
        Session session = null;
        Object object;
        try {
            session = sessionFactory.withOptions().tenantIdentifier(CurrentTenantIdentifier.DEFAULT_TENANT).openSession();
            transaction = session.beginTransaction();
            object = cl.persist(session);
            transaction.commit();
        } catch (Exception e) {
            LOG.error("### ERROR WHILE PERSISTING DATA TO DB!! ####", e);
            if (Objects.nonNull(transaction)) {
                transaction.rollback();
            }
            throw new ApiException(ApiResponseCode.ERROR);
        } finally {
            HibernateService.closeSilently(session);
        }
        return object;
    }

    public Object createWithTenant(FunctionalCreate cl) {
        Transaction transaction = null;
        Session session = null;
        Object object;
        try {
            session = sessionFactory.withOptions().tenantIdentifier(CurrentTenantIdentifier._tenantIdentifier.get()).openSession();
            transaction = session.beginTransaction();
            object = cl.persist(session);
            transaction.commit();
        } catch (Exception e) {
            LOG.error("### ERROR WHILE PERSISTING DATA TO DB!! ####", e);
            if (Objects.nonNull(transaction)) {
                transaction.rollback();
            }
            throw new ApiException(ApiResponseCode.ERROR);
        } finally {
            HibernateService.closeSilently(session);
        }
        return object;
    }

    public Object readWithTenant(FunctionalRead cl) {
        Session session = null;
        Object object;
        try {
            session = sessionFactory.withOptions().tenantIdentifier(CurrentTenantIdentifier._tenantIdentifier.get()).openSession();
            object = cl.read(session);
        } catch (Exception e) {
            LOG.error("### ERROR WHILE FETCHING DATA FROM DB: ", e);
            throw new ApiException(ApiResponseCode.ERROR);
        } finally {
            HibernateService.closeSilently(session);
        }
        return object;
    }

    public Object readWithDefaultTenant(FunctionalRead cl) {
        Session session = null;
        Object object;
        try {
            session = sessionFactory.withOptions().tenantIdentifier(CurrentTenantIdentifier.DEFAULT_TENANT).openSession();
            object = cl.read(session);
        } catch (Exception e) {
            LOG.error("### ERROR WHILE FETCHING DATA FROM DB: ", e);
            throw new ApiException(ApiResponseCode.ERROR);
        } finally {
            HibernateService.closeSilently(session);
        }
        return object;
    }

    public void updateWithTenant(FunctionalUpdate cl) {
        Transaction transaction = null;
        Session session = null;
        try {
            session = sessionFactory.withOptions().tenantIdentifier(CurrentTenantIdentifier._tenantIdentifier.get()).openSession();
            transaction = session.beginTransaction();
            cl.update(session);
            transaction.commit();
        } catch (Exception e) {
            LOG.error("### ERROR WHILE UPDATING DATA TO DB!! ####", e);
            if (Objects.nonNull(transaction)) {
                transaction.rollback();
            }
            throw new ApiException(ApiResponseCode.ERROR);
        } finally {
            HibernateService.closeSilently(session);
        }
    }

    public void updateWithDefaultTenant(FunctionalUpdate cl) {
        Transaction transaction = null;
        Session session = null;
        try {
            session = sessionFactory.withOptions().tenantIdentifier(CurrentTenantIdentifier.DEFAULT_TENANT).openSession();
            transaction = session.beginTransaction();
            cl.update(session);
            transaction.commit();
        } catch (Exception e) {
            LOG.error("### ERROR WHILE UPDATING DATA TO DB!! ####", e);
            if (Objects.nonNull(transaction)) {
                transaction.rollback();
            }
            throw new ApiException(ApiResponseCode.ERROR);
        } finally {
            HibernateService.closeSilently(session);
        }
    }

    public void deleteWithTenant(FunctionalDelete cl) {
        Transaction transaction = null;
        Session session = null;
        try {
            session = sessionFactory.withOptions().tenantIdentifier(CurrentTenantIdentifier._tenantIdentifier.get()).openSession();
            transaction = session.beginTransaction();
            cl.delete(session);
            transaction.commit();
        } catch (Exception e) {
            LOG.error("### ERROR WHILE DELETING DATA FROM DB : ", e);
            if (Objects.nonNull(transaction)) {
                transaction.rollback();
            }
            throw new ApiException(ApiResponseCode.ERROR);
        } finally {
            HibernateService.closeSilently(session);
        }
    }

    public void deleteWithDefaultTenant(FunctionalDelete cl) {
        Transaction transaction = null;
        Session session = null;
        try {
            session = sessionFactory.withOptions().tenantIdentifier(CurrentTenantIdentifier.DEFAULT_TENANT).openSession();
            transaction = session.beginTransaction();
            cl.delete(session);
            transaction.commit();
        } catch (Exception e) {
            LOG.error("### ERROR WHILE DELETING DATA FROM DB : ", e);
            if (Objects.nonNull(transaction)) {
                transaction.rollback();
            }
            throw new ApiException(ApiResponseCode.ERROR);
        } finally {
            HibernateService.closeSilently(session);
        }
    }


}
