/**
 * @author jayant
 * @created 10-Apr-2017
 */
package com.incs83.app.main;

import org.apache.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.incs83.logger.LoggerFactory;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@ComponentScan(value = "com.incs83.app")
@EnableAutoConfiguration
@Configuration
@EnableSwagger2
@EnableMongoRepositories("com.incs83.app.service.data")
@EnableScheduling
public class Launcher {

    private static final Logger LOG = LoggerFactory.make();

    public static void main(String[] args) {
        LOG.info("Starting CDS launcher...");
        SpringApplication.run(Launcher.class, args);
    }

    public class SwaggerConfig {
        @Bean
        public Docket api() {
            return new Docket(DocumentationType.SWAGGER_2)
                    .select()
                    .paths(PathSelectors.any())
                    .build();
        }
    }
}
