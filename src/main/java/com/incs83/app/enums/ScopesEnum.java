package com.incs83.app.enums;

/**
 * @Author Jayant Puri
 * @Created 13-Apr-2017
 */
public enum ScopesEnum {
    REFRESH_TOKEN;

    public String authority() {
        return "ROLE_" + this.name();
    }
}
