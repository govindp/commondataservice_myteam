package com.incs83.app.enums;

public enum TouchPotentialEnum {

    ENGAGED("1"),
    HIGH("2"),
    HIGHEST("3"),
    MEDIUM("4"),
    LOW("5");

    TouchPotentialEnum(String s) {
    }
}
