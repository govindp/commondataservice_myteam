package com.incs83.app.enums;

public enum OrbitEnum {

    MID_MANAGEMENT("1"),
    TOP_MANAGEMENT("2"),
    STAKE_HOLDER("3"),
    EXECUTION("4"),
    EXPERT("5");

    OrbitEnum(String s) {
    }

}
