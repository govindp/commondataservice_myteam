package com.incs83.app.enums;

/**
 * Created by hari on 2/8/17.
 */
public enum Gender {
    MALE, FEMALE
}
