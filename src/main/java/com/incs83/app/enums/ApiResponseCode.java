package com.incs83.app.enums;


import com.incs83.app.enums.abstraction.ResponseCode;

public enum ApiResponseCode implements ResponseCode {
    SUCCESS(0, "SUCCESS"),
    ERROR(1, "ERROR"),
    BAD_REQUEST(2, "BAD_REQUEST"),
    ERROR_PROCESSING_REQUEST(3, "ERROR_PROCESSING_REQUEST"),
    AUTH_FAILED(4, "AUTH_FAILED"),
    GENERIC_UNAUTHORIZED(5, "GENERIC_UNAUTHORIZED"),
    INVALID_USERNAME_PASSWORD(6, "INVALID_USERNAME_PASSWORD"),
    TOKEN_EXPIRED(7, "TOKEN_EXPIRED"),
    RESOURCE_PERMISSION_DENIED(8, "RESOURCE_PERMISSION_DENIED"),
    RESOURCE_NOT_ALLOWED(9, "RESOURCE_NOT_ALLOWED"),
    PARENT_REFERENCE_FOUND(10, "PARENT_REFERENCE_FOUND"),
    FILE_IO_ERROR(11, "FILE_IO_ERROR"),
    FILE_IS_EMPTY(12, "FILE_IS_EMPTY"),
    FILE_UPLOAD_FAILED(13, "FILE_UPLOAD_FAILED");

    private int code;
    private String message;

    ApiResponseCode(int code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public int getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return message;
    }

}
