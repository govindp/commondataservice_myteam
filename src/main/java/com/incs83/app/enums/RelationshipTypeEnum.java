package com.incs83.app.enums;

public enum RelationshipTypeEnum {

    LINKED,
    OWNS,
    SHARED

}