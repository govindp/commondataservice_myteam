package com.incs83.app.enums;

import com.incs83.app.entities.*;

/**
 * Created by jayant on 24/4/17.
 */
public enum ResourceType {

    ACCOUNT(Account.class),
    ACCOUNT_TENANT(AccountTenant.class),
    ACCOUNT_TENANT_DETAILS(AccountTenantDetails.class),
    ACCOUNT_REQUEST(AccountRequisitionRequest.class),
    ROLE(Role.class),
    USER(User.class),
    CONTACTS(Contacts.class),
    COMPANY(Company.class),
    TASK_TEMPLATE(TaskTemplate.class),
    TASK(Task.class),
    TASK_DETAIL(TaskDetail.class),
    PERMISSION(Permission.class),
    ENTITLEMENT(Entitlement.class),
    CARD(Card.class);

    @SuppressWarnings("rawtypes")
    private Class clazz;

    @SuppressWarnings("rawtypes")
    ResourceType(Class clazz) {
        this.clazz = clazz;
    }

    @SuppressWarnings("rawtypes")
    public Class getClazz() {
        return this.clazz;
    }
}
