package com.incs83.app.enums.abstraction;


public interface ResponseCode {
    int getCode();

    String getMessage();
}
