package com.incs83.app.enums;

/**
 * Created by jayant on 24/4/17.
 */
public enum AccountRequestStatus {

    PENDING, PROCESSED
}
