package com.incs83.app.constants.queries;

public interface AccountSQL {

    String GET_ALL_PENDING_REQUESTS = "from AccountRequisitionRequest where requestState = 'PENDING'";

    String GET_ALL_PROCESSED_REQUESTS = "from AccountRequisitionRequest where requestState = 'PROCESSED'";

    String TENANT_BY_NAME = "from AccountTenant where tenantName=:tenantName";
}
