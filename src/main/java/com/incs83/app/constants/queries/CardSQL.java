/**
 * @Author Jayant Puri
 * @Created 28-May-2017
 */
package com.incs83.app.constants.queries;

/**
 * @author jayant
 */
public interface CardSQL {

    String GET_CARD_BY_USER_ID = "from Card where userId = :userId ";
}
