/**
 * @Author Jayant Puri
 * @Created 28-May-2017
 */
package com.incs83.app.constants.queries;

/**
 * @author jayant
 *
 */
public interface UserSQL {

    String GET_USER_BY_EMAIL = "from User where email = :email ";
}
