package com.incs83.app.constants.queries;

public interface TaskSQL {

    String TASK_FOR_ASSIGNEE = "from Task where assignee =:assignee and state in ('ASSIGNED','IN_PROGRESS','COMPLETED')";
    String TEMPLATE_BY_LABEL = "from TaskTemplate where name =:name";
    String TASK_BY_STATE = "from Task where state = :state";
    String TASK_BY_ID_AND_SERVICE = "from Task where id=:id and service =:service";
    String TASK_NEW = "from Task where state='NEW'";
}
