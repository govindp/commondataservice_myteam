/**
 * @Author Jayant Puri
 * @Created 10-Apr-2017
 */
package com.incs83.app.constants;

/**
 * @author jayant
 */
public interface ApplicationConstants {

    String APP_MAIL_CONFIGURED = "app.mail.configured";

    String SPRING_PROFILE_PROPERTY = "spring.profiles.active";
    String BOOTSTRAP_PROPERTY = "bootstrap";
    String GET = "get";
    String EMPTY_STRING = "";

    String HIBERNATE_DIALECT = "hibernate.dialect";
    String HIBERNATE_SHOW_SQL = "hibernate.show_sql";
    String HIBERNATE_FORMAT_SQL = "hibernate.format_sql";
    String HIBERNATE_PACKAGE_SCAN = "com.incs83.app.entities";
    String HIBERNATE_DDL_MODE = "hibernate.hbm2ddl.auto";
    String HIBERNATE_ML_CONNECTION_PROVIDER = "hibernate.multi_tenant_connection_provider";
    String HIBERNATE_ML_TENANT_IDENTIFIER = "hibernate.tenant_identifier_resolver";
    String HIBERNATE_ML = "hibernate.multiTenancy";
    String HIBERNATE_CP_PROVIDER_CLASS = "hibernate.connection.provider_class";
    String HIBERNATE_CP_MIN_SIZE = "hibernate.c3p0.min_size";
    String HIBERNATE_CP_MAX_SIZE = "hibernate.c3p0.max_size";
    String HIBERNATE_CP_TIMEOUT = "hibernate.c3p0.timeout";
    String HIBERNATE_CP_MAX_STATEMENTS = "hibernate.c3p0.max_statements";

    String DATABASE = "__DATABASE__";
    String JDBC_URL = "jdbcurl";
    String DB_USERNAME = "username";
    String DB_PASSWORD = "password";

    String CACHE_AVAILABLE = "cache.available";
    String SECURITY_AVAILABLE = "app.security.configured";
    String OCR_APP_KEY = "ocr.app.key";
    String OCR_APP_URL = "ocr.app.url";
    String ID = "id";
    String NAME = "name";
    String EMAIL = "email";
    String YES = "y";
    String COMMA = ",";
    String DOUBLE_HYPHEN = "--";
    int CONNECTIONS_CSV_COLUMN_LENGTH = 7;
    String CARD_FOLDER = "cards/";
}
