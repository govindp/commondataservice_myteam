package com.incs83.app.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.StringJoiner;

/**
 * Created by hari on 16/8/17.
 */
@Entity
@Table(name = "card")
public class Card extends BaseEntity implements Serializable {

	private static final long serialVersionUID = 6516107695052657784L;

	@Id
    private String id;

    @Column
    private String frontImg;

    @Column
    private String backImg;

    @Column
    private String frontText;

    @Column
    private String backText;

    @Column(nullable = false)
    @JsonIgnore
    private String userId;

    public String getId() {
        return id;
    }

    public Card setId(String id) {
        this.id = id;
        return this;
    }

    public String getFrontImg() {
        return frontImg;
    }

    public Card setFrontImg(String frontImg) {
        this.frontImg = frontImg;
        return this;
    }

    public String getBackImg() {
        return backImg;
    }

    public Card setBackImg(String backImg) {
        this.backImg = backImg;
        return this;
    }

    public String getFrontText() {
        return frontText;
    }

    public Card setFrontText(String frontText) {
        this.frontText = frontText;
        return this;
    }

    public String getBackText() {
        return backText;
    }

    public Card setBackText(String backText) {
        this.backText = backText;
        return this;
    }

    public String getUserId() {
        return userId;
    }

    public Card setUserId(String userId) {
        this.userId = userId;
        return this;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", "Card{", "}")
                .add("id='" + id + "'")
                .add("frontImg='" + frontImg + "'")
                .add("backImg='" + backImg + "'")
                .add("frontText='" + frontText + "'")
                .add("backText='" + backText + "'")
                .add("userId='" + userId + "'")
                .toString();
    }
}
