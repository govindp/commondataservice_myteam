/**
 * @Author Jayant Puri
 * @Created 28-May-2017
 */
package com.incs83.app.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "account_tenant_details")
public class AccountTenantDetails extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 8043939240736104198L;

    @Id
    private String id;

    @OneToOne
    private AccountTenant accountTenant;

    @Column(nullable = false)
    private String mysqlSchemaName;

    @Column(nullable = false)
    private String mysqlUrl;

    @Column(nullable = false)
    private String mysqlUsername;

    @Column(nullable = false)
    private String mysqlPassword;

    @Column(nullable = false)
    private String mongoCollectionName;

    @Column(nullable = false)
    private String mongoUrl;

    @Column(nullable = false)
    private String mongoUsername;

    @Column(nullable = false)
    private String mongoPassword;

    @Column(nullable = false)
    private String neo4jNodeName;

    @Column(nullable = false)
    private String neo4jUrl;

    @Column(nullable = false)
    private String neo4jUsername;

    @Column(nullable = false)
    private String neo4jPassword;

    public String getId() {
        return id;
    }

    public AccountTenantDetails setId(String id) {
        this.id = id;
        return this;
    }

    public AccountTenant getAccountTenant() {
        return accountTenant;
    }

    public AccountTenantDetails setAccountTenant(AccountTenant accountTenant) {
        this.accountTenant = accountTenant;
        return this;
    }

    public String getMysqlSchemaName() {
        return mysqlSchemaName;
    }

    public AccountTenantDetails setMysqlSchemaName(String mysqlSchemaName) {
        this.mysqlSchemaName = mysqlSchemaName;
        return this;
    }

    public String getMysqlUrl() {
        return mysqlUrl;
    }

    public AccountTenantDetails setMysqlUrl(String mysqlUrl) {
        this.mysqlUrl = mysqlUrl;
        return this;
    }

    public String getMysqlUsername() {
        return mysqlUsername;
    }

    public AccountTenantDetails setMysqlUsername(String mysqlUsername) {
        this.mysqlUsername = mysqlUsername;
        return this;
    }

    public String getMysqlPassword() {
        return mysqlPassword;
    }

    public AccountTenantDetails setMysqlPassword(String mysqlPassword) {
        this.mysqlPassword = mysqlPassword;
        return this;
    }

    public String getMongoCollectionName() {
        return mongoCollectionName;
    }

    public AccountTenantDetails setMongoCollectionName(String mongoCollectionName) {
        this.mongoCollectionName = mongoCollectionName;
        return this;
    }

    public String getMongoUrl() {
        return mongoUrl;
    }

    public AccountTenantDetails setMongoUrl(String mongoUrl) {
        this.mongoUrl = mongoUrl;
        return this;
    }

    public String getMongoUsername() {
        return mongoUsername;
    }

    public AccountTenantDetails setMongoUsername(String mongoUsername) {
        this.mongoUsername = mongoUsername;
        return this;
    }

    public String getMongoPassword() {
        return mongoPassword;
    }

    public AccountTenantDetails setMongoPassword(String mongoPassword) {
        this.mongoPassword = mongoPassword;
        return this;
    }

    public String getNeo4jNodeName() {
        return neo4jNodeName;
    }

    public AccountTenantDetails setNeo4jNodeName(String neo4jNodeName) {
        this.neo4jNodeName = neo4jNodeName;
        return this;
    }

    public String getNeo4jUrl() {
        return neo4jUrl;
    }

    public AccountTenantDetails setNeo4jUrl(String neo4jUrl) {
        this.neo4jUrl = neo4jUrl;
        return this;
    }

    public String getNeo4jUsername() {
        return neo4jUsername;
    }

    public AccountTenantDetails setNeo4jUsername(String neo4jUsername) {
        this.neo4jUsername = neo4jUsername;
        return this;
    }

    public String getNeo4jPassword() {
        return neo4jPassword;
    }

    public AccountTenantDetails setNeo4jPassword(String neo4jPassword) {
        this.neo4jPassword = neo4jPassword;
        return this;
    }
}
