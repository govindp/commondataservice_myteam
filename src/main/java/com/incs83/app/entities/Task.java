/**
 * @Author Jayant Puri
 * @Created 12-Apr-2017
 */
package com.incs83.app.entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.incs83.app.common.TaskRequest;
import com.incs83.app.utils.application.CommonUtils;

@Entity()
@Table(name = "task")
public class Task extends BaseEntity implements Serializable {

	private static final long serialVersionUID = -6769791217408415373L;

	@Id
    private String id;

    @Column(nullable = false)
    private String label;

    @Column(nullable = false)
    private String service;

    @Lob
    @JsonIgnore
    private String payload;

    @Column(nullable = false)
    private String state;

    @Column(nullable = false)
    private String assignee;

    @Column
    private String description;

    @Column
    private String shortDescription;

    @Column
    private String dueDate;

    @Column
    private String priority;

    @JoinColumn(name = "task_id")
    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<TaskDetail> taskDetails;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getPayload() {
        return payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getAssignee() {
        return assignee;
    }

    public void setAssignee(String assignee) {
        this.assignee = assignee;
    }

    public Set<TaskDetail> getTaskDetails() {
        return taskDetails;
    }

    public void setTaskDetails(Set<TaskDetail> taskDetails) {
        this.taskDetails = taskDetails;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public static Task generateFrom(TaskRequest taskRequest) {
        Task t = new Task();
        t.setId(CommonUtils.generateUUID());
        t.setTaskDetails(new HashSet<>());
        t.setService(taskRequest.getService());
        t.setAssignee(taskRequest.getAssignee());
        t.setState("ASSIGNED");
        t.setPayload(taskRequest.getPayload());
        t.setDueDate(taskRequest.getDueDate());
        t.setPriority(taskRequest.getPriority()==null?"HIGH":taskRequest.getPriority());
        t.setLabel(taskRequest.getLabel());
        t.setDescription(taskRequest.getDescription());
        t.setShortDescription(taskRequest.getShortDescription());
        CommonUtils.setCreateEntityFields(t);
        return t;
    }
}
