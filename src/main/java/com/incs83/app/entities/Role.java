/**
 * @Author Jayant Puri
 * @Created 12-Apr-2017
 */
package com.incs83.app.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.incs83.security.tokenFactory.Entitlement;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity()
@Table(name = "role")
public class Role extends BaseEntity implements Serializable {

    private static final long serialVersionUID = -5723258342757016392L;

    @Id
    private String id;

    @Column(nullable = false)
    String name;

    @Column(nullable = false)
    String service;

    @Lob
    @JsonIgnore
    String mapping;

    @Transient
    private List<Entitlement> roleEntitlement;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getMapping() {
        return mapping;
    }

    public void setMapping(String mapping) {
        this.mapping = mapping;
    }

    public List<Entitlement> getRoleEntitlement() {
        return roleEntitlement;
    }

    public void setRoleEntitlement(List<Entitlement> roleEntitlement) {
        this.roleEntitlement = roleEntitlement;
    }
}
