/**
 * @Author Jayant Puri
 * @Created 12-Apr-2017
 */
package com.incs83.app.entities;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity()
@Table(name = "task_template")
public class TaskTemplate extends BaseEntity implements Serializable {

	private static final long serialVersionUID = -4096593496629901471L;

	@Id
    private String id;

    @Column(nullable = false)
    private String name;

    @JoinColumn(name = "task_template_id")
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<TaskTemplateDetails> taskTemplateDetails;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<TaskTemplateDetails> getTaskTemplateDetails() {
        return taskTemplateDetails;
    }

    public void setTaskTemplateDetails(Set<TaskTemplateDetails> taskTemplateDetails) {
        this.taskTemplateDetails = taskTemplateDetails;
    }

}
