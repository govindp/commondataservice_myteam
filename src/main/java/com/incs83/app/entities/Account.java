/**
 * @Author Jayant Puri
 * @Created 28-May-2017
 */
package com.incs83.app.entities;

import com.incs83.app.annotations.Neo4jRecord;
import com.incs83.app.utils.application.CommonUtils;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "account")
public class Account extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 8043939240736104198L;

    @Id
    @Neo4jRecord
    private String id;

    @OneToOne
    private AccountRequisitionRequest accountRequest;

    @Column(nullable = false)
    @Neo4jRecord
    private String companyName;

    @Column(nullable = false)
    private String firstName;

    @Column
    private String middleName;

    @Column(nullable = false)
    private String lastName;

    @Column(nullable = false, unique = true)
    // Account Administrator User-name
    private String emailId;

    @Column(nullable = false)
    // Account Administrator Password
    private String password;

    @Column
    private String title;

    @Column
    private String state;

    @Column
    private String city;

    @Column
    private String zipcode;

    @Column
    private String country;

    @Column
    private String address;

    @Column(nullable = false)
    private String website;

    @Column(nullable = false)
    private String phoneNumber;

    @Column(nullable = false)
    private String status;

    @Column
    private String comments;

    @Column(nullable = false)
    private Boolean enabled;

    public AccountRequisitionRequest getAccountRequest() {
        return accountRequest;
    }

    public Account setAccountRequest(AccountRequisitionRequest accountRequest) {
        this.accountRequest = accountRequest;
        return this;
    }

    public String getCompanyName() {
        return companyName;
    }

    public Account setCompanyName(String companyName) {
        this.companyName = companyName;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public Account setTitle(String title) {
        this.title = title;
        return this;
    }

    public String getFirstName() {
        return firstName;
    }

    public Account setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getMiddleName() {
        return middleName;
    }

    public Account setMiddleName(String middleName) {
        this.middleName = middleName;
        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public Account setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public String getEmailId() {
        return emailId;
    }

    public Account setEmailId(String emailId) {
        this.emailId = emailId;
        return this;
    }

    public String getState() {
        return state;
    }

    public Account setState(String state) {
        this.state = state;
        return this;
    }

    public String getCity() {
        return city;
    }

    public Account setCity(String city) {
        this.city = city;
        return this;
    }

    public String getZipcode() {
        return zipcode;
    }

    public Account setZipcode(String zipcode) {
        this.zipcode = zipcode;
        return this;
    }

    public String getAddress() {
        return address;
    }

    public Account setAddress(String address) {
        this.address = address;
        return this;
    }

    public String getWebsite() {
        return website;
    }

    public Account setWebsite(String website) {
        this.website = website;
        return this;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public Account setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        return this;
    }

    public String getId() {
        return id;
    }

    public Account setId(String id) {
        this.id = id;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public Account setPassword(String password) {
        this.password = password;
        return this;
    }

    public String getStatus() {
        return status;
    }

    public Account setStatus(String status) {
        this.status = status;
        return this;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public Account setEnabled(Boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    public String getCountry() {
        return country;
    }

    public Account setCountry(String country) {
        this.country = country;
        return this;
    }

    public String getComments() {
        return comments;
    }

    public Account setComments(String comments) {
        this.comments = comments;
        return this;
    }

    public static Account generateFrom(AccountRequisitionRequest accountRequisitionRequest) {
        Account account = new Account()
                .setAccountRequest(accountRequisitionRequest)
                .setAddress(accountRequisitionRequest.getAddress())
                .setCity(accountRequisitionRequest.getCity())
                .setCompanyName(accountRequisitionRequest.getCompanyName())
                .setTitle(accountRequisitionRequest.getTitle())
                .setCountry(accountRequisitionRequest.getCountry())
                .setEmailId(accountRequisitionRequest.getEmailId())
                .setFirstName(accountRequisitionRequest.getFirstName())
                .setMiddleName(accountRequisitionRequest.getMiddleName())
                .setLastName(accountRequisitionRequest.getLastName())
                .setPhoneNumber(accountRequisitionRequest.getPhoneNumber())
                .setState(accountRequisitionRequest.getState())
                .setWebsite(accountRequisitionRequest.getWebsite())
                .setZipcode(accountRequisitionRequest.getZipcode())
                .setId(CommonUtils.generateUUID());
        CommonUtils.setCreateEntityFields(account);
        return account;
    }

}
