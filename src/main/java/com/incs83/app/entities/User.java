/**
 * @Author Jayant Puri
 * @Created 12-Apr-2017
 */
package com.incs83.app.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.incs83.app.annotations.Neo4jRecord;
import com.incs83.app.common.UserRequest;
import com.incs83.app.enums.*;
import com.incs83.app.security.auth.ajax.UserAuthorization;
import com.incs83.app.utils.application.CommonUtils;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

@Entity(name = "User")
@Table(name = "user")
@Document(collection = "user")
public class User extends BaseEntity implements UserDetails, Serializable {

    private static final long serialVersionUID = -5723258342757016392L;

    @Id
    @Neo4jRecord
    private String id;

    @Column
    @JsonIgnore
    @org.springframework.data.annotation.Transient
    private String password;

    @Column(nullable = false)
    @Neo4jRecord
    private String firstName = "";

    @Column
    @Neo4jRecord
    @JsonIgnore
    private String middleName = "";

    @Column(nullable = false)
    @Neo4jRecord
    private String lastName = "";

    @Column
    @Neo4jRecord
    @Enumerated(EnumType.STRING)
    private Gender gender;

    @Column(nullable = false)
    @Neo4jRecord
    private String email = "";

    @Column(nullable = false)
    @Neo4jRecord
    private String company = "";

    @Column(nullable = false)
    @Neo4jRecord
    private String title = "";

    @Column
    @JsonIgnore
    @Neo4jRecord
    private String mobilePhone = "";

    @Column
    @JsonIgnore
    @Neo4jRecord
    private String deskPhone = "";

    @Column
    @JsonIgnore
    @Neo4jRecord
    private String city = "";

    @Column
    @JsonIgnore
    @Neo4jRecord
    private String state = "";

    @Column
    @JsonIgnore
    @Neo4jRecord
    private String address1 = "";

    @Column
    @JsonIgnore
    @Neo4jRecord
    private String address2 = "";

    @Column
    @JsonIgnore
    @Neo4jRecord
    private String pincode = "";

    @Column
    @JsonIgnore
    @Neo4jRecord
    private String country = "";

    @Column
    @JsonIgnore
    @Neo4jRecord
    private String imageUrl = "";

    @Column
    @JsonIgnore
    @Neo4jRecord
    private Date dob;

    @Column(columnDefinition = "tinyint(4) default '1'")
    @JsonIgnore
    @org.springframework.data.annotation.Transient
    private boolean isAccountNonExpired;

    @Column(columnDefinition = "tinyint(4) default '1'")
    @JsonIgnore
    @org.springframework.data.annotation.Transient
    private boolean isAccountNonLocked;

    @Column(columnDefinition = "tinyint(4) default '1'")
    @JsonIgnore
    @org.springframework.data.annotation.Transient
    private boolean isCredNonExpired;

    @Column(columnDefinition = "tinyint(4) default '1'")
    @JsonIgnore
    @org.springframework.data.annotation.Transient
    private boolean isEnabled;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @org.springframework.data.annotation.Transient
    private Set<Role> role;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "user_service")
    @Column
    @org.springframework.data.annotation.Transient
    private Set<String> service;

    @JsonIgnore
    @Transient
    @Neo4jRecord
    private String occupation = "";

    @JsonIgnore
    @Transient
    @Neo4jRecord
    private String emailType = "";

    @JsonIgnore
    @Transient
    @Neo4jRecord
    @Enumerated(EnumType.STRING)
    private ProfessionalContactEnum quadrant;

    @Column
    @Neo4jRecord
    @Transient
    @JsonIgnore
    private String location = "";

    @Column
    @Neo4jRecord
    @Transient
    @JsonIgnore
    private String socialMedia = "";

    @Neo4jRecord
    @Transient
    @JsonIgnore
    private String age;

    @Neo4jRecord
    @Transient
    @JsonIgnore
    private String experience;

    @Neo4jRecord
    @Transient
    @JsonIgnore
    @Enumerated(EnumType.STRING)
    private OrbitEnum orbits;
    @Neo4jRecord
    @Transient
    @JsonIgnore
    @Enumerated(EnumType.STRING)
    private TouchPotentialEnum potential;
    @Neo4jRecord
    @Transient
    @JsonIgnore
    @Enumerated(EnumType.STRING)
    private TouchPotentialEnum touch;
    @Neo4jRecord
    @Transient
    @JsonIgnore
    private String resumeLastUpdated = "";
    @Neo4jRecord
    @Transient
    @JsonIgnore
    private String sourceOfResume = "";
    @Neo4jRecord
    @Transient
    @JsonIgnore
    private String association = "";
    @Neo4jRecord
    @Transient
    @JsonIgnore
    private String skills = "";
    @Neo4jRecord
    @Transient
    @JsonIgnore
    private String jobType = "";
    @Neo4jRecord
    @Transient
    @JsonIgnore
    private String currentIndustry = "";
    @Neo4jRecord
    @Transient
    @JsonIgnore
    private String industryLookingFor = "";
    @Neo4jRecord
    @Transient
    @JsonIgnore
    private String currentAreas = "";
    @Neo4jRecord
    @Transient
    @JsonIgnore
    private String areasLookingFor = "";
    @Neo4jRecord
    @Transient
    @JsonIgnore
    private String desiredCompanyType = "";
    @Neo4jRecord
    @Transient
    @JsonIgnore
    private String salary = "";
    @Neo4jRecord
    @Transient
    @JsonIgnore
    private String qualification = "";
    @Neo4jRecord
    @Transient
    @JsonIgnore
    private String educationBackground = "";
    @Neo4jRecord
    @Transient
    @JsonIgnore
    private String willingToTravel = "";
    @Neo4jRecord
    @Transient
    @JsonIgnore
    private String relocation = "";
    @Neo4jRecord
    @Transient
    @JsonIgnore
    private String visaStatus = "";
    @Neo4jRecord
    @Transient
    @JsonIgnore
    @Enumerated(EnumType.STRING)
    private LifeCycleEnum lifeCycle;
    @Neo4jRecord
    @Transient
    @JsonIgnore
    private String maritalStatus = "";
    @Neo4jRecord
    @Transient
    @JsonIgnore
    private String lineOfBusiness;
    @Neo4jRecord
    @Transient
    @JsonIgnore
    @Enumerated(EnumType.STRING)
    private GeneralContactsType generalContactsType = GeneralContactsType.PROFESSIONAL;
    @Neo4jRecord
    @Transient
    @JsonIgnore
    private String contactCardUploadUrl;
    @Neo4jRecord
    @Transient
    private String lastCompany;
    @Neo4jRecord
    @Transient
    private String secondLastCompany;
    @Neo4jRecord
    @Transient
    private String thirdLastCompany;


    @Override
    @JsonIgnore
    public Set<UserAuthorization> getAuthorities() {
        Set<UserAuthorization> authorities = new HashSet<>();
        Iterator<Role> roleList = this.getRole().iterator();
        while (roleList.hasNext()) {
            authorities.add(new UserAuthorization().setUser(this).setAuthority(roleList.next().getName()));
        }
        return authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    @JsonIgnore
    public boolean isAccountNonExpired() {
        return isAccountNonExpired;
    }

    @Override
    @JsonIgnore
    public boolean isAccountNonLocked() {
        return isAccountNonLocked;
    }

    @Override
    @JsonIgnore
    public boolean isCredentialsNonExpired() {
        return isCredNonExpired;
    }

    @Override
    @JsonIgnore
    public boolean isEnabled() {
        return isEnabled;
    }

    public String getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getCompany() {
        return company;
    }

    public String getTitle() {
        return title;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public String getDeskPhone() {
        return deskPhone;
    }

    public String getCity() {
        return city;
    }

    public String getState() {
        return state;
    }

    public String getAddress1() {
        return address1;
    }

    public String getAddress2() {
        return address2;
    }

    public String getPincode() {
        return pincode;
    }

    public Date getDob() {
        return dob;
    }

    public String getCountry() {
        return country;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    @JsonIgnore
    public boolean isCredNonExpired() {
        return isCredNonExpired;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public void setDeskPhone(String deskPhone) {
        this.deskPhone = deskPhone;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    @SuppressWarnings("deprecation")
	public void setDob(String dob) {
        try {
            Date date = new Date(dob);
            this.dob = date;
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.dob = null;
    }

    public void setAccountNonExpired(boolean accountNonExpired) {
        isAccountNonExpired = accountNonExpired;
    }

    public void setAccountNonLocked(boolean accountNonLocked) {
        isAccountNonLocked = accountNonLocked;
    }

    public void setCredNonExpired(boolean credNonExpired) {
        isCredNonExpired = credNonExpired;
    }

    public void setEnabled(boolean enabled) {
        isEnabled = enabled;
    }

    public Set<Role> getRole() {
        return role;
    }

    public void setRole(Set<Role> role) {
        this.role = role;
    }

    public Set<String> getService() {
        return service;
    }

    public void setService(Set<String> service) {
        this.service = service;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getEmailType() {
        return emailType;
    }

    public void setEmailType(String emailType) {
        this.emailType = emailType;
    }

    public ProfessionalContactEnum getQuadrant() {
        return quadrant;
    }

    public void setQuadrant(ProfessionalContactEnum quadrant) {
        this.quadrant = quadrant;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getSocialMedia() {
        return socialMedia;
    }

    public void setSocialMedia(String socialMedia) {
        this.socialMedia = socialMedia;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public OrbitEnum getOrbits() {
        return orbits;
    }

    public void setOrbits(OrbitEnum orbits) {
        this.orbits = orbits;
    }

    public TouchPotentialEnum getPotential() {
        return potential;
    }

    public void setPotential(TouchPotentialEnum potential) {
        this.potential = potential;
    }

    public String getResumeLastUpdated() {
        return resumeLastUpdated;
    }

    public void setResumeLastUpdated(String resumeLastUpdated) {
        this.resumeLastUpdated = resumeLastUpdated;
    }

    public String getSourceOfResume() {
        return sourceOfResume;
    }

    public void setSourceOfResume(String sourceOfResume) {
        this.sourceOfResume = sourceOfResume;
    }

    public String getAssociation() {
        return association;
    }

    public void setAssociation(String association) {
        this.association = association;
    }

    public String getSkills() {
        return skills;
    }

    public void setSkills(String skills) {
        this.skills = skills;
    }

    public String getJobType() {
        return jobType;
    }

    public void setJobType(String jobType) {
        this.jobType = jobType;
    }

    public String getCurrentIndustry() {
        return currentIndustry;
    }

    public void setCurrentIndustry(String currentIndustry) {
        this.currentIndustry = currentIndustry;
    }

    public String getIndustryLookingFor() {
        return industryLookingFor;
    }

    public void setIndustryLookingFor(String industryLookingFor) {
        this.industryLookingFor = industryLookingFor;
    }

    public String getCurrentAreas() {
        return currentAreas;
    }

    public void setCurrentAreas(String currentAreas) {
        this.currentAreas = currentAreas;
    }

    public String getAreasLookingFor() {
        return areasLookingFor;
    }

    public void setAreasLookingFor(String areasLookingFor) {
        this.areasLookingFor = areasLookingFor;
    }

    public String getDesiredCompanyType() {
        return desiredCompanyType;
    }

    public void setDesiredCompanyType(String desiredCompanyType) {
        this.desiredCompanyType = desiredCompanyType;
    }

    public String getSalary() {
        return salary;
    }

    public void setSalary(String salary) {
        this.salary = salary;
    }

    public String getQualification() {
        return qualification;
    }

    public void setQualification(String qualification) {
        this.qualification = qualification;
    }

    public String getEducationBackground() {
        return educationBackground;
    }

    public void setEducationBackground(String educationBackground) {
        this.educationBackground = educationBackground;
    }

    public String getWillingToTravel() {
        return willingToTravel;
    }

    public void setWillingToTravel(String willingToTravel) {
        this.willingToTravel = willingToTravel;
    }

    public String getRelocation() {
        return relocation;
    }

    public void setRelocation(String relocation) {
        this.relocation = relocation;
    }

    public String getVisaStatus() {
        return visaStatus;
    }

    public void setVisaStatus(String visaStatus) {
        this.visaStatus = visaStatus;
    }

    public LifeCycleEnum getLifeCycle() {
        return lifeCycle;
    }

    public void setLifeCycle(LifeCycleEnum lifeCycle) {
        this.lifeCycle = lifeCycle;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public TouchPotentialEnum getTouch() {
        return touch;
    }

    public void setTouch(TouchPotentialEnum touch) {
        this.touch = touch;
    }

    public String getLineOfBusiness() {
        return lineOfBusiness;
    }

    public void setLineOfBusiness(String lineOfBusiness) {
        this.lineOfBusiness = lineOfBusiness;
    }

    public GeneralContactsType getGeneralContactsType() {
        return generalContactsType;
    }

    public void setGeneralContactsType(GeneralContactsType generalContactsType) {
        this.generalContactsType = generalContactsType;
    }

    public String getContactCardUploadUrl() {
        return contactCardUploadUrl;
    }

    public void setContactCardUploadUrl(String contactCardUploadUrl) {
        this.contactCardUploadUrl = contactCardUploadUrl;
    }

    public String getLastCompany() {
        return lastCompany;
    }

    public void setLastCompany(String lastCompany) {
        this.lastCompany = lastCompany;
    }

    public String getSecondLastCompany() {
        return secondLastCompany;
    }

    public void setSecondLastCompany(String secondLastCompany) {
        this.secondLastCompany = secondLastCompany;
    }

    public String getThirdLastCompany() {
        return thirdLastCompany;
    }

    public void setThirdLastCompany(String thirdLastCompany) {
        this.thirdLastCompany = thirdLastCompany;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", "User{", "}")
                .add("id='" + id + "'")
                .add("password='" + password + "'")
                .add("firstName='" + firstName + "'")
                .add("middleName='" + middleName + "'")
                .add("lastName='" + lastName + "'")
                .add("gender=" + gender)
                .add("email='" + email + "'")
                .add("company='" + company + "'")
                .add("title='" + title + "'")
                .add("mobilePhone='" + mobilePhone + "'")
                .add("deskPhone='" + deskPhone + "'")
                .add("city='" + city + "'")
                .add("state='" + state + "'")
                .add("address1='" + address1 + "'")
                .add("address2='" + address2 + "'")
                .add("pincode='" + pincode + "'")
                .add("country='" + country + "'")
                .add("imageUrl='" + imageUrl + "'")
                .add("dob=" + dob)
                .add("isAccountNonExpired=" + isAccountNonExpired)
                .add("isAccountNonLocked=" + isAccountNonLocked)
                .add("isCredNonExpired=" + isCredNonExpired)
                .add("isEnabled=" + isEnabled)
                .add("role=" + role)
                .add("service=" + service)
                .add("occupation='" + occupation + "'")
                .add("emailType='" + emailType + "'")
                .add("quadrant=" + quadrant)
                .add("location='" + location + "'")
                .add("socialMedia='" + socialMedia + "'")
                .add("age='" + age + "'")
                .add("experience='" + experience + "'")
                .add("orbits=" + orbits)
                .add("potential=" + potential)
                .add("touch=" + touch)
                .add("resumeLastUpdated='" + resumeLastUpdated + "'")
                .add("sourceOfResume='" + sourceOfResume + "'")
                .add("association='" + association + "'")
                .add("skills='" + skills + "'")
                .add("jobType='" + jobType + "'")
                .add("currentIndustry='" + currentIndustry + "'")
                .add("industryLookingFor='" + industryLookingFor + "'")
                .add("currentAreas='" + currentAreas + "'")
                .add("areasLookingFor='" + areasLookingFor + "'")
                .add("desiredCompanyType='" + desiredCompanyType + "'")
                .add("salary='" + salary + "'")
                .add("qualification='" + qualification + "'")
                .add("educationBackground='" + educationBackground + "'")
                .add("willingToTravel='" + willingToTravel + "'")
                .add("relocation='" + relocation + "'")
                .add("visaStatus='" + visaStatus + "'")
                .add("lifeCycle=" + lifeCycle)
                .add("maritalStatus='" + maritalStatus + "'")
                .add("lineOfBusiness='" + lineOfBusiness + "'")
                .add("generalContactsType=" + generalContactsType)
                .add("contactCardUploadUrl='" + contactCardUploadUrl + "'")
                .add("lastCompany='" + lastCompany + "'")
                .add("secondLastCompany='" + secondLastCompany + "'")
                .add("thirdLastCompany='" + thirdLastCompany + "'")
                .toString();
    }

    public static User generateFrom(UserRequest userRequest, User u) {
        u.setCompany(userRequest.getCompany());
        u.setTitle(userRequest.getTitle());
        u.setImageUrl(userRequest.getImageUrl());
        u.setFirstName(userRequest.getFirstName());
        u.setMiddleName(userRequest.getMiddleName());
        u.setLastName(userRequest.getLastName());
        u.setGender(Gender.valueOf(userRequest.getGender()));
        u.setEmail(userRequest.getEmail());
        u.setEmailType(userRequest.getEmailType());
        u.setMobilePhone(userRequest.getMobilePhone());
        u.setDeskPhone(userRequest.getDeskPhone());
        u.setCity(userRequest.getCity());
        u.setState(userRequest.getState());
        u.setAddress1(userRequest.getAddress1());
        u.setAddress2(userRequest.getAddress2());
        u.setPincode(userRequest.getPincode());
        u.setCountry(userRequest.getCountry());
        u.setDob(userRequest.getDob());
        u.setOccupation(userRequest.getOccupation());
        u.setQuadrant(ProfessionalContactEnum.valueOf(userRequest.getQuadrant()));
        u.setLocation(userRequest.getLocation());
        u.setSocialMedia(userRequest.getSocialMedia());
        u.setAge(userRequest.getAge());
        u.setExperience(userRequest.getExperience());
        u.setOrbits(OrbitEnum.valueOf(userRequest.getOrbits()));
        u.setPotential(TouchPotentialEnum.valueOf(userRequest.getPotential()));
        u.setTouch(TouchPotentialEnum.valueOf(userRequest.getTouch()));
        u.setResumeLastUpdated(userRequest.getResumeLastUpdated());
        u.setSourceOfResume(userRequest.getSourceOfResume());
        u.setAssociation(userRequest.getAssociation());
        u.setSkills(userRequest.getSkills());
        u.setJobType(userRequest.getJobType());
        u.setCurrentIndustry(userRequest.getCurrentIndustry());
        u.setIndustryLookingFor(userRequest.getIndustryLookingFor());
        u.setCurrentAreas(userRequest.getCurrentAreas());
        u.setAreasLookingFor(userRequest.getAreasLookingFor());
        u.setDesiredCompanyType(userRequest.getDesiredCompanyType());
        u.setSalary(userRequest.getSalary());
        u.setQualification(userRequest.getQualification());
        u.setEducationBackground(userRequest.getEducationBackground());
        u.setWillingToTravel(userRequest.getWillingToTravel());
        u.setRelocation(userRequest.getRelocation());
        u.setVisaStatus(userRequest.getVisaStatus());
        u.setLifeCycle(LifeCycleEnum.valueOf(userRequest.getLifeCycle()));
        u.setMaritalStatus(userRequest.getMaritalStatus());
        u.setLineOfBusiness(userRequest.getLineOfBusiness());
        CommonUtils.setCreateEntityFields(u);
        return u;
    }
}
