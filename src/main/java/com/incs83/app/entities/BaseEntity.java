/**
 * @Author Jayant Puri
 * @Created 13-Apr-2017
 */
package com.incs83.app.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.incs83.app.utils.application.DateUtils;
import com.incs83.app.utils.system.SecurityUtils;
import com.incs83.security.tokenFactory.UserContext;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import java.io.Serializable;
import java.util.Date;

@MappedSuperclass
public abstract class BaseEntity implements Serializable {

    private static final long serialVersionUID = -5723258342757016392L;

    @Column(nullable = false)
    @JsonIgnore
    private String createdBy;

    @Column
    @JsonIgnore
    private String updatedBy;

    @Column(nullable = false)
    @JsonIgnore
    private Date createdAt;

    @Column
    @JsonIgnore
    private Date updatedAt;

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    @PrePersist
    void setCreateEntityFields() {
        this.setCreatedAt(DateUtils.getCurrentDate());
        this.setUpdatedAt(DateUtils.getCurrentDate());
        UserContext userContext = SecurityUtils.getAuthenticatedUser();
        this.setCreatedBy(userContext != null ? userContext.getId() : "SYSTEM");
        this.setUpdatedBy(userContext != null ? userContext.getId() : "SYSTEM");
    }

    @PreUpdate
    void setUpdateEntityFields() {
        this.setUpdatedAt(DateUtils.getCurrentDate());
        UserContext userContext = SecurityUtils.getAuthenticatedUser();
        this.setUpdatedBy(userContext != null ? userContext.getId() : "SYSTEM");
    }

}