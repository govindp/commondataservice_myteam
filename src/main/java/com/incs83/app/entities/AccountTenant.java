/**
 * @Author Jayant Puri
 * @Created 28-May-2017
 */
package com.incs83.app.entities;

import com.incs83.app.annotations.Neo4jRecord;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "account_tenant")
public class AccountTenant extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 8043939240736104198L;

    @Id
    @Neo4jRecord
    private String id;

    @OneToOne
    private Account account;

    @Column(nullable = false, unique = true)
    @Neo4jRecord
    private String tenantName;

    @Column(nullable = false, unique = true)
    @Neo4jRecord
    private String tenantGuid;

    @Column(nullable = false, unique = true)
    private String tenantUrl;

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public String getTenantName() {
        return tenantName;
    }

    public void setTenantName(String tenantName) {
        this.tenantName = tenantName;
    }

    public String getTenantGuid() {
        return tenantGuid;
    }

    public void setTenantGuid(String tenantGuid) {
        this.tenantGuid = tenantGuid;
    }

    public String getTenantUrl() {
        return tenantUrl;
    }

    public void setTenantUrl(String tenantUrl) {
        this.tenantUrl = tenantUrl;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
