/**
 * @Author Jayant Puri
 * @Created 28-May-2017
 */
package com.incs83.app.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.incs83.app.common.AccountRequest;
import com.incs83.app.enums.AccountRequestStatus;
import com.incs83.app.utils.application.CommonUtils;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.stream.Collectors;

@Entity
@Table(name = "account_requisition_request")
public class AccountRequisitionRequest extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 8043939240736104198L;

    @Id
    private String id;

    @Column(nullable = false)
    private String companyName;

    @Column(nullable = false)
    private String firstName;

    @Column(nullable = false)
    private String lastName;

    @Column
    private String middleName;

    @Column(nullable = false, unique = true)
    private String emailId;

    @Column
    private String title;

    @Column
    @JsonIgnore
    private String state;

    @Column
    @JsonIgnore
    private String city;

    @Column
    @JsonIgnore
    private String country;

    @Column
    @JsonIgnore
    private String zipcode;

    @Column
    @JsonIgnore
    private String address;

    @Column(nullable = false)
    @JsonIgnore
    private String website;

    @Column(nullable = false)
    private String phoneNumber;

    @Column(nullable = false)
    private String servicesRequested;

    @Column
    private String lineOfBusiness;

    @Column(nullable = false)
    @JsonIgnore
    private String requestState;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getRequestState() {
        return requestState;
    }

    public void setRequestState(String requestState) {
        this.requestState = requestState;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getServicesRequested() {
        return servicesRequested;
    }

    public void setServicesRequested(String servicesRequested) {
        this.servicesRequested = servicesRequested;
    }

    public String getLineOfBusiness() {
        return lineOfBusiness;
    }

    public void setLineOfBusiness(String lineOfBusiness) {
        this.lineOfBusiness = lineOfBusiness;
    }

    public static AccountRequisitionRequest generateFrom(AccountRequest accountRequest) {
        AccountRequisitionRequest accountRequisitionRequest = new AccountRequisitionRequest();
        accountRequisitionRequest.setCompanyName(accountRequest.getCompanyName());
        accountRequisitionRequest.setTitle(accountRequest.getTitle());
        accountRequisitionRequest.setAddress((accountRequest.getAddress()));
        accountRequisitionRequest.setCity(accountRequest.getCity());
        accountRequisitionRequest.setEmailId(accountRequest.getEmailId());
        accountRequisitionRequest.setFirstName(accountRequest.getFirstName());
        accountRequisitionRequest.setMiddleName(accountRequest.getMiddleName());
        accountRequisitionRequest.setLastName(accountRequest.getLastName());
        accountRequisitionRequest.setPhoneNumber(accountRequest.getPhoneNumber());
        accountRequisitionRequest.setState(accountRequest.getState());
        accountRequisitionRequest.setWebsite(accountRequest.getWebsite());
        accountRequisitionRequest.setZipcode(accountRequest.getZipcode());
        accountRequisitionRequest.setId(CommonUtils.generateUUID());
        accountRequisitionRequest.setRequestState(AccountRequestStatus.PENDING.name());
        accountRequisitionRequest.setCountry(accountRequest.getCountry());
        accountRequisitionRequest.setServicesRequested(accountRequest.getServices().stream().collect(Collectors.joining(",")));
        accountRequisitionRequest.setLineOfBusiness(accountRequest.getLineOfBusiness());
        CommonUtils.setCreateEntityFields(accountRequisitionRequest);
        return accountRequisitionRequest;
    }
}
