/**
 * @author jayant
 * @created 10-Apr-2017
 */
package com.incs83.app.init;

import com.incs83.app.constants.ApplicationConstants;
import com.incs83.logger.LoggerFactory;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

@Configuration
public class PropertiesInitializer {

    private static final Logger LOG = LoggerFactory.make();

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertyConfigurer() {

        Resource resource;
        String activeProfile;
        PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer = new PropertySourcesPlaceholderConfigurer();

        // Get the Active Profile
        activeProfile = System.getProperty(ApplicationConstants.SPRING_PROFILE_PROPERTY);
        if (activeProfile == null) {
            LOG.info("No Environment Specified...Exiting");
            System.exit(1);
        }
        LOG.info("Environment :: " + activeProfile);
        resource = new ClassPathResource("/application-" + activeProfile + ".properties");

        // Load the Property file as per environment
        propertySourcesPlaceholderConfigurer.setLocation(resource);
        return propertySourcesPlaceholderConfigurer;
    }
}
