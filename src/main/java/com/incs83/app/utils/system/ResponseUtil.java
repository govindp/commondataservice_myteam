/**
 * @Author Jayant Puri
 * @Created 11-Apr-2017
 */
package com.incs83.app.utils.system;

import com.incs83.app.enums.ApiResponseCode;
import com.incs83.app.enums.abstraction.ResponseCode;
import com.incs83.app.responsedto.ApiResponseDTO;
import com.incs83.app.responsedto.ResponseDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

@Component
@SuppressWarnings({"rawtypes","unchecked"})
public class ResponseUtil {

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private ResponseUtil util;
    
	private static final Map data = new HashMap();

    public Locale getLocale(String locale) {
        return locale != null ? new Locale(locale) : Locale.UK;
    }


    private static ResponseUtil responseUtil;

    @PostConstruct
    private void init() {
        responseUtil = util;
    }

    public static ResponseUtil getBean() {
        return responseUtil;
    }

    public ResponseDTO ok() {
        String message = messageSource.getMessage(String.valueOf(ApiResponseCode.SUCCESS.getCode()), null, getLocale(null));
        return new ApiResponseDTO(message, data);
    }

    public ResponseDTO ok(Object data) {
        String message = messageSource.getMessage(String.valueOf(ApiResponseCode.SUCCESS.getCode()), null, getLocale(null));
        return new ApiResponseDTO(message, data);
    }

    public ResponseDTO ok(ResponseCode responseCode) {
        String message = messageSource.getMessage(String.valueOf(responseCode.getCode()), null, getLocale(null));
        return new ApiResponseDTO(message, data);
    }

    public ResponseDTO ok(Object data, ResponseCode responseCode) {
        String message = messageSource.getMessage(String.valueOf(responseCode.getCode()), null, getLocale(null));
        return new ApiResponseDTO(message, data);
    }

    public ResponseDTO ok(Object data, ResponseCode responseCode, String locale) {
        String message = messageSource.getMessage(String.valueOf(responseCode.getCode()), null, getLocale(locale));
        return new ApiResponseDTO(message, data);
    }

    public ResponseDTO exception(int code) {
        String message = messageSource.getMessage(String.valueOf(code), null, getLocale(null));
        return new ApiResponseDTO(code, message, data);
    }

    public ResponseDTO exception(int code, String message) {
        return new ApiResponseDTO(code, message, data);
    }

    public ResponseDTO exception(ResponseCode responseCode) {
        String message = messageSource.getMessage(String.valueOf(responseCode.getCode()), null, getLocale(null));
        return new ApiResponseDTO(responseCode.getCode(), message, data);
    }

    public ResponseDTO validationFailed(int code, String message) {
        return new ApiResponseDTO(code, message, data);
    }

}
