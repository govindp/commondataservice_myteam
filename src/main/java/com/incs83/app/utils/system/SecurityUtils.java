package com.incs83.app.utils.system;

import com.incs83.security.tokenFactory.UserContext;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * Created by jayant on 24/4/17.
 */
public class SecurityUtils {

    public static UserContext getAuthenticatedUser() {
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return authentication == null ? null : (authentication.getPrincipal() instanceof String ? null : (UserContext) authentication.getPrincipal());
    }
}
