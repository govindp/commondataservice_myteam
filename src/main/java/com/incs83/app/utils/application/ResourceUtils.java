/**
 * @Author Jayant Puri
 * @Created 29-May-2017
 */
package com.incs83.app.utils.application;

import com.incs83.app.enums.ResourceType;

import java.lang.reflect.Field;
import java.util.*;

/**
 * Created by rosrivas on 9/5/16.
 */
public class ResourceUtils {

    public static String generateId() {
        return UUID.randomUUID().toString().replace("-", "");
    }

    public static Map<String, Object> getFieldMap(ResourceType type, Object resource) throws IllegalAccessException {
        Map<String, Object> fieldMap = new HashMap<>();
        List<Field> fields = new ArrayList<>();
        getDeclaredFields(type.getClazz(), fields);
        for (Field field : fields) {
            field.setAccessible(true);
            fieldMap.put(field.getName(), field.get(resource));
        }
        return fieldMap;
    }

    public static <T> List<Field> getFields(T resource) throws IllegalArgumentException {
        List<Field> fields = new ArrayList<>();
        getDeclaredFields(resource.getClass(), fields);
        return fields;
    }

    private static void getDeclaredFields(Class<?> object, List<Field> fields) {
        fields.addAll(Arrays.asList(object.getDeclaredFields()));
        if (object.getSuperclass().getName().equals(Object.class.getName())) {
            return;
        }
        getDeclaredFields(object.getSuperclass(), fields);
    }
}
