package com.incs83.app.utils.application;

import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.*;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;


/**
 * Created by jayant on 1/5/17.
 */
@Component
public class SQLUtils {

    public static StringBuffer createWhereClauseWithAndCondition(LinkedHashMap<String, Object> clauseParams) {
        StringBuffer whereClauseQuery = new StringBuffer("where ");
        for (Map.Entry<String, Object> entry : clauseParams.entrySet()) {
            whereClauseQuery.append(entry.getKey() + " = ");
            if (entry.getValue() instanceof Long) {
                whereClauseQuery.append(entry.getValue());
            } else if (entry.getValue() instanceof String) {
                whereClauseQuery.append(" '" + entry.getValue() + "' ");
            }
            whereClauseQuery.append("and ");
        }
        return new StringBuffer(whereClauseQuery.toString().substring(0, whereClauseQuery.toString().length() - 4));
    }

    public static StringBuffer createWhereClauseWithOrCondition(LinkedHashMap<String, Object> clauseParams) {
        StringBuffer whereClauseQuery = new StringBuffer("where ");
        String param = null;
        for (Map.Entry<String, Object> entry : clauseParams.entrySet()) {
            param = param == null ? entry.getKey() : param;
            whereClauseQuery.append(param + " = ");
            if (entry.getValue() instanceof Long) {
                whereClauseQuery.append(entry.getValue());
            } else if (entry.getValue() instanceof String) {
                whereClauseQuery.append(" '" + entry.getValue() + "' ");
            }
            whereClauseQuery.append("or ");
        }
        return new StringBuffer(whereClauseQuery.toString().substring(0, whereClauseQuery.toString().length() - 3));
    }

    public static StringBuffer createWhereClauseWithoutCondition(LinkedHashMap<String, Object> clauseParams) {
        StringBuffer whereClauseQuery = new StringBuffer("where ");
        for (Map.Entry<String, Object> entry : clauseParams.entrySet()) {
            whereClauseQuery.append(entry.getKey() + " = ");
            if (entry.getValue() instanceof Long) {
                whereClauseQuery.append(entry.getValue());
            } else if (entry.getValue() instanceof String) {
                whereClauseQuery.append(" '" + entry.getValue() + "' ");
            }
        }
        return whereClauseQuery;
    }

    public static StringBuffer createWhereClauseWithInCondition(Set<Object> clauseParams, String param) {
        StringBuffer whereClauseQuery = new StringBuffer("where " + param + " in ( ");
        Iterator<Object> entry = clauseParams.iterator();
        while (entry.hasNext()) {
            Object obj = entry.next();
            if (obj instanceof Long) {
                whereClauseQuery.append(obj);
            } else if (obj instanceof String) {
                whereClauseQuery.append(" '" + obj + "' ");
            }
            whereClauseQuery.append(", ");
        }
        return new StringBuffer(whereClauseQuery.toString().substring(0, whereClauseQuery.toString().length() - 2)).append(" )");
    }

    public boolean createAccountFromSQL(Connection connection, String schemaName, String fileName) throws Exception {
        try (InputStream fs = getClass().getClassLoader().getResourceAsStream(fileName);
             BufferedReader br = new BufferedReader(new InputStreamReader(fs));) {
            connection.createStatement().execute("CREATE SCHEMA " + schemaName);
            connection.setCatalog(schemaName);
            StringBuilder sbOut = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null) {
                sbOut.append(line);
            }
            String sql = sbOut.toString();
            String[] commands = sql.split(";");
            for (String command : commands) {
                connection.createStatement().execute(command);
            }
            return true;
        } catch (Exception e) {
            throw new RuntimeException();
        }
    }

    public boolean checkDBCreated (Connection connection, String dbName) {
        boolean flag = false;
        try {
            PreparedStatement pst = connection.prepareStatement("SHOW DATABASES LIKE '" + dbName+"'");
             ResultSet rs = pst.executeQuery();
            flag = rs.next();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return flag;
    }
}
