/**
 * @Author Jayant Puri
 * @Created 11-Apr-2017
 */
package com.incs83.app.utils.application;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.incs83.app.constants.ApplicationConstants;
import com.incs83.app.entities.BaseEntity;
import com.incs83.app.entities.Role;
import com.incs83.app.entities.User;
import com.incs83.app.enums.ResourceType;
import com.incs83.app.mt.CurrentTenantIdentifier;
import com.incs83.app.service.generic.DataAccessService;
import com.incs83.app.utils.system.SecurityUtils;
import com.incs83.security.tokenFactory.Authority;
import com.incs83.security.tokenFactory.Entitlement;
import com.incs83.security.tokenFactory.UserContext;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.lang.management.OperatingSystemMXBean;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;
import java.util.UUID;
@Component
@SuppressWarnings({"rawtypes","unchecked"})
public class CommonUtils {
    
	@Autowired
    DataAccessService dataAccessService;

    public static final Logger LOG = LogManager.getLogger(CommonUtils.class);

    public static TreeMap<String, String> getSystemHealth() {
        TreeMap<String, String> healthStats = new TreeMap<String, String>();
        OperatingSystemMXBean operatingSystemMXBean = ManagementFactory.getOperatingSystemMXBean();
        for (Method method : operatingSystemMXBean.getClass().getDeclaredMethods()) {
            method.setAccessible(true);
            if (method.getName().startsWith(ApplicationConstants.GET) && Modifier.isPublic(method.getModifiers())) {
                Object value;
                try {
                    value = method.invoke(operatingSystemMXBean);
                } catch (Exception e) {
                    LOG.error("Error while fetching parameter for System Health");
                    value = e;
                }
                healthStats.put(method.getName().replaceAll(ApplicationConstants.GET, ApplicationConstants.EMPTY_STRING), String.valueOf(value));
            }
        }
        return healthStats;
    }

    public static String generateUUID() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }

	/*public static String encodePassword(String plainText){
        return new Md5PasswordEncoder().encodePassword(plainText,null);
	}*/

    public static void setCreateEntityFields(Object obj) {
        if (obj instanceof BaseEntity) {
            BaseEntity entity = (BaseEntity) obj;
            entity.setCreatedAt(DateUtils.getCurrentDate());
            UserContext userContext = SecurityUtils.getAuthenticatedUser();
            entity.setCreatedBy(userContext != null ? userContext.getId() : "SYSTEM");
        }
    }

    public static void setUpdateEntityFields(Object obj) {
        if (obj instanceof BaseEntity) {
            BaseEntity entity = (BaseEntity) obj;
            entity.setUpdatedAt(DateUtils.getCurrentDate());
            UserContext userContext = SecurityUtils.getAuthenticatedUser();
            entity.setUpdatedBy(userContext != null ? userContext.getId() : "SYSTEM");
        }
    }

    public static String getLocaleStringFromClientHTTPRequest(HttpServletRequest httpServletRequest) {
        return httpServletRequest.getLocale().getLanguage();
    }

	public static Authority generateScopePayloadForToken(User user) {
        List<Entitlement> entitlements = new ArrayList<>();
        Authority authority = new Authority();
        user.getRole().forEach(role -> {
            authority.setName(role.getName());
            authority.setService(role.getService());
            authority.setTenant(CurrentTenantIdentifier._tenantName.get());
            List<Entitlement> list = null;
            try {
                list = (List<Entitlement>) new ObjectMapper().readValue(role.getMapping(), List.class);
            } catch (IOException e) {
                e.printStackTrace();
            }
            for (int i = 0; i < list.size(); i++) {
                Entitlement entitlement = new ObjectMapper().convertValue(list.get(i), Entitlement.class);
                entitlements.add(entitlement);
            }
            authority.setEntitlements(entitlements);
        });
        return authority;
    }

    public static Role setRoleMappingWithEntitlement(Role r) {
        List<Entitlement> entitlements = new ArrayList<>();
        if (r.getMapping() == null) {
            r.setRoleEntitlement(new ArrayList<>());
        } else {
            List<Entitlement> list = null;
            try {
                list = (List<Entitlement>) new ObjectMapper().readValue(r.getMapping(), List.class);
            } catch (IOException e) {
                e.printStackTrace();
            }
            for (int i = 0; i < list.size(); i++) {
                Entitlement entitlement = new ObjectMapper().convertValue(list.get(i), Entitlement.class);
                entitlements.add(entitlement);
            }
            r.setRoleEntitlement(entitlements);
        }
        return r;
    }

    public static String nullObjToEmptyString(String obj) {
        if(obj==null){
            return "";
        }else {
            return obj.trim().replaceAll("'", " ").replaceAll("\"", " ");
        }
    }

    public String getUserNameFromId(String id) {
        String name = "";
        try {
            User u = (User)dataAccessService.read(ResourceType.USER,id);
            name = u.getFirstName() + " " + u.getLastName();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return  name;
    }


}
