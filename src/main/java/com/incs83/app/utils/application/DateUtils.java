package com.incs83.app.utils.application;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by jayant on 24/4/17.
 */
public class DateUtils {

    public static Date getCurrentDate() {
        return new Date();
    }

    public static boolean isStaleDate(Long date2) {
        return new Date().getTime() > date2;
    }

    public static String getCurrentTimeStampAsString() {
        return (String.valueOf(new Date().getTime()));
    }

    public static String parseDatetoYYYYMMDD(Date date) {
        try {
            return new SimpleDateFormat("yyyy-MM-dd").format(date).toString();
        } catch (Exception e) {
            return null;
        }
    }

    public static String parseDatetoMMDDYYYY(Date date, char separator) {
        try {
            return new SimpleDateFormat("MM" + separator + "dd" + separator + "yyyy").format(date).toString();
        } catch (Exception e) {
            return null;
        }
    }

}
