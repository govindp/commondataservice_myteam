package com.incs83.app.business;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.incs83.app.context.ExecutionContext;
import com.incs83.app.entities.Role;
import com.incs83.app.entities.User;
import com.incs83.app.enums.ApiResponseCode;
import com.incs83.app.enums.ResourceType;
import com.incs83.app.responsedto.dto.UserProfileDTO;
import com.incs83.app.security.exceptions.ApiException;
import com.incs83.app.service.generic.DataAccessService;
import com.incs83.app.utils.application.CommonUtils;
import com.incs83.logger.LoggerFactory;

/**
 * Created by jayant on 3/8/17.
 */
@Service
@SuppressWarnings("rawtypes")
public class ProfileService {

    @Autowired
    DataAccessService dataAccessService;

    private static final Logger LOG = LoggerFactory.make();

    public UserProfileDTO getProfile(String id) {
        User userAttributesfromMongo = null;
        User userAttributesfromSql = null;
        try {
            userAttributesfromMongo = (User) dataAccessService.readFromMongo(ResourceType.USER, id==null?ExecutionContext.CONTEXT.get().getUsercontext().getId():id);
            if(id==null){
                userAttributesfromSql = (User) dataAccessService.read(ResourceType.USER, ExecutionContext.CONTEXT.get().getUsercontext().getId());
            }
        } catch (Exception e) {
            LOG.error("Error while fetching user...", e);
            throw new ApiException(ApiResponseCode.ERROR);
        }
        UserProfileDTO userProfileDTO = new UserProfileDTO();
        if(id==null) {
            Set<Role> roles = userAttributesfromSql.getRole();
            Iterator rolesItr = roles.iterator();
            Role r = null;
            while (rolesItr.hasNext()) {
                r = (Role) rolesItr.next();
                CommonUtils.setRoleMappingWithEntitlement(r);
            }
            userProfileDTO.setRole(roles);
            userProfileDTO.setService(userAttributesfromSql.getService());
        }
        HashMap<String,Object> businessInfoMap = new HashMap<>();
        HashMap<String,Object> personalInfoMap = new HashMap<>();
        HashMap<String,Object> demographicMap = new HashMap<>();
        HashMap<String,Object> professionalMap = new HashMap<>();

        // Personal Info
        personalInfoMap.put("name",userAttributesfromMongo.getFirstName()+" "+userAttributesfromMongo.getLastName());
        personalInfoMap.put("gender",userAttributesfromMongo.getGender());
        personalInfoMap.put("company",Arrays.asList(userAttributesfromMongo.getCompany().toString().split(";")));
        personalInfoMap.put("title",userAttributesfromMongo.getTitle());
        personalInfoMap.put("phone",userAttributesfromMongo.getMobilePhone());
        personalInfoMap.put("imageUrl",userAttributesfromMongo.getImageUrl());
        personalInfoMap.put("email",userAttributesfromMongo.getEmail());
        personalInfoMap.put("social",Arrays.asList(userAttributesfromMongo.getSocialMedia().toString().split(";")));

        // Business Info
        businessInfoMap.put("quadrant",userAttributesfromMongo.getQuadrant());
        businessInfoMap.put("orbit",userAttributesfromMongo.getOrbits());

        // Demographic Info
        demographicMap.put("city",userAttributesfromMongo.getCity());
        demographicMap.put("state",userAttributesfromMongo.getState());
        demographicMap.put("addr1",userAttributesfromMongo.getAddress1());
        demographicMap.put("addr2",userAttributesfromMongo.getAddress2());
        demographicMap.put("country",userAttributesfromMongo.getCountry());
        demographicMap.put("pincode",userAttributesfromMongo.getPincode());

        // Professional Info
        professionalMap.put("skills",Arrays.asList(userAttributesfromMongo.getSkills().toString().split(";")));
        professionalMap.put("qualification",Arrays.asList(userAttributesfromMongo.getQualification().toString().split(";")));
        professionalMap.put("education",Arrays.asList(userAttributesfromMongo.getEducationBackground().toString().split(";")));
        professionalMap.put("experience",userAttributesfromMongo.getExperience());

        userProfileDTO.setBusinessInfoInfo(businessInfoMap);
        userProfileDTO.setDemographicInfo(demographicMap);
        userProfileDTO.setPersonalInfo(personalInfoMap);
        userProfileDTO.setProfessionalInfo(professionalMap);
        return userProfileDTO;
    }
}
