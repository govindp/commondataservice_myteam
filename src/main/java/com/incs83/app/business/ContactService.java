package com.incs83.app.business;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.commons.io.FileUtils;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.incs83.app.common.CardContactRequest;
import com.incs83.app.common.CardRequest;
import com.incs83.app.config.CloudinaryConfig;
import com.incs83.app.constants.ApplicationConstants;
import com.incs83.app.constants.queries.CardSQL;
import com.incs83.app.context.ExecutionContext;
import com.incs83.app.dao.Repository;
import com.incs83.app.entities.Card;
import com.incs83.app.entities.Company;
import com.incs83.app.entities.User;
import com.incs83.app.enums.ApiResponseCode;
import com.incs83.app.enums.ContactsType;
import com.incs83.app.enums.Gender;
import com.incs83.app.enums.GeneralContactsType;
import com.incs83.app.enums.LifeCycleEnum;
import com.incs83.app.enums.OrbitEnum;
import com.incs83.app.enums.ProfessionalContactEnum;
import com.incs83.app.enums.ResourceType;
import com.incs83.app.enums.TouchPotentialEnum;
import com.incs83.app.responsedto.dto.CompanyDTO;
import com.incs83.app.responsedto.dto.CompanyDetailsDTO;
import com.incs83.app.security.exceptions.ApiException;
import com.incs83.app.service.data.CloudinaryService;
import com.incs83.app.service.data.HTTPService;
import com.incs83.app.service.data.MongoService;
import com.incs83.app.service.data.Neo4jService;
import com.incs83.app.utils.application.CommonUtils;
import com.incs83.logger.LoggerFactory;

/**
 * Created by hari on 1/8/17.
 */
@Service
@SuppressWarnings({"rawtypes","unchecked", "resource" })
public class ContactService {

    private static final Logger LOG = LoggerFactory.make();

    @Autowired
    private Neo4jService neo4jService;

    @Autowired
    private HTTPService httpService;

    @Autowired
    private Environment environment;

    @Autowired
    private CloudinaryService cloudinaryService;

    @Autowired
    private Repository<Card> cardRepository;

    @Autowired
    private CloudinaryConfig cloudinaryConfig;

    @Autowired
    private MongoService mongoService;

    public void uploadContacts(MultipartFile file, ContactsType type) throws Exception {
        if (null == file || file.isEmpty()) {
            throw new ApiException(ApiResponseCode.FILE_IS_EMPTY);
        }
        File fileOnDisk = new File(Calendar.getInstance().getTimeInMillis() + file.getOriginalFilename());
        FileUtils.writeByteArrayToFile(fileOnDisk, file.getBytes());
        LOG.info(String.format("Excel found with name : %s ", file.getOriginalFilename()));
        String userId = ExecutionContext.CONTEXT.get().getUsercontext().getId();
        List<Object> contactList = new ArrayList<>();
        if (type.equals(ContactsType.MY)) {
            contactList = readUserFromExcel(fileOnDisk).stream().map(mapToUser).collect(Collectors.toList());
            neo4jService.uploadContacts(contactList, userId, type);
        } else if (type.equals(ContactsType.COMPANY)) {
            processCompanyInfo(fileOnDisk);
        }
        fileOnDisk.delete();
    }

    private Function<String, User> mapToUser = (line) -> {
        String[] p = line.split(ApplicationConstants.DOUBLE_HYPHEN);
        int i = 0;
        User u = new User();
        u.setId(CommonUtils.generateUUID());
        u.setCompany(CommonUtils.nullObjToEmptyString(p[i]));
        u.setTitle(CommonUtils.nullObjToEmptyString(p[++i]));
        u.setImageUrl(CommonUtils.nullObjToEmptyString(p[++i]));
        u.setFirstName(CommonUtils.nullObjToEmptyString(p[++i]));
        u.setMiddleName(CommonUtils.nullObjToEmptyString(p[++i]));
        u.setLastName(CommonUtils.nullObjToEmptyString(p[++i]));
        u.setGender(Gender.valueOf(CommonUtils.nullObjToEmptyString(p[++i]).toUpperCase()));
        u.setEmail(CommonUtils.nullObjToEmptyString(p[++i]));
        u.setEmailType(CommonUtils.nullObjToEmptyString(p[++i]));
        u.setMobilePhone(CommonUtils.nullObjToEmptyString(p[++i]));
        u.setDeskPhone(CommonUtils.nullObjToEmptyString(p[++i]));
        u.setCity(CommonUtils.nullObjToEmptyString(p[++i]));
        u.setState(CommonUtils.nullObjToEmptyString(p[++i]));
        u.setAddress1(CommonUtils.nullObjToEmptyString(p[++i]));
        u.setAddress2(CommonUtils.nullObjToEmptyString(p[++i]));
        u.setPincode(CommonUtils.nullObjToEmptyString(p[++i]));
        u.setCountry(CommonUtils.nullObjToEmptyString(p[++i]));
        u.setDob(new Date());
        ++i;
        u.setOccupation(CommonUtils.nullObjToEmptyString(p[++i]));
        u.setQuadrant(ProfessionalContactEnum.valueOf(CommonUtils.nullObjToEmptyString(p[++i])));
        u.setLocation(CommonUtils.nullObjToEmptyString(p[++i]));
        u.setSocialMedia(CommonUtils.nullObjToEmptyString(p[++i]));
        u.setAge(CommonUtils.nullObjToEmptyString(p[++i]));
        u.setExperience(CommonUtils.nullObjToEmptyString(p[++i]));
        u.setOrbits(OrbitEnum.valueOf(CommonUtils.nullObjToEmptyString(p[++i])));
        u.setPotential(TouchPotentialEnum.valueOf(CommonUtils.nullObjToEmptyString(p[++i]).toUpperCase()));
        u.setTouch(TouchPotentialEnum.valueOf(CommonUtils.nullObjToEmptyString(p[++i]).toUpperCase()));
        u.setResumeLastUpdated(CommonUtils.nullObjToEmptyString(p[++i]));
        u.setSourceOfResume(CommonUtils.nullObjToEmptyString(p[++i]));
        u.setAssociation(CommonUtils.nullObjToEmptyString(p[++i]));
        u.setSkills(CommonUtils.nullObjToEmptyString(p[++i]));
        u.setJobType(CommonUtils.nullObjToEmptyString(p[++i]));
        u.setCurrentIndustry(CommonUtils.nullObjToEmptyString(p[++i]));
        u.setIndustryLookingFor(CommonUtils.nullObjToEmptyString(p[++i]));
        u.setCurrentAreas(CommonUtils.nullObjToEmptyString(p[++i]));
        u.setAreasLookingFor(CommonUtils.nullObjToEmptyString(p[++i]));
        u.setDesiredCompanyType(CommonUtils.nullObjToEmptyString(p[++i]));
        u.setSalary(CommonUtils.nullObjToEmptyString(p[++i]));
        u.setQualification(CommonUtils.nullObjToEmptyString(p[++i]));
        u.setEducationBackground(CommonUtils.nullObjToEmptyString(p[++i]));
        u.setWillingToTravel(CommonUtils.nullObjToEmptyString(p[++i]));
        u.setRelocation(CommonUtils.nullObjToEmptyString(p[++i]));
        u.setVisaStatus(CommonUtils.nullObjToEmptyString(p[++i]));
        u.setLifeCycle(LifeCycleEnum.valueOf(CommonUtils.nullObjToEmptyString(p[++i])));
        u.setMaritalStatus(CommonUtils.nullObjToEmptyString(p[++i]));
        u.setLineOfBusiness(CommonUtils.nullObjToEmptyString(p[++i]));
        CommonUtils.setCreateEntityFields(u);
        return u;
    };

    private Function<String, User> mapToCompanyUser = (line) -> {
        String[] p = line.split(ApplicationConstants.DOUBLE_HYPHEN);
        int i = 0;
        User u = new User();
        u.setId(CommonUtils.generateUUID());
        u.setPassword("");
        u.setFirstName(p[i]);
        u.setMiddleName(p[++i]);
        u.setLastName(p[++i]);
        u.setGender(Gender.valueOf(p[++i].equals("NA") ? null : p[i].toUpperCase()));
        u.setEmail(p[++i]);
        u.setCompany(p[++i]);
        u.setTitle(p[++i]);
        u.setMobilePhone(p[++i]);
        u.setDeskPhone(p[++i]);
        u.setCity(p[++i]);
        u.setState(p[++i]);
        u.setAddress1(p[++i]);
        u.setAddress2(p[++i]);
        u.setPincode(p[++i]);
        u.setCountry(p[++i]);
        u.setImageUrl(p[++i]);
        u.setDob(p[++i]);
        u.setOccupation(p[++i]);
        u.setEmailType(p[++i]);
        u.setQuadrant(ProfessionalContactEnum.valueOf(p[++i].equals("NA") ? null : p[i].toUpperCase()));
        u.setLocation(p[++i]);
        u.setSocialMedia(p[++i]);
        u.setAge(p[++i]);
        u.setExperience(p[++i]);
        u.setOrbits(p[++i].equals("NA") ? null : OrbitEnum.valueOf(p[i].toUpperCase()));
        u.setPotential(TouchPotentialEnum.valueOf(p[++i].equals("NA") ? null : p[i].toUpperCase()));
        u.setTouch(TouchPotentialEnum.valueOf(p[++i].equals("NA") ? null : p[i].toUpperCase()));
        u.setResumeLastUpdated(p[++i]);
        u.setSourceOfResume(p[++i]);
        u.setAssociation(p[++i]);
        u.setSkills(p[++i]);
        u.setJobType(p[++i]);
        u.setCurrentIndustry(p[++i]);
        u.setIndustryLookingFor(p[++i]);
        u.setCurrentAreas(p[++i]);
        u.setAreasLookingFor(p[++i]);
        u.setDesiredCompanyType(p[++i]);
        u.setSalary(p[++i]);
        u.setQualification(p[++i]);
        u.setEducationBackground(p[++i]);
        u.setWillingToTravel(p[++i]);
        u.setRelocation(p[++i]);
        u.setVisaStatus(p[++i]);
        u.setLifeCycle(LifeCycleEnum.valueOf(p[++i].equals("NA") ? null : p[i].toUpperCase()));
        u.setMaritalStatus(p[++i]);
        u.setLineOfBusiness(p[++i]);
        u.setGeneralContactsType(GeneralContactsType.PROFESSIONAL);
        u.setLastCompany(p[++i]);
        u.setSecondLastCompany(p[++i]);
        u.setThirdLastCompany(p[++i]);
        CommonUtils.setCreateEntityFields(u);
        return u;
    };

    private Function<String, Company> mapToCompany = (line) -> {
        String[] p = line.split(ApplicationConstants.DOUBLE_HYPHEN);
        int i = 0;
        Company company = new Company();
        company.setId(CommonUtils.generateUUID());
        company.setCompanyName(p[i]);
        company.setWebsite(p[++i]);
        company.setContact(p[++i]);
        company.setLocation(p[++i]);
        company.setSocialMedia(p[++i]);
        company.setCurrentStatus(p[++i]);
        company.setAssociation(p[++i]);
        company.setRelationship(p[++i]);
        company.setTouch(TouchPotentialEnum.valueOf(p[++i].equals("NA") ? null : p[i].toUpperCase()));
        company.setOrbits(OrbitEnum.valueOf(p[++i].equals("NA") ? null : p[i].toUpperCase()));
        company.setNearTermPotential(p[++i]);
        company.setLongTermPotential(p[++i]);
        company.setMarketLevel(p[++i]);
        company.setEntityType(p[++i]);
        company.setIndustryType(p[++i]);
        company.setRevenue(p[++i]);
        company.setCompanySize(p[++i]);
        company.setTechnology(p[++i]);
        company.setCategory(p[++i]);
        company.setCompetitors(p[++i]);
        CommonUtils.setCreateEntityFields(company);
        return company;
    };

    public Object uploadContactCard(CardRequest files) {
        if (Objects.isNull(files.getFrontImg()) || files.getFrontImg().isEmpty()) {
            throw new ApiException(ApiResponseCode.FILE_IS_EMPTY);
        }
        String frontFilename = null, backFilename = null, frontText = null, backText = null;

        frontFilename = ApplicationConstants.CARD_FOLDER + Calendar.getInstance().getTimeInMillis() + "_" + files.getFrontImg().getOriginalFilename();
        frontText = uploadFile(files.getFrontImg(), frontFilename);
        if (Objects.nonNull(files.getBackImg()) && !files.getBackImg().isEmpty()) {
            backFilename = ApplicationConstants.CARD_FOLDER + Calendar.getInstance().getTimeInMillis() + "_" + files.getBackImg().getOriginalFilename();
            backText = uploadFile(files.getBackImg(), backFilename);
        }
        Card card = new Card()
                .setId(CommonUtils.generateUUID())
                .setFrontImg(frontFilename)
                .setFrontText(frontText)
                .setBackImg(backFilename)
                .setBackText(backText)
                .setUserId(ExecutionContext.CONTEXT.get().getUsercontext().getId());
        CommonUtils.setCreateEntityFields(card);
        card = cardRepository.create(card);
        card.setFrontImg(cloudinaryConfig.getUrl() + card.getFrontImg() + ".jpg");
        if (Objects.nonNull(files.getBackImg()) && !files.getBackImg().isEmpty()) {
            card.setBackImg(cloudinaryConfig.getUrl() + card.getBackImg() + ".jpg");
        }
        return card;
    }

    private String uploadFile(MultipartFile file, String filename) {
        HttpResponse response = null;
        String responseString = null;
        File fileOnDisk = new File(filename);
        try {
            FileUtils.writeByteArrayToFile(fileOnDisk, file.getBytes());
            cloudinaryService.uploadImage(fileOnDisk, filename, new ArrayList<String>());
            HashMap<String, String> headers = new HashMap<>();
            MultipartEntityBuilder builder = MultipartEntityBuilder.create();
            builder.addTextBody("apikey", environment.getProperty(ApplicationConstants.OCR_APP_KEY));
            builder.addBinaryBody("file", fileOnDisk);
            response = httpService.doPost(environment.getProperty(ApplicationConstants.OCR_APP_URL), headers, null, builder.build());
            if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
                throw new ApiException(ApiResponseCode.ERROR);
            }
            responseString = EntityUtils.toString(response.getEntity(), "UTF-8");
            LOG.info("Parsed Text :: " + responseString);
        } catch (IOException e) {
            throw new ApiException(ApiResponseCode.FILE_IO_ERROR);
        } finally {
            fileOnDisk.delete();
        }
        Map parsedData = null;
        try {
            parsedData = new ObjectMapper().readValue(responseString, HashMap.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        List list = (List) parsedData.get("ParsedResults");
        Map map = (Map) list.get(0);
        String text = (String) map.get("ParsedText");
        return text;
    }


    public List<Object> toUser(CardContactRequest cardContactRequest) {
        User u = new User();
        u.setId(CommonUtils.generateUUID());
        u.setFirstName(CommonUtils.nullObjToEmptyString(cardContactRequest.getFirstName()));
        u.setLastName(CommonUtils.nullObjToEmptyString(cardContactRequest.getLastName()));
        u.setGender(cardContactRequest.getGender());
        u.setMobilePhone(CommonUtils.nullObjToEmptyString(cardContactRequest.getMobile()));
        u.setDeskPhone(CommonUtils.nullObjToEmptyString(cardContactRequest.getLandline()));
        u.setEmail(CommonUtils.nullObjToEmptyString(cardContactRequest.getEmail()));
        u.setEmailType("Work");
        u.setImageUrl("NA");
        u.setAddress1(CommonUtils.nullObjToEmptyString(cardContactRequest.getAddress()));
        u.setDob(cardContactRequest.getDob());
        u.setCompany(CommonUtils.nullObjToEmptyString(cardContactRequest.getCompany()));
        u.setTitle(CommonUtils.nullObjToEmptyString(cardContactRequest.getDesignation()));
        u.setTouch(TouchPotentialEnum.HIGH); //ConnectionStrength
        u.setQuadrant(ProfessionalContactEnum.BUSINESS_STRATEGY); //Quadrant
        u.setOrbits(OrbitEnum.STAKE_HOLDER); //Orbit
        CommonUtils.setCreateEntityFields(u);
        List<Object> user = new ArrayList<>();
        user.add(u);
        return user;
    }

    private List<String> readUserFromExcel(File file) {
        List<String> fileLines = new ArrayList<>();
        try {
            FileInputStream excelFile = new FileInputStream(file);
            Workbook workbook = new XSSFWorkbook(excelFile);
            Sheet sheet = workbook.getSheetAt(0);
            fileLines = iterateSheet(sheet);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        fileLines.remove(0); //Removing Header
        return fileLines;
    }

    @SuppressWarnings("deprecation")
	private List<String> iterateSheet(Sheet sheet) {
        List<String> fileLines = new ArrayList<>();
        Iterator<Row> iterator = sheet.iterator();
        while (iterator.hasNext()) {
            Row currentRow = iterator.next();
            Iterator<Cell> cellIterator = currentRow.iterator();
            StringBuffer line = new StringBuffer();
            while (cellIterator.hasNext()) {
                Cell currentCell = cellIterator.next();
                if (currentCell.getCellTypeEnum() == CellType.STRING) {
                    line.append(currentCell.getStringCellValue().trim().replaceAll("\n", "") + ApplicationConstants.DOUBLE_HYPHEN);
                } else if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {
                    line.append(String.valueOf(currentCell.getNumericCellValue()).trim().replaceAll("\n", "") + ApplicationConstants.DOUBLE_HYPHEN);
                }
            }
            fileLines.add(line.toString());
        }
        return fileLines;
    }

    private List<String> processCompanyInfo(File file) {
        List<String> fileLines = new ArrayList<>();
        XSSFWorkbook workbook = null;
        try {
            FileInputStream excelFile = new FileInputStream(file);
            workbook = new XSSFWorkbook(excelFile);
            Sheet sheet = null;
            sheet = workbook.getSheet("staff");
            if (Objects.nonNull(sheet)) {
                fileLines = iterateSheet(sheet);
                fileLines.remove(0); //Removing Header
            } else {
                //TODO handle sheet not found
            }
            List<Object> users = fileLines.stream().map(mapToCompanyUser).collect(Collectors.toList());
            mongoService.create(ResourceType.USER, users);
            sheet = workbook.getSheet("profile");
            if (Objects.nonNull(sheet)) {
                fileLines = iterateSheet(sheet);
                fileLines.remove(0); //Removing Header
            } else {
                //TODO handle sheet not found
            }
            List<String> userIds = users.stream().map(u -> {
                User user = (User) u;
                return user.getId();
            }).collect(Collectors.toList());
            List<Object> companies = fileLines.stream().map(mapToCompany).collect(Collectors.toList());
            Company company = (Company) companies.get(0);
            company.setUsers(userIds);
            mongoService.create(ResourceType.COMPANY, companies);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return fileLines;
    }

    public List<Card> fetchCards() {
        String userId = ExecutionContext.CONTEXT.get().getUsercontext().getId();
        HashMap<String, String> params = new HashMap<>();
        params.put("userId", userId);
        List<Card> cards = cardRepository.read(CardSQL.GET_CARD_BY_USER_ID, params);
        cards = cards.stream().map(card -> {
            card.setFrontImg(cloudinaryConfig.getUrl() + card.getFrontImg() + ".jpg");
            if (Objects.nonNull(card.getBackImg()))
                card.setBackImg(cloudinaryConfig.getUrl() + card.getBackImg() + ".jpg");
            return card;
        }).collect(Collectors.toList());
        return cards;
    }

    public void deleteCard(String cardId) {
        try {
            Card card = cardRepository.read(ResourceType.CARD.getClazz(), cardId);
            cloudinaryService.deleteImage(card.getFrontImg());
            if (Objects.nonNull(card.getBackImg()))
                cloudinaryService.deleteImage(card.getBackImg());
            cardRepository.delete(ResourceType.CARD.getClazz(), cardId);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<CompanyDTO> getCompanies() {
        List<Company> companies = mongoService.read(ResourceType.COMPANY, new HashMap<>());
        return companies.stream().map(company -> new CompanyDTO(company.getId(), company.getCompanyName())).collect(Collectors.toList());
    }

    public CompanyDetailsDTO getCompanyDetails(String companyId) {
        Company company = (Company) mongoService.read(ResourceType.COMPANY, companyId);
        List<User> users = company.getUsers().stream().map(u -> (User) mongoService.read(ResourceType.USER, u)).collect(Collectors.toList());
        return CompanyDetailsDTO.mapFromCompany(company, users);
    }
}
