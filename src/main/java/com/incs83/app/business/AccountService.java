package com.incs83.app.business;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.log4j.Logger;
import org.hibernate.engine.jdbc.connections.spi.MultiTenantConnectionProvider;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.incs83.app.common.AccountRequest;
import com.incs83.app.config.BmsConfig;
import com.incs83.app.constants.ApplicationConstants;
import com.incs83.app.constants.queries.AccountSQL;
import com.incs83.app.entities.Account;
import com.incs83.app.entities.AccountRequisitionRequest;
import com.incs83.app.entities.AccountTenant;
import com.incs83.app.entities.AccountTenantDetails;
import com.incs83.app.entities.BaseEntity;
import com.incs83.app.entities.Role;
import com.incs83.app.entities.User;
import com.incs83.app.enums.AccountRequestActions;
import com.incs83.app.enums.AccountRequestStatus;
import com.incs83.app.enums.ApiResponseCode;
import com.incs83.app.enums.Gender;
import com.incs83.app.enums.LifeCycleEnum;
import com.incs83.app.enums.OrbitEnum;
import com.incs83.app.enums.ProfessionalContactEnum;
import com.incs83.app.enums.ResourceType;
import com.incs83.app.enums.TouchPotentialEnum;
import com.incs83.app.mt.CurrentTenantIdentifier;
import com.incs83.app.security.exceptions.ApiException;
import com.incs83.app.service.application.MailService;
import com.incs83.app.service.data.Neo4jService;
import com.incs83.app.service.generic.DataAccessService;
import com.incs83.app.utils.application.CommonUtils;
import com.incs83.app.utils.application.SQLUtils;
import com.incs83.logger.LoggerFactory;
import com.incs83.security.tokenFactory.Entitlement;
import com.incs83.security.tokenFactory.Permission;

/**
 * Created by jayant on 1/5/17.
 */
@Service
@SuppressWarnings({"rawtypes","unchecked"})
public class AccountService {

    private static final Logger LOG = LoggerFactory.make();

    @Autowired
    private SQLUtils sqlUtils;

    @Autowired
    private MultiTenantConnectionProvider connectionProvider;

	@Autowired
    private DataAccessService dataAccessService;

    @Autowired
    private BCryptPasswordEncoder encoder;

    @Autowired
    private Neo4jService neo4jService;

    @Autowired
    private BmsConfig bmsConfig;

    @Autowired
    private MailService mailService;

    public void requestAccountCreation(AccountRequest accountRequest) {
        AccountRequisitionRequest accountRequisitionRequest = null;
        try {
            LOG.info("Creating an Account Request...");
            accountRequisitionRequest = AccountRequisitionRequest.generateFrom(accountRequest);
            dataAccessService.create(ResourceType.ACCOUNT_REQUEST, accountRequisitionRequest);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<AccountRequisitionRequest> getAllRequests(String type) {
        List<AccountRequisitionRequest> request = new ArrayList<>();
        LOG.info(String.format("Fetch %s Account Creation Requests...", type == null ? "all" : type));
        if (type == null) {
            try {
                request = (List<AccountRequisitionRequest>) dataAccessService.read(ResourceType.ACCOUNT_REQUEST);
            } catch (Exception e) {
                e.printStackTrace();
                throw new ApiException(ApiResponseCode.ERROR_PROCESSING_REQUEST);
            }
        } else {
            if (Objects.equals(type, AccountRequestStatus.PENDING.name())) {
                request = getAllPendingRequests();
            } else if (Objects.equals(type, AccountRequestStatus.PROCESSED.name())) {
                request = getAllProcessedRequests();
            }
        }
        return request;
    }

    public List<AccountRequisitionRequest> getAllPendingRequests() {
        List<AccountRequisitionRequest> pendingRequest = new ArrayList<>();
        try {
            LOG.info("Fetch All Pending Account Creation Requests...");
            pendingRequest = (List<AccountRequisitionRequest>) dataAccessService.read(ResourceType.ACCOUNT_REQUEST,
                    AccountSQL.GET_ALL_PENDING_REQUESTS, new HashMap<>());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return pendingRequest;
    }

    public List<AccountRequisitionRequest> getAllProcessedRequests() {
        List<AccountRequisitionRequest> processedRequest = new ArrayList<>();
        try {
            LOG.info("Fetch All Processed Account Creation Requests...");
            processedRequest = (List<AccountRequisitionRequest>) dataAccessService.read(ResourceType.ACCOUNT_REQUEST,
                    AccountSQL.GET_ALL_PROCESSED_REQUESTS, new HashMap<>());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return processedRequest;
    }

    public AccountRequisitionRequest getRequestDetails(String id) {
        AccountRequisitionRequest accountRequisitionRequest = null;
        try {
            accountRequisitionRequest = (AccountRequisitionRequest) dataAccessService.read(ResourceType.ACCOUNT_REQUEST,
                    id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return accountRequisitionRequest;
    }

    public void approveRequest(String token, String id) throws Exception {
        AccountRequisitionRequest accountRequisitionRequest = (AccountRequisitionRequest) dataAccessService
                .read(ResourceType.ACCOUNT_REQUEST, id);
        Account account = Account.generateFrom(accountRequisitionRequest);
        approveRequest(account);
    }

    public void processRequest(String token, String id, String action) throws Exception {
        boolean saveFlag = false;
        String tenantName = null;
        AccountRequisitionRequest accountRequisitionRequest = (AccountRequisitionRequest) dataAccessService
                .read(ResourceType.ACCOUNT_REQUEST, id);
        Account account = Account.generateFrom(accountRequisitionRequest);
        if (action.equals(AccountRequestActions.REJECT.name())) {
            saveFlag = rejectRequest(account);
        } else if (action.equals(AccountRequestActions.APPROVE.name())) {
            saveFlag = approveRequest(account);
            if (saveFlag) {
                AccountTenant accountTenant = null;
                accountTenant = getAccountTenant(accountRequisitionRequest.getCompanyName().split(" ")[0], account);
                saveFlag = dataAccessService.create(ResourceType.ACCOUNT_TENANT, accountTenant);
                if (saveFlag) {
                    tenantName = accountTenant.getTenantGuid();
                    AccountTenantDetails accountTenantDetails = getAccountTenantDetails(accountTenant);
                    saveFlag = dataAccessService.create(ResourceType.ACCOUNT_TENANT_DETAILS, accountTenantDetails);
                    saveFlag = processAccountCreationFormalities(token, saveFlag, tenantName, accountRequisitionRequest, account, accountTenant);
                }
            }
        }

        if (saveFlag) {
            markRequestAsProcessed(tenantName, accountRequisitionRequest);
        } else {
            throw new RuntimeException("Failed to Update Account Request TaskTemplate");
        }
    }

    private void markRequestAsProcessed(String tenantName, AccountRequisitionRequest accountRequisitionRequest) throws Exception {
        boolean saveFlag;
        accountRequisitionRequest.setRequestState("PROCESSED");
        CommonUtils.setUpdateEntityFields(accountRequisitionRequest);
        saveFlag = dataAccessService.update(ResourceType.ACCOUNT_REQUEST, accountRequisitionRequest);
        if (saveFlag) {
            User admin = createTenantAdmin(accountRequisitionRequest, tenantName);
            if (admin != null) {
                neo4jService.createUser(admin,accountRequisitionRequest);
            }
        }
        HashMap<String, String> mailParams = new HashMap<>();
        mailParams.put("user", accountRequisitionRequest.getFirstName() + " " + accountRequisitionRequest.getLastName());
        mailParams.put("url", "http://52.38.175.21:3000");
        mailParams.put("username", accountRequisitionRequest.getEmailId());
        mailService.sendOnboardingEmail(accountRequisitionRequest.getEmailId(), mailParams);
    }

    private boolean processAccountCreationFormalities(String token, boolean saveFlag, String tenantName, AccountRequisitionRequest accountRequisitionRequest, Account account, AccountTenant accountTenant) throws Exception {
        if (saveFlag) {
            saveFlag = sqlUtils.createAccountFromSQL(connectionProvider.getAnyConnection(),
                    tenantName, "createTenant.sql");
            if (saveFlag) {
                if (bmsConfig.isAvailable()) {
                    if (accountRequisitionRequest.getServicesRequested().contains("BMS")) {
                        saveFlag = provisionBmsInstance(token, accountTenant.getTenantName(), accountTenant.getTenantGuid(), account.getCompanyName());
                    }
                }
                if (saveFlag) {
                    saveFlag = neo4jService.createAccountTenantRelation(account, accountTenant);
                }
            }
        }
        return saveFlag;
    }

    private AccountTenantDetails getAccountTenantDetails(AccountTenant accountTenant) {
        AccountTenantDetails accountTenantDetails = new AccountTenantDetails()
                .setAccountTenant(accountTenant)
                .setId(CommonUtils.generateUUID())
                .setMongoCollectionName(accountTenant.getTenantGuid())
                .setMongoUsername(ApplicationConstants.EMPTY_STRING)
                .setMongoUrl(ApplicationConstants.EMPTY_STRING)
                .setMongoPassword(ApplicationConstants.EMPTY_STRING)
                .setMysqlSchemaName(accountTenant.getTenantGuid())
                .setMysqlUrl(ApplicationConstants.EMPTY_STRING)
                .setMysqlUsername(ApplicationConstants.EMPTY_STRING)
                .setMysqlPassword(ApplicationConstants.EMPTY_STRING)
                .setNeo4jNodeName(accountTenant.getTenantGuid())
                .setNeo4jUrl(ApplicationConstants.EMPTY_STRING)
                .setNeo4jUsername(ApplicationConstants.EMPTY_STRING)
                .setNeo4jPassword(ApplicationConstants.EMPTY_STRING);
        CommonUtils.setCreateEntityFields(accountTenantDetails);
        return accountTenantDetails;
    }

    private boolean approveRequest(Account account) throws Exception {
        boolean saveFlag;
        account.setComments("Approved Request");
        account.setStatus("APPROVED");
        account.setEnabled(true);
        account.setPassword(encoder.encode("veryeasy1"));
        saveFlag = dataAccessService.create(ResourceType.ACCOUNT, account);
        return saveFlag;
    }

    private AccountTenant getAccountTenant(String s, Account account) {
        AccountTenant accountTenant;
        accountTenant = new AccountTenant();
        accountTenant.setAccount(account);
        accountTenant.setId(CommonUtils.generateUUID());
        accountTenant.setTenantGuid(s.toLowerCase() + "_"
                + Calendar.getInstance().getTimeInMillis());
        accountTenant.setTenantName(s.toLowerCase());
        accountTenant.setTenantUrl(
                "http://localhost:8080/" + s.toLowerCase());
        CommonUtils.setCreateEntityFields(accountTenant);
        return accountTenant;
    }

    private boolean rejectRequest(Account account) throws Exception {
        boolean saveFlag;
        account.setComments("Rejected Request");
        account.setStatus("REJECTED");
        account.setEnabled(false);
        account.setPassword("");
        saveFlag = dataAccessService.create(ResourceType.ACCOUNT, account);
        return saveFlag;
    }

    private boolean provisionBmsInstance(String token, String tenantName, String tenantGuid, String companyName) throws IOException {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpPost request = new HttpPost(bmsConfig.getBaseUrl() + "/83incs/api/v1/bms/tenant");
        request.addHeader("Content-type", "application/json");
        request.addHeader("X-Authorization", token);
        JSONObject object = new JSONObject();
        object.put("companyName", companyName);
        object.put("tenantGuid", tenantGuid);
        object.put("tenantName", tenantName);
        StringEntity entity = new StringEntity(object.toString());
        request.setEntity(entity);
        HttpResponse httpResponse = httpClient.execute(request);
        if (httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
            LOG.info("BMS instance created successfully.");
            return true;
        } else {
            LOG.error("BMS instance creation failed.");
            return false;
        }
    }

    private User createTenantAdmin(AccountRequisitionRequest accountRequisitionRequest, String tenantName) throws Exception {
        CurrentTenantIdentifier._tenantIdentifier.set(tenantName);
        List<Entitlement> entitlements = new ArrayList<>();
        Entitlement entitlement = new Entitlement();
        entitlement.setResource("ALL");
        // TODO Remove All and Assign All BMS Resources
        List<String> permissions = new ArrayList<>();
        permissions.add(Permission.ALL.getValue());
        entitlement.setPermissions(permissions);
        entitlements.add(entitlement);

        Role tenantAdminRole = new Role();
        tenantAdminRole.setName("Admin");
        tenantAdminRole.setId(CommonUtils.generateUUID());
        tenantAdminRole.setMapping(new ObjectMapper().writeValueAsString(entitlements));
        tenantAdminRole.setService(accountRequisitionRequest.getServicesRequested());
        CommonUtils.setCreateEntityFields(tenantAdminRole);
        dataAccessService.create(ResourceType.ROLE, tenantAdminRole);

        Set<Role> tenatAdmin = new HashSet<>();
        Iterable<BaseEntity> roles = dataAccessService.read(ResourceType.ROLE);
        Iterator<BaseEntity> roleIterator = roles.iterator();
        while (roleIterator.hasNext()) {
            tenatAdmin.add((Role) roleIterator.next());
        }

        LOG.info("Creating Tenant Admin User");
        User tenantAdmin = new User();
        tenantAdmin.setEmail(accountRequisitionRequest.getEmailId());
        tenantAdmin.setPassword(encoder.encode("veryeasy1"));
        tenantAdmin.setDob(new GregorianCalendar(1980, Calendar.APRIL, 9).getTime());
        tenantAdmin.setFirstName(accountRequisitionRequest.getFirstName());
        tenantAdmin.setMiddleName(accountRequisitionRequest.getMiddleName());
        tenantAdmin.setLastName(accountRequisitionRequest.getLastName());
        tenantAdmin.setMobilePhone(accountRequisitionRequest.getPhoneNumber());
        tenantAdmin.setRole(tenatAdmin);
        tenantAdmin.setId(CommonUtils.generateUUID());
        tenantAdmin.setAccountNonExpired(true);
        tenantAdmin.setCredNonExpired(true);
        tenantAdmin.setImageUrl("NA");
        tenantAdmin.setAccountNonLocked(true);
        tenantAdmin.setEnabled(true);
        tenantAdmin.setGender(Gender.MALE);
        Set<String> services = new HashSet<>();
        services.add(accountRequisitionRequest.getServicesRequested());
        tenantAdmin.setService(services);
        tenantAdmin.setCompany(accountRequisitionRequest.getCompanyName());
        tenantAdmin.setTitle(accountRequisitionRequest.getTitle());

        // Should be Supplied At the time of Tenant Admin Creation
        tenantAdmin.setQuadrant(ProfessionalContactEnum.BUSINESS_STRATEGY);
        tenantAdmin.setOrbits(OrbitEnum.STAKE_HOLDER);
        tenantAdmin.setTouch(TouchPotentialEnum.ENGAGED);
        tenantAdmin.setLineOfBusiness(accountRequisitionRequest.getLineOfBusiness());
        tenantAdmin.setLifeCycle(LifeCycleEnum.JOB_MATURITY_LEVEL_2);

        CommonUtils.setCreateEntityFields(tenantAdmin);
        if (dataAccessService.create(ResourceType.USER, tenantAdmin)) {
            return tenantAdmin;
        }
        return null;
    }

    public AccountTenant getAccountTenant(String tenant) {
        AccountTenant accountTenant = null;
        try {
            HashMap<String,String> params = new HashMap<>();
            params.put("tenantName",tenant);
            List<AccountTenant> accountTenants = (List<AccountTenant>) dataAccessService.read(ResourceType.ACCOUNT_TENANT,AccountSQL.TENANT_BY_NAME,params);
            accountTenant = accountTenants.isEmpty() ? null : accountTenants.get(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return accountTenant;
    }

    public List<Account> getAllAccounts() {
        List<Account> request = new ArrayList<>();
        try {
            LOG.info("Fetch All created Accounts...");
            request = (List<Account>) dataAccessService.read(ResourceType.ACCOUNT);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return request;
    }
}

