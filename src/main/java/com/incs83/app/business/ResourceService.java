package com.incs83.app.business;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.incs83.app.common.*;
import com.incs83.app.context.ExecutionContext;
import com.incs83.app.entities.*;
import com.incs83.app.enums.ApiResponseCode;
import com.incs83.app.enums.ResourceType;
import com.incs83.app.security.exceptions.ApiException;
import com.incs83.app.service.data.Neo4jService;
import com.incs83.app.service.generic.AuthenticationFilterService;
import com.incs83.app.service.generic.DataAccessService;
import com.incs83.app.statemachine.Persist;
import com.incs83.app.utils.application.CommonUtils;
import com.incs83.logger.LoggerFactory;
import com.incs83.security.tokenFactory.Entitlement;
import com.incs83.security.tokenFactory.Permission;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by rosrivas on 7/2/17.
 */
@Service
@SuppressWarnings({"rawtypes","unchecked","unused"})
public class ResourceService {

    @Autowired
    private DataAccessService dataAccessService;

    @Autowired
    private BCryptPasswordEncoder encoder;

    @Autowired
    private Neo4jService neo4jService;

    @Autowired
    private Persist persist;

    private static final Logger LOG = LoggerFactory.make();

    public void addSubState(AddSubStateRequest addSubStateRequest) throws Exception {
        try {
            TaskTemplate taskTemplate = (TaskTemplate) dataAccessService.read(ResourceType.TASK_TEMPLATE, addSubStateRequest.getId());
            Set<TaskTemplateDetails> updatedTaskTemplateDetails = new HashSet();
            updatedTaskTemplateDetails.addAll(taskTemplate.getTaskTemplateDetails());
            for (TaskTemplateDetails taskTemplateDetails : addSubStateRequest.getTaskTemplateDetails()) {
                taskTemplateDetails.setId(CommonUtils.generateUUID());
                CommonUtils.setCreateEntityFields(taskTemplateDetails);
                updatedTaskTemplateDetails.add(taskTemplateDetails);
            }
            taskTemplate.setTaskTemplateDetails(updatedTaskTemplateDetails);
            dataAccessService.update(ResourceType.TASK_TEMPLATE, taskTemplate);
        } catch (Exception e) {
            LOG.info("Failed while TaskTemplate creation....");
            throw e;
        }
    }

    public void createState(StateRequest stateRequest) throws Exception {
        TaskTemplate taskTemplate = new TaskTemplate();
        taskTemplate.setId(CommonUtils.generateUUID());
        CommonUtils.setCreateEntityFields(taskTemplate);
        taskTemplate.setName(stateRequest.getName());
        Set<TaskTemplateDetails> updatedTaskTemplateDetails = new HashSet();
        for (TaskTemplateDetails taskTemplateDetails : stateRequest.getTaskTemplateDetails()) {
            taskTemplateDetails.setId(CommonUtils.generateUUID());
            CommonUtils.setCreateEntityFields(taskTemplateDetails);
            updatedTaskTemplateDetails.add(taskTemplateDetails);
        }
        taskTemplate.setTaskTemplateDetails(updatedTaskTemplateDetails);
        try {
            dataAccessService.create(ResourceType.TASK_TEMPLATE, taskTemplate);
        } catch (Exception e) {
            LOG.info("Failed while TaskTemplate creation....");
            throw e;
        }
    }

    public List<TaskTemplate> getStates(String id) {
        List<TaskTemplate> taskTemplates = new ArrayList<>();
        try {
            if (id != null) {
                taskTemplates.add((TaskTemplate) dataAccessService.read(ResourceType.TASK_TEMPLATE, id));
            } else {
                taskTemplates = (List<TaskTemplate>) dataAccessService.read(ResourceType.TASK_TEMPLATE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return taskTemplates;
    }

    public void createRole(RoleRequest roleRequest) throws Exception {

        Role role = new Role();
        role.setId(CommonUtils.generateUUID());
        role.setName(roleRequest.getName());
        role.setService(roleRequest.getService());
        role.setMapping(createRoleEntitlementMapping(roleRequest.getRoleEntitlement()));

        CommonUtils.setCreateEntityFields(role);
        try {
            dataAccessService.create(ResourceType.ROLE, role);
        } catch (Exception e) {
            LOG.error("Failed while Role creation....");
            throw new ApiException(ApiResponseCode.ERROR);
        }
    }

    public void updateRole(String id, RoleRequest roleRequest) {
        try {
            Role role = (Role) dataAccessService.read(ResourceType.ROLE, id);
            if (roleRequest.getName() != null) {
                role.setName(roleRequest.getName());
            }
            if (roleRequest.getService() != null) {
                role.setName(roleRequest.getService());
            }
            if (roleRequest.getRoleEntitlement() != null) {
                role.setMapping(createRoleEntitlementMapping(roleRequest.getRoleEntitlement()));
            }
            CommonUtils.setUpdateEntityFields(role);
            dataAccessService.update(ResourceType.ROLE, role);
        } catch (Exception e) {
            LOG.error("Error while updating role...", e);
            throw new ApiException(ApiResponseCode.ERROR);
        }
    }

    public List<Role> getRoles(String id) {
        List<Role> roles;
        roles = new ArrayList<>();
        Role r = null;
        try {
            if (id != null) {
                r = (Role) dataAccessService.read(ResourceType.ROLE, id);
                if (r != null) {
                    CommonUtils.setRoleMappingWithEntitlement(r);
                    roles.add(r);
                }
            } else {
                roles = (List<Role>) dataAccessService.read(ResourceType.ROLE);
                Iterator rolesItr = roles.iterator();
                while (rolesItr.hasNext()) {
                    r = (Role) rolesItr.next();
                    CommonUtils.setRoleMappingWithEntitlement(r);
                }
            }
        } catch (Exception e) {
            LOG.error("Error while fetching role...", e);
            throw new ApiException(ApiResponseCode.ERROR);
        }
        return roles;
    }

    public void deleteRole(String id) {
        try {
            dataAccessService.delete(ResourceType.ROLE, id);
        } catch (Exception e) {
            LOG.error("Error while deleting role...", e);
            throw new ApiException(ApiResponseCode.ERROR);
        }
    }

    public void updateUser(String id, UserRequest userRequest) throws Exception {

        /*try {
            User user = (User) dataAccessService.read(ResourceType.USER, (Object) id);
            List<Field> fields = ResourceUtils.getFields(user);
            List<Object> role = (List<Object>) payload.get("role");
            if (role != null) {
                LinkedHashMap<String, Object> searchableKeysMap = new LinkedHashMap<>();
                searchableKeysMap.put(ApplicationConstants.NAME, "ROLE_USER");
                Set<Object> params = new HashSet<>();
                params.addAll(role);
                Iterable<BaseEntity> roles = dataAccessService.read(ResourceType.ROLE, searchableKeysMap, "IN", params,
                        "id");
                Iterator<BaseEntity> roleIterator = roles.iterator();
                Set<Role> roleSet = new HashSet<>();
                while (roleIterator.hasNext()) {
                    roleSet.add((Role) roleIterator.next());
                }
                user.setRole(roleSet);
            }
            List<String> service = (List<String>) payload.get("service");
            if (service != null) {
                Set<String> services = new HashSet<>();
                services.addAll(service);
                user.setService(services);
            }
            for (Field field : fields) {
                Object obj = payload.get(field.getName());
                field.setAccessible(true);
                if (!payload.containsKey(field.getName()) || field.getName().equals("role")) {
                    continue;
                }
                field.set(user, payload.get(field.getName()));
            }
            Session sess = neo4jService.getNeo4jSession();
            Map<String, String> userFields = new HashMap<>();
            userFields.put("email", user.getEmail());
            Record record = neo4JDao.getNode(User.class, userFields, sess);
            if (dataAccessService.update(ResourceType.USER, user)) {
                if (record != null) {
                    neo4JDao.updateNode(user, null, sess);
                }
            }
            neo4jService.closeSession(sess);
        } catch (Exception e) {
            LOG.error("Failed while updating user....", e);
            throw new ApiException(ApiResponseCode.ERROR);
        }*/
    }

    public void deleteUser(String id) {
        try {
            dataAccessService.delete(ResourceType.USER, id);
        } catch (Exception e) {
            LOG.error("Error while deleting user...", e);
            throw new ApiException(ApiResponseCode.ERROR);
        }
    }

    public void createUser(UserRequest userRequest) throws Exception {
        User u = new User();
        u.setId(CommonUtils.generateUUID());
        Set<Role> roles = new HashSet<>();
        Iterator<String> itr = userRequest.getRoles().iterator();
        while (itr.hasNext()) {
            roles.add((Role) dataAccessService.read(ResourceType.ROLE, itr.next()));
        }
        u.setRole(roles);
        if (userRequest.getService() != null) {
            Set<String> services = new HashSet<>();
            services.addAll(userRequest.getService());
            u.setService(services);
        } else {
            Set<String> services = new HashSet<>();
            services.add("CDS");
            u.setService(services);
        }
        u.setPassword(encoder.encode("veryeasy1"));
        User.generateFrom(userRequest,u);
        boolean saveFlag = dataAccessService.create(ResourceType.USER, u);
        if (saveFlag) {
            neo4jService.createUser(u,null);

        }
    }

    public List<User> getUsers(String id) {
        List<User> users = new ArrayList<>();
        try {
            if (id != null) {
                User u;
                u = (User) dataAccessService.read(ResourceType.USER, id);
                if (!Objects.isNull(u)) {
                    Set<Role> roles = u.getRole();
                    Iterator rolesItr = roles.iterator();
                    Role r = null;
                    while (rolesItr.hasNext()) {
                        r = (Role) rolesItr.next();
                        CommonUtils.setRoleMappingWithEntitlement(r);
                    }
                    users.add(u);
                }
            } else {
                users = (List<User>) dataAccessService.read(ResourceType.USER);
                Iterator<User> usrItr = users.iterator();
                while (usrItr.hasNext()) {
                    Set<Role> roles = usrItr.next().getRole();
                    Iterator rolesItr = roles.iterator();
                    Role r = null;
                    while (rolesItr.hasNext()) {
                        r = (Role) rolesItr.next();
                        CommonUtils.setRoleMappingWithEntitlement(r);
                    }
                }
            }
        } catch (Exception e) {
            LOG.error("Error while fetching user...", e);
            throw new ApiException(ApiResponseCode.ERROR);
        }
        return users.stream().filter(p->!p.getRole().iterator().next().getName().equals("ROLE_TENANT_ADMIN")).collect(Collectors.toList());
    }

    private String createRoleEntitlementMapping(List<RoleEntitlement> roleEntitlements) throws Exception {
        List<Entitlement> entitlements = new ArrayList<>();
        if (roleEntitlements != null) {
            roleEntitlements.forEach(roleEntitlement -> {
                entitlements.add(roleEntitlement.mapToEntitlement());
            });
        }
        return new ObjectMapper().writeValueAsString(entitlements);
    }

    public List<String> getPermissions() {
        return Arrays.stream(Permission.values()).map(Permission::getValue).collect(Collectors.toList());
    }

    public List<String> getEntitlements(String service) {
        List<String> entitlements = new ArrayList<>();
        StringTokenizer stringTokenizer = new StringTokenizer(ExecutionContext.CONTEXT.get().getUsercontext().getAuthority().getService(), ",");
        while (stringTokenizer.hasMoreTokens()) {
            if (service.equals(stringTokenizer.nextToken())) {
                Enumeration<String> enumeration = AuthenticationFilterService.getAuthenticatedResources().get(service).keys();
                while (enumeration.hasMoreElements()) {
                    entitlements.add(enumeration.nextElement());
                }
            }
        }
        return entitlements;
    }



    public void deleteState(String id) {
        try {
            dataAccessService.delete(ResourceType.TASK_TEMPLATE, id);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}

