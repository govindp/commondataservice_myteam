package com.incs83.app.business;

import com.incs83.app.common.AccountRequest;
import com.incs83.app.common.TaskRequest;
import com.incs83.app.constants.queries.TaskSQL;
import com.incs83.app.context.ExecutionContext;
import com.incs83.app.constants.queries.UserSQL;
import com.incs83.app.entities.*;
import com.incs83.app.enums.ResourceType;
import com.incs83.app.responsedto.dto.MyTaskDTO;
import com.incs83.app.service.generic.DataAccessService;
import com.incs83.app.statemachine.Persist;
import com.incs83.app.utils.application.CommonUtils;
import com.incs83.logger.LoggerFactory;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by jayant on 11/8/17.
 */
@Service
@SuppressWarnings({"rawtypes","unchecked","unused"})
public class TaskService {

    private static final Logger LOG = LoggerFactory.make();

    @Autowired
    private DataAccessService dataAccessService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private Persist persist;

    @Autowired
    private CommonUtils commonUtils;

    public void createTask(TaskRequest taskRequest) throws Exception {
        Task task = Task.generateFrom(taskRequest);
        try {
            dataAccessService.create(ResourceType.TASK, task);
            //persist.change(task.getId(), null, "ASSIGN", "CDS", null);
        } catch (Exception e) {
            LOG.info("Failed while Task creation....");
            throw e;
        }
    }

    public void assignTask(AccountRequest accountRequest, String id) {
        //TODO: need to get email from request
        String email = "admin@83incs.com";
        String query = UserSQL.GET_USER_BY_EMAIL;
        HashMap<String, Object> params = new HashMap<>();
        params.put("email", email);
        User user = null;
        try {
            user = (User) dataAccessService.read(ResourceType.USER, query, params).iterator().next();
            Task task = (Task) dataAccessService.read(ResourceType.TASK, id);
            task.setAssignee(user.getId());
            dataAccessService.update(ResourceType.TASK, task);
            accountService.requestAccountCreation(accountRequest);
            //persist.change(id,null, "ASSIGN", "CDS", null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteTask(String id) {
        try {
            dataAccessService.delete(ResourceType.TASK, id);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public HashMap<String,List<MyTaskDTO>> getTasksForUser(String id) {
        List<Task> tasks = new ArrayList<>();
        List<MyTaskDTO> assignedTasks = new ArrayList<>();
        HashMap<String, String> queryParms = new HashMap<>();
        queryParms.put("assignee", id==null?ExecutionContext.CONTEXT.get().getUsercontext().getId():id);
        try {
            tasks = (List<Task>) dataAccessService.read(ResourceType.TASK, TaskSQL.TASK_FOR_ASSIGNEE, queryParms);
        } catch (Exception e) {
            e.printStackTrace();
        }
        for (Task t :tasks){
            MyTaskDTO myTaskDTO = new MyTaskDTO();
            String name = commonUtils.getUserNameFromId(t.getCreatedBy());
            myTaskDTO.setAvatar(name.split(" ")[0].substring(0,1).toUpperCase() + name.split(" ")[1].substring(0,1).toUpperCase());
            myTaskDTO.setCreatedByName(name);
            myTaskDTO.setCreatedBy(t.getCreatedBy());
            myTaskDTO.setDescription(t.getDescription());
            myTaskDTO.setDueDate(t.getDueDate());
            myTaskDTO.setName(t.getLabel());
            myTaskDTO.setShortDescription(t.getShortDescription());
            myTaskDTO.setCreatedAt(String.valueOf(t.getCreatedAt()));
            myTaskDTO.setState(t.getState());
            myTaskDTO.setId(t.getId());
            myTaskDTO.setService(t.getService());
            myTaskDTO.setPriority(t.getPriority());
            assignedTasks.add(myTaskDTO);
        }
        HashMap<String,List<MyTaskDTO>> resultMap = new HashMap<>();
        resultMap.put("assigned",assignedTasks.stream().filter(p->p.getState().equals("ASSIGNED")).collect(Collectors.toList()));
        resultMap.put("inProgress",assignedTasks.stream().filter(p->p.getState().equals("IN_PROGRESS")).collect(Collectors.toList()));
        resultMap.put("completed",assignedTasks.stream().filter(p->p.getState().equals("COMPLETED")).collect(Collectors.toList()));
        return resultMap;
    }
}
