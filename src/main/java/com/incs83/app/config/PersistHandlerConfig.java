package com.incs83.app.config;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.Executors;

import org.hibernate.engine.jdbc.connections.spi.MultiTenantConnectionProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.statemachine.StateMachine;

import com.incs83.app.constants.queries.TaskSQL;
import com.incs83.app.entities.AccountTenant;
import com.incs83.app.entities.Task;
import com.incs83.app.entities.TaskDetail;
import com.incs83.app.entities.TaskTemplate;
import com.incs83.app.entities.TaskTemplateDetails;
import com.incs83.app.enums.ResourceType;
import com.incs83.app.mt.CurrentTenantIdentifier;
import com.incs83.app.service.generic.DataAccessService;
import com.incs83.app.statemachine.Persist;
import com.incs83.app.statemachine.PersistStateMachineHandler;
import com.incs83.app.utils.application.CommonUtils;
import com.incs83.app.utils.application.SQLUtils;

/**
 * Created by rosrivas on 7/23/17.
 */
@Configuration
@SuppressWarnings({"rawtypes","unchecked","unused"})
public class PersistHandlerConfig {

    @Autowired
    private StateMachine<String, String> stateMachine;

    @Autowired
    private DataAccessService dataAccessService;

    @Autowired
    private SQLUtils sqlUtils;

    @Autowired
    private MultiTenantConnectionProvider connectionProvider;

    @Bean
    public Persist persist() {
        return new Persist(persistStateMachineHandler());
    }

    @Bean
    public PersistStateMachineHandler persistStateMachineHandler() {
        return new PersistStateMachineHandler(stateMachine);
    }

    @Scheduled(fixedDelay = 100000)
    public void startTaskExecution() {
        /*try {
            CurrentTenantIdentifier._tenantIdentifier.set(CurrentTenantIdentifier.DEFAULT_TENANT);
            List<AccountTenant> accountTenants = (List<AccountTenant>) dataAccessService.read(ResourceType.ACCOUNT_TENANT);
            Executors.newSingleThreadExecutor().execute(new Runnable() {
                @Override
                public void run() {
                    runInitializerForTenant(CurrentTenantIdentifier.DEFAULT_TENANT);
                }
            });
            for (AccountTenant accountTenant : accountTenants) {
                String dbName = accountTenant.getTenantGuid();
                Executors.newSingleThreadExecutor().execute(new Runnable() {
                    @Override
                    public void run() {
                        runInitializerForTenant(dbName);
                    }
                });
            }
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }*/
    }

	@Async
    public void runInitializerForTenant(String tenant) {
        /*try {
            if(!sqlUtils.checkDBCreated(connectionProvider.getAnyConnection(),tenant)){
                return;
            }
            System.out.println("Initializer: " + Thread.currentThread().getId());
            CurrentTenantIdentifier._tenantIdentifier.set(tenant);
            List<Task> tasks = (List<Task>) dataAccessService
                    .read(ResourceType.TASK, TaskSQL.TASK_NEW, new HashMap<>());
            if (Objects.isNull(tasks)) {
                return;
            }
            for (Task task : tasks) {
                HashMap<String, String> params = new HashMap<>();
                params.put("name", task.getLabel());
                List<TaskTemplate> taskTemplates = (List<TaskTemplate>) dataAccessService.read(ResourceType.TASK_TEMPLATE,
                        TaskSQL.TEMPLATE_BY_LABEL, params);
                Set<TaskDetail> details = new HashSet<>();
                for (TaskTemplate t : taskTemplates) {
                    Set<TaskTemplateDetails> taskTemplateDetails = t.getTaskTemplateDetails();
                    for (TaskTemplateDetails td : taskTemplateDetails) {
                        TaskDetail detail = new TaskDetail();
                        detail.setSequence(td.getSequence());
                        detail.setEndpoint(td.getEndpoint());
                        detail.setState("PENDING");
                        detail.setId(CommonUtils.generateUUID());
                        detail.setName(td.getName());
                        CommonUtils.setCreateEntityFields(detail);
                        detail.setTask(task);
                        dataAccessService.create(ResourceType.TASK_DETAIL, detail);
                    }
                }
                //task.getTaskDetails().clear();
                //task.setTaskDetails(details);
                //task.setState("READY");
                persist().change(task.getId(), null, "INITIALIZE", "CDS", null);
            }
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }*/
    }

}
