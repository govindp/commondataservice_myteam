package com.incs83.app.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Neo4jConfig {


    @Value("${neo4jdb.hostname}")
    private String dbHostName;

    @Value("${neo4jdb.portnumber}")
    private String portNumber;

    @Value("${neo4jdb.username}")
    private String userName;

    @Value("${neo4jdb.password}")
    private String password;

    @Value("${neo4jdb.poolSize}")
    private String poolSize;

    public String getDbHostName() {
        return dbHostName;
    }

    public void setDbHostName(String dbHostName) {
        this.dbHostName = dbHostName;
    }

    public String getPortNumber() {
        return portNumber;
    }

    public void setPortNumber(String portNumber) {
        this.portNumber = portNumber;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPoolSize() {
        return poolSize;
    }

    public void setPoolSize(String poolSize) {
        this.poolSize = poolSize;
    }
}
