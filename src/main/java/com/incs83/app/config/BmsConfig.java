package com.incs83.app.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * Created by rosrivas on 7/8/17.
 */
@Configuration
public class BmsConfig {

    @Value("${bms.baseurl}")
    private String baseUrl;

    @Value("${bms.available}")
    private boolean isAvailable;

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public boolean isAvailable() {
        return isAvailable;
    }

    public void setAvailable(boolean available) {
        isAvailable = available;
    }
}
