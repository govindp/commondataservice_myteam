package com.incs83.app.config;

import org.springframework.context.annotation.Bean;
import org.springframework.statemachine.config.StateMachineConfigurerAdapter;
import org.springframework.statemachine.config.builders.StateMachineModelConfigurer;
import org.springframework.statemachine.config.model.StateMachineModelFactory;

import com.incs83.app.statemachine.ApplicationStateMachineModelFactory;

/**
 * Created by rosrivas on 7/15/17.
 */
//@Configuration
//@EnableStateMachine
//@EnableStateMachineFactory
public class StateMachineFactoryConfig
        extends StateMachineConfigurerAdapter<String, String> {


    @Override
    public void configure(StateMachineModelConfigurer<String, String> model) throws Exception {
        model.withModel()
                .factory(modelFactory());
    }

    @Bean
    public StateMachineModelFactory<String, String> modelFactory() {
        return new ApplicationStateMachineModelFactory();
    }
}

