package com.incs83.app.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.StringJoiner;

/**
 * Created by hari on 16/8/17.
 */
@ConfigurationProperties(prefix = "cloudinary")
@Configuration
public class CloudinaryConfig {
    private String name;
    private String key;
    private String secret;
    private String folder;
    private String url;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFolder() {
        return folder;
    }

    public void setFolder(String folder) {
        this.folder = folder;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", "CloudinaryConfig{", "}")
                .add("name='" + name + "'")
                .add("key='" + key + "'")
                .add("secret='" + secret + "'")
                .add("folder='" + folder + "'")
                .add("url='" + url + "'")
                .toString();
    }
}
