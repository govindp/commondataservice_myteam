package com.incs83.app.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * Created by jayant on 4/5/17.
 */
@Configuration
public class CacheConfig {

    @Value("${hazelCastHostName}")
    private String hostname;

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }
}
