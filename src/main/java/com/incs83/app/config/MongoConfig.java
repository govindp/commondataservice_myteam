package com.incs83.app.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;

import com.incs83.app.mt.CurrentTenantIdentifier;
import com.incs83.app.mt.MongoTenantTemplate;
import com.mongodb.MongoClient;
import com.mongodb.ServerAddress;

/**
 * Created by jayant on 24/4/17.
 */
@Configuration
public class MongoConfig {

    @Value("${mongodb.hostname}")
    private String mongoDBHost;

    @Value("${mongodb.portnumber}")
    private Integer mongoPortNumber;

    @Value("${mongodb.username}")
    private String mongoUserName;

    @Value("${mongodb.password}")
    private String mongoPassword;

    @Value("${mongodb.database}")
    private String mongoDatabase;

    public String getMongoDBHost() {
        return mongoDBHost;
    }

    public void setMongoDBHost(String mongoDBHost) {
        this.mongoDBHost = mongoDBHost;
    }

    public Integer getMongoPortNumber() {
        return mongoPortNumber;
    }

    public void setMongoPortNumber(Integer mongoPortNumber) {
        this.mongoPortNumber = mongoPortNumber;
    }

    public String getMongoUserName() {
        return mongoUserName;
    }

    public void setMongoUserName(String mongoUserName) {
        this.mongoUserName = mongoUserName;
    }

    public String getMongoPassword() {
        return mongoPassword;
    }

    public void setMongoPassword(String mongoPassword) {
        this.mongoPassword = mongoPassword;
    }

    public String getMongoDatabase() {
        return mongoDatabase;
    }

    public void setMongoDatabase(String mongoDatabase) {
        this.mongoDatabase = mongoDatabase;
    }

    @Bean
    public MongoTemplate mongoTemplate() throws Exception {
        ServerAddress serverAddress = new ServerAddress(mongoDBHost, mongoPortNumber);
        /*List mongoCredentials = new ArrayList<MongoCredential>();
        mongoCredentials.add(MongoCredential.createCredential(mongoUserName,mongoDatabase, mongoPassword.toCharArray()));*/
        return new MongoTenantTemplate(
                new SimpleMongoDbFactory(new MongoClient(serverAddress),
                        CurrentTenantIdentifier.DEFAULT_TENANT));
    }
}

