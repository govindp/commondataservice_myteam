package com.incs83.app.config;

import com.sendgrid.SendGrid;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by Jayant Puri on 11-07-2017.
 */
@Configuration
public class EmailConfig {

    @Bean
    String sendGridAPIKey(@Value("${mail.api-key}") String value) {
        return value;
    }

    @Bean
    SendGrid sendGrid(String sendGridAPIKey) {
        SendGrid sendGridMailerObj = new SendGrid(sendGridAPIKey);
        return sendGridMailerObj;
    }
}
