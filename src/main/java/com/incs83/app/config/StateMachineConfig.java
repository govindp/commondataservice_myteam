package com.incs83.app.config;

import com.incs83.app.statemachine.ActionExecutor;
import com.incs83.app.statemachine.GuardExecutor;
import com.incs83.app.statemachine.StateMachineEventListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.config.EnableStateMachine;
import org.springframework.statemachine.config.StateMachineConfigurerAdapter;
import org.springframework.statemachine.config.builders.StateMachineConfigurationConfigurer;
import org.springframework.statemachine.config.builders.StateMachineStateConfigurer;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;

/**
 * Created by rosrivas on 7/15/17.
 */
@Configuration
@EnableStateMachine
public class StateMachineConfig
        extends StateMachineConfigurerAdapter<String, String> {

    @Override
    public void configure(StateMachineConfigurationConfigurer<String, String> config)
            throws Exception {
        config
                .withConfiguration()
                .autoStartup(false)
                .listener(stateMachineEventListener());
    }

    @Override
    public void configure(StateMachineStateConfigurer<String, String> states)
            throws Exception {
        states
                .withStates()
                .initial("NEW")
                .state("NEW")
                .state("READY")
                .state("ASSIGNED")
                .state("IN_PROGRESS")
                .state("COMPLETED")
                .state("PENDING")
                .state("FAILED");
    }

    @Override
    public void configure(StateMachineTransitionConfigurer<String, String> transitions)
            throws Exception {
        transitions
                .withExternal()
                .source("NEW").target("READY").event("INITIALIZE").action(actionExecutor())
                .and()
                .withExternal()
                .source("NEW").target("ASSIGNED").event("ASSIGN").action(actionExecutor())
                .and()
                .withExternal()
                .source("READY").target("ASSIGNED").event("ASSIGN").action(actionExecutor())
                .and()
                .withExternal()
                .source("READY").target("IN_PROGRESS").event("START_PROGRESS").action(actionExecutor())
                .and()
                .withExternal()
                .source("ASSIGNED").target("IN_PROGRESS").event("START_PROGRESS").action(actionExecutor())
                .and()
                .withExternal()
                .source("IN_PROGRESS").target("COMPLETED").event("MARK_COMPLETE").action(actionExecutor())
                .and()
                .withExternal()
                .source("PENDING").target("COMPLETED").event("SUCCESS").action(actionExecutor())
                .and()
                .withExternal()
                .source("PENDING").target("FAILED").event("FAILURE").action(actionExecutor());
    }

    @Bean
    StateMachineEventListener stateMachineEventListener() {
        return new StateMachineEventListener();
    }

    @Bean
    public ActionExecutor actionExecutor() {
        return new ActionExecutor();
    }

    @Bean
    public GuardExecutor guardExecutor() {
        return new GuardExecutor();
    }
}

