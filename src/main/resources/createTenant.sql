CREATE TABLE `role` (
  `id` varchar(255) NOT NULL ,
  `createdAt` datetime NOT NULL,
  `createdBy` varchar(255) NOT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `updatedBy` varchar(255) DEFAULT NULL,
  `mapping` longtext,
  `name` varchar(255) NOT NULL,
  `service` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `entitlement` (
  `id` varchar(255) NOT NULL,
  `createdAt` datetime NOT NULL,
  `createdBy` varchar(255) NOT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `updatedBy` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `permission` (
  `id` varchar(255) NOT NULL,
  `createdAt` datetime NOT NULL,
  `createdBy` varchar(255) NOT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `updatedBy` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `user` (
  `id` varchar(255) NOT NULL,
  `createdAt` datetime NOT NULL,
  `createdBy` varchar(255) NOT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `updatedBy` varchar(255) DEFAULT NULL,
  `address1` varchar(255) DEFAULT NULL,
  `address2` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `company` varchar(255) NOT NULL,
  `country` varchar(255) DEFAULT NULL,
  `deskPhone` varchar(255) DEFAULT NULL,
  `gender` varchar(10) DEFAULT NULL,
  `dob` datetime DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `firstName` varchar(255) NOT NULL,
  `middleName` varchar(255) DEFAULT NULL,
  `imageUrl` varchar(255) DEFAULT NULL,
  `isAccountNonExpired` tinyint(4) DEFAULT '1',
  `isAccountNonLocked` tinyint(4) DEFAULT '1',
  `isCredNonExpired` tinyint(4) DEFAULT '1',
  `isEnabled` tinyint(4) DEFAULT '1',
  `lastName` varchar(255) NOT NULL,
  `mobilePhone` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `pincode` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `user_role` (
  `user_id` varchar(255) NOT NULL,
  `role_id` varchar(255) NOT NULL ,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `FKa68196081fvovjhkek5m97n3y` (`role_id`),
  CONSTRAINT `FKa68196081fvovjhkek5m97n3y` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`),
  CONSTRAINT `FKs51iigign85r46j35s8npjs02` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `user_service` (
  `user_id` varchar(255) NOT NULL,
  `service` varchar(255) DEFAULT NULL,
  KEY `FKjajs0m1lk9ndu1mrnyl120gbe` (`user_id`),
  CONSTRAINT `FKjajs0m1lk9ndu1mrnyl120gbe` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE task_template (
  id varchar(255) NOT NULL,
  createdAt datetime NOT NULL,
  createdBy varchar(255) NOT NULL,
  updatedAt datetime DEFAULT NULL,
  updatedBy varchar(255) DEFAULT NULL,
  name varchar(255) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE task_template_detail (
  id varchar(255) NOT NULL,
  createdAt datetime NOT NULL,
  createdBy varchar(255) NOT NULL,
  updatedAt datetime DEFAULT NULL,
  updatedBy varchar(255) DEFAULT NULL,
  endpoint varchar(255) DEFAULT NULL,
  name varchar(255) NOT NULL,
  sequence bigint(20) NOT NULL,
  task_template_id varchar(255) DEFAULT NULL,
  PRIMARY KEY (id),
  KEY FK2hqgorf0p3vya4e15j5voks06 (task_template_id),
  CONSTRAINT FK2hqgorf0p3vya4e15j5voks06 FOREIGN KEY (task_template_id) REFERENCES task_template (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE task (
  id varchar(255) NOT NULL,
  createdAt datetime NOT NULL,
  createdBy varchar(255) NOT NULL,
  updatedAt datetime DEFAULT NULL,
  updatedBy varchar(255) DEFAULT NULL,
  label varchar(255) NOT NULL,
  description varchar(255) DEFAULT NULL,
  payload longtext,
  service varchar(255) NOT NULL,
  state varchar(255) NOT NULL,
  assignee varchar(255) NOT NULL,
  shortDescription varchar(255) DEFAULT NULL,
  dueDate varchar(255) NOT NULL,
  priority varchar(255) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE task_detail (
  id varchar(255) NOT NULL,
  createdAt datetime NOT NULL,
  createdBy varchar(255) NOT NULL,
  updatedAt datetime DEFAULT NULL,
  updatedBy varchar(255) DEFAULT NULL,
  name varchar(255) NOT NULL,
  payload longtext,
  endpoint varchar(255) DEFAULT NULL,
  state varchar(255) NOT NULL,
  sequence bigint(20) NOT NULL,
  task_id varchar(255) DEFAULT NULL,
  PRIMARY KEY (id),
  KEY FK7uj4gbe2wnx71q6p7cj9uo0da (task_id),
  CONSTRAINT FK7uj4gbe2wnx71q6p7cj9uo0da FOREIGN KEY (task_id) REFERENCES task (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `card` (
  `id` varchar(255) NOT NULL,
  `createdAt` datetime NOT NULL,
  `createdBy` varchar(255) NOT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `updatedBy` varchar(255) DEFAULT NULL,
  `backImg` varchar(255) DEFAULT NULL,
  `backText` text DEFAULT NULL,
  `frontImg` varchar(255) DEFAULT NULL,
  `frontText` text DEFAULT NULL,
  `userId` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;