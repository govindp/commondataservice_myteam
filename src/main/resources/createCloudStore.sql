drop database if exists cloudstore;
create database cloudstore;
use cloudstore;

CREATE TABLE role (
  id varchar(255) NOT NULL ,
  createdAt datetime NOT NULL,
  createdBy varchar(255) NOT NULL,
  updatedAt datetime DEFAULT NULL,
  updatedBy varchar(255) DEFAULT NULL,
  mapping longtext,
  name varchar(255) NOT NULL,
  service varchar(255) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE user (
  id varchar(255) NOT NULL,
  createdAt datetime NOT NULL,
  createdBy varchar(255) NOT NULL,
  updatedAt datetime DEFAULT NULL,
  updatedBy varchar(255) DEFAULT NULL,
  address1 varchar(255) DEFAULT NULL,
  address2 varchar(255) DEFAULT NULL,
  city varchar(255) DEFAULT NULL,
  company varchar(255) NOT NULL,
  country varchar(255) DEFAULT NULL,
  deskPhone varchar(255) DEFAULT NULL,
  gender varchar(10) DEFAULT NULL,
  dob datetime DEFAULT NULL,
  email varchar(255) NOT NULL,
  firstName varchar(255) NOT NULL,
  middleName varchar(255) DEFAULT NULL,
  imageUrl varchar(255) DEFAULT NULL,
  isAccountNonExpired tinyint(4) DEFAULT '1',
  isAccountNonLocked tinyint(4) DEFAULT '1',
  isCredNonExpired tinyint(4) DEFAULT '1',
  isEnabled tinyint(4) DEFAULT '1',
  lastName varchar(255) NOT NULL,
  mobilePhone varchar(255) DEFAULT NULL,
  password varchar(255) DEFAULT NULL,
  pincode varchar(255) DEFAULT NULL,
  state varchar(255) DEFAULT NULL,
  title varchar(255) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE user_role (
  user_id varchar(255) NOT NULL,
  role_id varchar(255) NOT NULL,
  PRIMARY KEY (user_id,role_id),
  KEY FKa68196081fvovjhkek5m97n3y (role_id),
  CONSTRAINT FKa68196081fvovjhkek5m97n3y FOREIGN KEY (role_id) REFERENCES role (id),
  CONSTRAINT FKs51iigign85r46j35s8npjs02 FOREIGN KEY (user_id) REFERENCES user (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE user_service (
  user_id varchar(255) NOT NULL,
  service varchar(255) DEFAULT NULL,
  KEY FKjajs0m1lk9ndu1mrnyl120gbe (user_id),
  CONSTRAINT FKjajs0m1lk9ndu1mrnyl120gbe FOREIGN KEY (user_id) REFERENCES user (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE account_requisition_request (
  id varchar(255) NOT NULL,
  createdAt datetime NOT NULL,
  createdBy varchar(255) NOT NULL,
  updatedAt datetime DEFAULT NULL,
  updatedBy varchar(255) DEFAULT NULL,
  address varchar(255) DEFAULT NULL,
  city varchar(255) DEFAULT NULL,
  lineOfBusiness varchar(255) DEFAULT NULL,
  country varchar(255) DEFAULT NULL,
  companyName varchar(255) NOT NULL,
  title varchar(255) DEFAULT NULL,
  emailId varchar(255) NOT NULL,
  firstName varchar(255) NOT NULL,
  middleName varchar(255) DEFAULT NULL,
  lastName varchar(255) NOT NULL,
  phoneNumber varchar(255) NOT NULL,
  requestState varchar(255) NOT NULL,
  state varchar(255) DEFAULT NULL,
  website varchar(255) NOT NULL,
  zipcode varchar(255) DEFAULT NULL,
  servicesRequested varchar(255) NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY UK_sx483gmuklpxxplsu27bhg6p1 (emailId)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE account (
  id varchar(255) NOT NULL,
  createdAt datetime NOT NULL,
  createdBy varchar(255) NOT NULL,
  updatedAt datetime DEFAULT NULL,
  updatedBy varchar(255) DEFAULT NULL,
  address varchar(255) DEFAULT NULL,
  city varchar(255) DEFAULT NULL,
  companyName varchar(255) NOT NULL,
  title varchar(255) DEFAULT NULL,
  country varchar(255) DEFAULT NULL,
  emailId varchar(255) NOT NULL,
  enabled bit(1) NOT NULL,
  firstName varchar(255) NOT NULL,
  middleName varchar(255) DEFAULT NULL,
  lastName varchar(255) NOT NULL,
  password varchar(255) NOT NULL,
  phoneNumber varchar(255) NOT NULL,
  state varchar(255) DEFAULT NULL,
  status varchar(255) NOT NULL,
  comments varchar(255) NOT NULL,
  website varchar(255) NOT NULL,
  zipcode varchar(255) DEFAULT NULL,
  accountRequest_id varchar(255) DEFAULT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY UK_h739mi9trtcslb9k93qn3y45d (emailId),
  KEY FKl3dfo2wtk75avsfrs3g36qr17 (accountRequest_id),
  CONSTRAINT FKl3dfo2wtk75avsfrs3g36qr17 FOREIGN KEY (accountRequest_id) REFERENCES account_requisition_request (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE account_tenant (
  id varchar(255) NOT NULL,
  createdAt datetime NOT NULL,
  createdBy varchar(255) NOT NULL,
  updatedAt datetime DEFAULT NULL,
  updatedBy varchar(255) DEFAULT NULL,
  tenantGuid varchar(255) NOT NULL,
  tenantName varchar(255) NOT NULL,
  tenantUrl varchar(255) NOT NULL,
  account_id varchar(255) DEFAULT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY UK_7c8hwf3bxm4px9fyq855sdoqa (tenantGuid),
  UNIQUE KEY UK_luxba3re7cpvvvws6kx8jobo5 (tenantName),
  UNIQUE KEY UK_nnmntis59frrad2q0tfk52xxp (tenantUrl),
  KEY FKfp0u95t7bx7ufajxbdstojekh (account_id),
  CONSTRAINT FKfp0u95t7bx7ufajxbdstojekh FOREIGN KEY (account_id) REFERENCES account (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE account_tenant_details (
  id varchar(255) NOT NULL,
  createdAt datetime NOT NULL,
  createdBy varchar(255) NOT NULL,
  updatedAt datetime DEFAULT NULL,
  updatedBy varchar(255) DEFAULT NULL,
  mongoCollectionName varchar(255) NOT NULL,
  mongoPassword varchar(255) NOT NULL,
  mongoUrl varchar(255) NOT NULL,
  mongoUsername varchar(255) NOT NULL,
  mysqlPassword varchar(255) NOT NULL,
  mysqlSchemaName varchar(255) NOT NULL,
  mysqlUrl varchar(255) NOT NULL,
  mysqlUsername varchar(255) NOT NULL,
  neo4jNodeName varchar(255) NOT NULL,
  neo4jPassword varchar(255) NOT NULL,
  neo4jUrl varchar(255) NOT NULL,
  neo4jUsername varchar(255) NOT NULL,
  accountTenant_id varchar(255) DEFAULT NULL,
  PRIMARY KEY (id),
  KEY FKs0jo26hitk5m1n9ms3yey8d1d (accountTenant_id),
  CONSTRAINT FKs0jo26hitk5m1n9ms3yey8d1d FOREIGN KEY (accountTenant_id) REFERENCES account_tenant (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE task_template (
  id varchar(255) NOT NULL,
  createdAt datetime NOT NULL,
  createdBy varchar(255) NOT NULL,
  updatedAt datetime DEFAULT NULL,
  updatedBy varchar(255) DEFAULT NULL,
  name varchar(255) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE task_template_detail (
  id varchar(255) NOT NULL,
  createdAt datetime NOT NULL,
  createdBy varchar(255) NOT NULL,
  updatedAt datetime DEFAULT NULL,
  updatedBy varchar(255) DEFAULT NULL,
  endpoint varchar(255) DEFAULT NULL,
  name varchar(255) NOT NULL,
  sequence bigint(20) NOT NULL,
  task_template_id varchar(255) DEFAULT NULL,
  PRIMARY KEY (id),
  KEY FK2hqgorf0p3vya4e15j5voks06 (task_template_id),
  CONSTRAINT FK2hqgorf0p3vya4e15j5voks06 FOREIGN KEY (task_template_id) REFERENCES task_template (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE task (
  id varchar(255) NOT NULL,
  createdAt datetime NOT NULL,
  createdBy varchar(255) NOT NULL,
  updatedAt datetime DEFAULT NULL,
  updatedBy varchar(255) DEFAULT NULL,
  label varchar(255) NOT NULL,
  description varchar(255) DEFAULT NULL,
  payload longtext,
  service varchar(255) NOT NULL,
  state varchar(255) NOT NULL,
  assignee varchar(255) NOT NULL,
  shortDescription varchar(255) DEFAULT NULL,
  dueDate varchar(255) NOT NULL,
  priority varchar(255) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE task_detail (
  id varchar(255) NOT NULL,
  createdAt datetime NOT NULL,
  createdBy varchar(255) NOT NULL,
  updatedAt datetime DEFAULT NULL,
  updatedBy varchar(255) DEFAULT NULL,
  name varchar(255) NOT NULL,
  payload longtext,
  endpoint varchar(255) DEFAULT NULL,
  state varchar(255) NOT NULL,
  sequence bigint(20) NOT NULL,
  task_id varchar(255) DEFAULT NULL,
  PRIMARY KEY (id),
  KEY FK7uj4gbe2wnx71q6p7cj9uo0da (task_id),
  CONSTRAINT FK7uj4gbe2wnx71q6p7cj9uo0da FOREIGN KEY (task_id) REFERENCES task (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
INSERT into role (id,createdAt,createdBy,updatedAt,updatedBy,name,service,mapping) values ('c7822f01c66b4ebcb67c8dab7f273c6c','2017-05-28 15:44:04','SYSTEM',NULL,NULL,'ROLE_SYS_ADMIN','CLOUDSTORE,BMS,HCM','[{"resource":"ALL","permissions":["ALL"]}]');
INSERT into user (id,createdAt,createdBy,updatedAt,updatedBy,address1,address2,city,company,country,deskPhone,dob,email,firstName,imageUrl,isAccountNonExpired,isAccountNonLocked,isCredNonExpired,isEnabled,lastName,mobilePhone,password,pincode,state,title) values ('dceed2c6223245748b70058d04e29ba9','2017-05-28 15:44:05','SYSTEM', NULL, NULL,'Flat 402', '600, S Abel Street','Milpitas','83incs','United States',NULL,'1974-04-29 00:00:00','admin@83incs.com','Deep',NULL,1,1,1,1,'Nayar','1234567890','$2a$10$uSuNgCpNPErQb6syv2tXyuU53IjMrCULkXlYAdSw/z/rRi8F4AdJe','94040','California','CEO');
INSERT into user_role (user_id,role_id) values ('dceed2c6223245748b70058d04e29ba9','c7822f01c66b4ebcb67c8dab7f273c6c');
INSERT into user_service (user_id,service) values ('dceed2c6223245748b70058d04e29ba9','CLOUDSTORE');