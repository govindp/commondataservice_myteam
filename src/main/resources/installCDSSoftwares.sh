#!/bin/bash

echo " **** Welcome to CDS Installation ****\nThis script will install the necessary softwares needed for CDS installation"
echo " **** RUN AS ROOT *****"

installJAVA() {
	echo "Installing JAVA 1.8....."
	sudo apt-get update &&
	sudo add-apt-repository ppa:webupd8team/java &&
	sudo apt-get update &&
	sudo apt-get install oracle-java8-installer &&
	echo "Installed JAVA Successfully"
}

installMySql() {
	echo "Installing MySql ...."
	sudo apt-get update &&
	sudo apt-get install mysql-server &&
	echo "Installed MySql Successfully"
}

installNeo4j() {
	echo "Installing Neo4J ...."
	wget -O - http://debian.neo4j.org/neotechnology.gpg.key | apt-key add - &&
	echo 'deb http://debian.neo4j.org/repo stable/' > /etc/apt/sources.list.d/neo4j.list &&
	sudo apt-get update &&
	sudo apt-get install neo4j &&
	/etc/init.d/neo4j start &&
	sleep 10
	curl -H "Content-Type: application/json" -X POST -d '{"password":"83incs"}' -u neo4j:neo4j http://localhost:7474/user/neo4j/password &&
	echo "Installed Neo4J Successfully"
}
installMongo(){
	echo "Installing Mongo DB...."
	sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv EA312927 &&
	echo "deb http://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.2.list &&
	sudo apt-get update &&
	sudo apt-get install -y mongodb-org &&
	touch /etc/systemd/system/mongodb.service &&
	echo "[Unit]
		Description=High-performance, schema-free document-oriented database
		After=network.target

		[Service]
		User=mongodb
		ExecStart=/usr/bin/mongod --quiet --config /etc/mongod.conf

		[Install]
		WantedBy=multi-user.target" > /etc/systemd/system/mongodb.service &&
	sudo systemctl start mongodb
	echo "Intsalling Mongo DB Successfull"
}
########## Installing JAVA ###
installJAVA  &&
########## Install MySql
installMySql &&
########## Install Neo4J
installNeo4j &&
########## Install Mongo
installMongo
echo "Script Completed Successfully"