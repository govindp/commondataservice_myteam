FROM java:8

ENV TZ GMT

VOLUME /tmp
ADD build/libs/commondataservice-1.0.0-RELEASE.jar cds.jar
EXPOSE 9999
RUN bash -c 'touch /cds.jar'
ENTRYPOINT ["java","-Dspring.profiles.active=dev", "-Dbootstrap=n", "-jar","/cds.jar"]
